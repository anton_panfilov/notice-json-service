<?php

// Webmaster Agent

return [
    '/account/index',
    '/account/profile/*',

    // webmasters
    '/account/webmasters/index',
    '/account/webmasters/filter-datetime',
    '/account/webmasters/webmaster/index',
    '/account/webmasters/webmaster/users',
    '/account/webmasters/webmaster/filters',

    '/account/webmasters/webmaster/payments-settings',
    '/account/webmasters/webmaster/billing-info-history',
    '/account/webmasters/webmaster/pays-history',

    '/account/webmasters/webmaster/page-main',
    '/account/webmasters/webmaster/page-reports',
    '/account/webmasters/webmaster/page-channels',

    // channels
    '/account/webmasters/channels/*',

    // webmaster api
    '/account/webmasters/payments/index',
    '/account/webmasters/payments/history',
    '/account/webmasters/payments/changes-history',

    // reports
    '/account/reports/webmaster/index',
    '/account/reports/webmaster/summary-hourly',
    '/account/reports/webmaster/leads',
    '/account/reports/webmaster/cpa',
    '/account/reports/webmaster/returns',
    '/account/reports/webmaster/leads-errors',

    '/account/reports/webmaster/top-list',
    '/account/reports/webmaster/dynamic',
    '/account/reports/webmaster/geo',

    // lead
    '/account/leads/index',
    '/account/leads/lead/index',
    '/account/leads/lead/body',
    '/account/leads/lead/log-webmaster',

    // user
    "/account/users/user/index",
    "/account/users/user/password",

    // call center
    '/account/leads/call-center/*',
];