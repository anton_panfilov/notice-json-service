<?php

// Buyer Agent

return [
    '/account/index',
    '/account/profile/*',

    // webmasters
    '/account/webmasters/webmaster/index',

    // reports
    '/account/reports/buyer/*',

    // leads
    '/account/leads/index',
    '/account/leads/lead/*',
    '/account/leads/servers',

    // postings
    '/account/buyers/postings/*',

    // buyers
    '/account/buyers/*',

    // buyers
    '/account/webmasters/index',
    '/account/webmasters/webmaster/index',

    // call-center
    '/account/leads/call-center/*',
];