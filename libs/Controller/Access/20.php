<?php

// Webmaster Access

return [
    '/account/index',
    '/account/profile/*',

    // channels
    '/account/webmasters/channels/*',

    // sync
    '/account/webmasters/sync/index',
    '/account/webmasters/sync/api',
    '/account/webmasters/sync/postback',

    // payments
    '/account/webmasters/payments/index',
    '/account/webmasters/payments/history',
    '/account/webmasters/payments/settings',
    '/account/webmasters/payments/changes-history',

    // webmaster api
    '/account/api/index',

    // reports
    '/account/reports/webmaster/index',
    '/account/reports/webmaster/summary-hourly',
    '/account/reports/webmaster/leads',
    '/account/reports/webmaster/cpa',
    '/account/reports/webmaster/returns',
    '/account/reports/webmaster/leads-errors',

    '/account/reports/webmaster/top-list',
    '/account/reports/webmaster/dynamic',

    // lead
    '/account/leads/index',
    '/account/leads/lead/index',
    '/account/leads/lead/log-webmaster',
];