<?php

/**
 * Class Validator_InArray
 *
 * Проверка присутсвия значения в массиве
 */

class Validator_InArray extends Validator_Abstract {

    const NO_IN_ARRAY = 'no_in_array';

    protected $_messages = array(
        self::NO_IN_ARRAY => 'Value must be from a list of: {list}',
    );

    /*****************************/

    protected $array;

    public function __construct(array $array, $message = null){
        $this->array = $array;
        if($message){
            $this->_messages[self::NO_IN_ARRAY] = $message;
        }
    }

    public function getSettings(){
        return [
            'array' => $this->array
        ];
    }

    /*****************************/

    public function isValid($value, $context = []){
        if(!in_array($value, $this->array)){
            $this->_error(self::NO_IN_ARRAY, ['list' => implode(", ", $this->array)]);
            return false;
        }
        return true;
    }
}