<?php

/**
 * Class Validator_USBankRouting
 *
 * Routing Number (ABA) банка в США. Проверяется на то что бы это были цифры + последняя цифра контрольная
 *
 */

class Validator_USBankRouting extends Validator_Abstract {

    const NUMBERS       = 'numbers';
    const LENGTH        = 'length';
    const INVALID_ABA   = 'invalid_aba';

    protected $_messages = array(
        self::NUMBERS       => 'Routing number can only digits',
        self::LENGTH        => 'Length routing number must be 9 characters',
        self::INVALID_ABA   => 'Invalid routing number',
    );

    /*****************************/

    public function isValid($value, $context = []){
        if(strlen($value) != 9){
            $this->_error(self::LENGTH);
            return false;
        }

        if(!ctype_digit($value)){
            $this->_error(self::NUMBERS);
            return false;
        }

        $n = 0;
        for ($i = 0; $i < 9; $i += 3){
            $n += $value[$i] * 3 + $value[$i+1] * 7 + $value[$i+2];
        }

        if($n != 0 && $n%10 == 0){
            return true;
        }
        else {
            $this->_error(self::INVALID_ABA);
            return false;
        }
    }
}