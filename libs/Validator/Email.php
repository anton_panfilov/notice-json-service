<?php

/**
 * Class Validator_Email
 *
 * Првоерка email по простой регулярке
 */

class Validator_Email extends Validator_Regexp {
    public function __construct($errorMessage = null){
        // значнеие по умолчанию
        $this->_messages[self::REGEXP] = 'Invalid email';

        // инизилизация регулярки
        parent::__construct('/^(?:\S+)@[a-z0-9][-a-z0-9]{0,61}(?:\.[a-z]{1,10})*$/i', $errorMessage);
    }
}