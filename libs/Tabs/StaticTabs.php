<?php

class Tabs_StaticTabs {
    /**
     * Имя Get переменной, из которой получется название таба, при условии что
     * функция getActionFromRequest() не переопределена на другую логику
     *
     * @var string
     */
    protected $actionValueName = 'tab';

    /**
     * Массив табов, а также разделяющих их декораторов
     *
     * @var array
     */
    protected $tabs = [];

    /**
     * Имя таба по умолчанию. Эти правила работают только если имя таба не переданно в запросе
     *
     * null - имя по умолчанию не определенно, будет выбран первый
     * false - не надо выбирать активный таб
     * string - имя таба по умолчанию, если такого таба нет будет выбран первый
     *
     * @var string|null|bool
     */
    protected $default = null;

    /**
     * Текст отображаемый под табами
     * Не обязательно изпользовать этот тип вывода данных, можно просто сделать Echo после рендера табов
     *
     * @var mixed
     */
    protected $text;

    /**
     * Получить из запроса имя активного таба.
     * Если запрос не передает это имя вернеться null
     *
     * @return string|null
     */
    protected function getActiveTabFromRequest(){
        return isset($_GET[$this->actionValueName]) ? $_GET[$this->actionValueName] : null;
    }

    /**
     * Установить название гет переменной, которая будет влиять на табы
     *
     * @param $name
     * @return $this
     */
    public function setValueName($name){
        $this->actionValueName = $name;
        return $this;
    }

    /**
     * Отрисовка одного таба
     * Можно переопределять для изменения метода передачи имени таба
     *
     * @param Tabs_Tab $tab
     *
     * @return string
     */
    protected function renderTab(Tabs_Tab $tab){
        $attr = new Html_Attribs();
        $attr->setAttrib("href", Http_Url::convert([
            $this->actionValueName => $tab->getName()
        ]));

        if($tab->getName() == $this->getActiveTab()){
            $attr->addAttribPre("class", "activeTab ");
        }

        return "<a " . $attr->render() . ">" . Translate::t($tab->getLabel()) . "</a>";
    }

    /**
     * Определение активного таба
     *
     * Срабатывает только 1 раз, при повторном срабатывании берет данные из кеша,
     * поэтому нельзы вызывать эту функцию до полной настройки табов
     *
     * Эта функция срабатывает автоматически при рендере
     *
     * return string Имя активного таба
     */
    public function getActiveTab(){
        if(is_null($this->default)){
            if(count($this->tabs)){
                $requestActiveTab = $this->getActiveTabFromRequest();
                foreach($this->tabs as $t){
                    if($t instanceof Tabs_Tab){
                        if($requestActiveTab == $t->getName()){
                            $this->default = $t->getName();
                            break;
                        }

                        if(is_null($this->default)) $this->default = $t->getName();
                    }
                }
            }
            else {
                $this->default = false;
            }
        }

        return $this->default;
    }

    /**
     * Получение параметров активногор таба или null Если нет такого таба
     *
     * @return mixed
     */
    public function getActiveTabParams(){
        if(isset($this->tabs[$this->getActiveTab()]) && $this->tabs[$this->getActiveTab()] instanceof Tabs_Tab){
            return $this->tabs[$this->getActiveTab()]->getParams();
        }
        return null;
    }

    /**
     * Получение параметров активногор таба или null Если нет такого таба
     *
     * @return mixed
     */
    public function getActiveTabLabel(){
        if(isset($this->tabs[$this->getActiveTab()]) && $this->tabs[$this->getActiveTab()] instanceof Tabs_Tab){
            return $this->tabs[$this->getActiveTab()]->getLabel();
        }
        return null;
    }

    /**
     * Добавить новый таб
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Tabs_Tab
     * @throws \Exception
     */
    public function addTab($name, $label = null, $params = null){
        if(!isset($this->tabs[$name])){
            $this->tabs[$name] = new Tabs_Tab($name, $label, $params);
            return $this->tabs[$name];
        }

        throw new \Exception("Tab with name `{$name}` already exists");
    }

    /**
     * Установить текст, отображаемый под табами
     *
     * @param $text
     */
    public function setText($text){
        $this->text = $text;
        return $this;
    }

    /**
     * Отрисовка табов
     *
     * @return string
     */
    public function render(){
        Site::addCSS('tabs.css');

        $r = '';

        if(count($this->tabs)){
            $r.= "<div class='ap_tabs'>";

            foreach($this->tabs as $tab){
                $r.= ($tab instanceof Tabs_Tab) ? $this->renderTab($tab) : (string)$tab;
            }

            $r.= "</div>";
        }

        if(strlen($this->text)){
            $r.= "<div class='ap_tabs_content'>" . $this->text . "</div>";
        }
        else {
            $r.= "<div class='ap_tabs_separator'></div>";
        }

        return $r;
    }



    /**
     * При использовании объекта как строки будет вызванна отрисовка табов через функцию render()
     *
     * @return string
     */
    public function __toString(){
        return (string)$this->render();
    }
}