<?php

class Email_Functions {
    /**
     * Парсинг строки и получение массива email + name
     *
     * @param $string
     *
     * @return array
     */
    static public function parseEmailString($string){
        $array = array();
        $string = str_replace(array("\r\n", "\r", "\n", ";"), array("\n", "\n", ",", ","), $string);
        $arr = explode("," , $string);

        foreach($arr as $el){
            $line = trim($el);

            if(strlen($line)){
                $matches = array();
                preg_match('/^(.*)(?:\<|\()[ ]*((?:[0-9a-zA-Z]|\.|\-|\_){1,128}@(?:[0-9a-zA-Z]|\.|\-){1,120}\.[a-zA-Z]{2,4})[ ]*(?:\>|\))$/i', $line, $matches);

                if(count($matches) == 3){
                    $name = null;
                    if(strlen(trim($matches[1])))$name = trim($matches[1]);
                    $array[] = array(
                        'email' => $matches[2],
                        'name'  => $name,
                    );
                }
                else {
                    $matches = array();
                    preg_match("/((?:[0-9a-zA-Z]|\.|\-|\_){1,128}@(?:[0-9a-zA-Z]|\.|\-){1,120}\.[a-zA-Z]{2,4})/i", $line, $matches);
                    if(count($matches) == 2){
                        $array[] = array(
                            'email' => $matches[1],
                            'name'  => null,
                        );
                    }
                }
            }
        }

        return $array;
    }
}