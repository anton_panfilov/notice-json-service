<?php

/**
 * Групповые функции для работы с emails + pool для темплейтов, смпт аккаунтов и другого
 */

class Email_Emails  {

    static protected $templates = array();
    static protected $smtp_accounts = array();

    /**
     * Загрузка шаблона по ID
     *
     * Если шаблон не будет найден, будет вызванно исключение
     * Загруженные шаблоны храняться в пуле, и при повторном получении берутся оттуда
     *
     * @param mixed $id
     *
     * @throws Exception
     * @return Email_Template
     */
    static public function getTemplate($id){
        if(!isset(self::$templates[$id])){
            $template = (new Email_Template())->load((int)$id);

            if($template->id){
                self::$templates[$id] = $template;
            }
            else {
                throw new \Exception("Email Template `{$id}` Not Found");
            }
        }
        return self::$templates[$id];
    }

    /***********************************************************/

    /**
     * @var null|array
     */
    protected static $cahce_smtp_accounts_list = null;

    /**
     * @return array Массив доступных SMTP аккаунтов.
     *          key     - id аккаунта
     *          value   - name + email
     */
    static public function getSMTPAccountsArray(){
        if(is_null(self::$cahce_smtp_accounts_list)){
            self::$cahce_smtp_accounts_list = [];
            $all = Db::site()->fetchAll("SELECT `id`, `email`, `name` FROM `email_smtp_accounts`");

            if(count($all)){
                foreach($all as $el){
                    self::$cahce_smtp_accounts_list[$el['id']] = strlen($el['name']) ?
                        "{$el['name']} <{$el['email']}>" :
                        $el['email'];
                }
            }
        }

        return self::$cahce_smtp_accounts_list;
    }

    /**
     * Загрузка данных по SMTP аккаунту
     *
     * Если данные не будут найдены будет вызванно исключение
     *
     * @param mixed $id
     *
     * @throws Exception
     * @return Email_AccountSMTP
     */
    static public function getSMTPAccount($id){
        if(!isset(self::$smtp_accounts[$id])){
            $acc = (new Email_AccountSMTP())->load((int)$id);

            if($acc->id){
                self::$smtp_accounts[$id] = $acc;
            }
            else {
                throw new \Exception("SMTP Accoount `{$id}` Not Found");
            }
        }
        return self::$smtp_accounts[$id];
    }
}
