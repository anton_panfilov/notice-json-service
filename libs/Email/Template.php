<?php


class Email_Template extends Db_Model {
    static protected $_table = 'email_templates';
    static protected $_structure = [
        'smtp_account',
        'title',
        'description',
        'subject',
        'message',
        'is_show',
    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $id;
    public $smtp_account;
    public $title;
    public $description;
    public $subject;
    public $message;
    public $is_show = 1;


    /**
     * @param bool $onlyHide
     */
    public function delete($onlyHide = true){
        if($onlyHide){
            $this->is_show = 0;
            $this->update('is_show');
        }
        else {
            parent::delete();
        }
    }

    /**
     * Получить объект SMTP аккаунта
     *
     * @throws \Exception если аккаунт с таким id не найден
     * @return Email_AccountSMTP
     */
    public function getSMTPAccount(){
        return Email_Emails::getSMTPAccount($this->smtp_account);
    }

    /**
     * Получить строку, от кого будет отправленно письмо
     * Пример: "abc@domain" or "name <abc@domain>"
     *
     * @return string
     */
    public function getFromString(){
        if(strlen($this->getSMTPAccount()->name)){
            return $this->getSMTPAccount()->name . " <" . $this->getSMTPAccount()->email . ">";
        }
        return $this->getSMTPAccount()->email;
    }

    /**
     * Поиск макросов, значения которых можно передать через параметры
     *
     * @return array
     */
    public function searchMacrosesParams(){
        preg_match_all("/(?:\{([a-z0-9._]*)\})/i", $this->message . "\r\n" . $this->subject, $matches);
        return array_unique($matches[1]);
    }
}