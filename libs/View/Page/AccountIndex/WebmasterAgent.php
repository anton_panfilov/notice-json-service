<?php

class View_Page_AccountIndex_WebmasterAgent extends View_Abstract {
    protected $content;

    public function render(){
        ob_start();
        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'>
                    <a href='/account/reports/webmaster/'>Summary Report</a>
                </h4>
                <?
                $from = date('Y-m-d', strtotime('-13 Days'));
                $till = date('Y-m-d');

                $all = Report_Webmaster_Summary::getSummaryDays($from, $till);

                $data   = $all['data'];
                $total  = $all['total'];

                $blank_style = 'color:#CCC';

                $decorator = new Decorator_Date();
                $decorator->markWeekends(1);

                if(count($data)){
                    foreach($data as $k => $el){
                        if($el['leads_all']){
                            $data[$k]['leads_all'] = "<a href='" . Http_Url::convert([
                                    'date' => [
                                        'from' => $el['date'],
                                        'till' => $el['date'],
                                    ],
                                    'status' => null,
                                ], '/account/reports/webmaster/leads') . "'>{$el['leads_all']}</a>";
                        }
                        else {
                            $data[$k]['leads_all'] = "<span style='{$blank_style}'>{$el['leads_all']}</span>";
                        }

                        if($el['leads_sold']){
                            $data[$k]['leads_sold'] = "<a href='" . Http_Url::convert([
                                    'date' => [
                                        'from' => $el['date'],
                                        'till' => $el['date'],
                                    ],
                                    'status' => Lead_Status::SOLD,
                                ], '/account/reports/webmaster/leads') . "'>{$el['leads_sold']}</a>";
                        }
                        else {
                            $data[$k]['leads_sold'] = "<span style='{$blank_style}'>{$el['leads_sold']}</span>";
                        }

                        $data[$k]['date'] = "<a style='text-decoration:none' href='" . Http_Url::convert([
                                'date' => [
                                    'from' => $el['date'],
                                    'till' => $el['date'],
                                ],
                                'status' => null,
                            ], '/account/reports/webmaster/summary-hourly') . "'>" . $decorator->render($el['date']) . "</a>";

                        $data[$k]['epl']      = is_null($el['epl'])       ? "<span style='{$blank_style}'>-</span>" : $el['epl'];
                    }
                }

                // суммарные данные
                if($total['leads_all']){
                    $total['leads_all'] = "<a style='color:#000' href='" . Http_Url::convert([
                            'date' => [
                                'from' => $from,
                                'till' => $till,
                            ],
                            'status' => null,
                        ], '/account/reports/webmaster/leads') . "'>{$total['leads_all']}</a>";
                }

                $table = new Table();
                $table->setPagingDisabled();
                $table->setData($data);

                $table->addField_Text('date',       'Date');
                $table->addField_Text('leads_all',  'Leads')    ->setTextAlignCenter();
                $table->addField_Text('leads_sold', 'Sales')    ->setTextAlignCenter();
                $table->addField_Text('epl',        'EPL')      ->setTextAlignCenter();
                $table->addField_Text('total_wm',   'WM')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->setDescription("Webmaster Earnings");


                $table->addFooter($total);

                echo $table->render();

                ?>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'><a href='/account/reports/webmaster/dynamic?tab=webmaster'>TOP Webmasters</a></h4>
                <?

                $table = new Table(
                    Report_Webmaster_EarningsDynamic::getArray()
                );

                $table->setPagingDisabled();

                // webmaster	week1_value	week2_value	week1_position	week2_position

                /*
                $table->addField_Text("week1_position", "")
                    ->setHeadStretch(2)
                    ->setWidthMin();
                */

                $table->addField_Pattern("", "")
                    ->setWidthMin()
                    ->addDecoratorFunction(function($value, $context){
                        if(is_numeric($context['week1_position']) && is_numeric($context['week2_position'])){
                            if($context['week1_position'] < $context['week2_position']){
                                return "<b style='color:#3cc051'>+" .
                                ($context['week2_position'] - $context['week1_position']) .
                                "</b>";
                            }
                            else if($context['week1_position'] > $context['week2_position']){
                                return "<b style='color:#ed4e2a'>" .
                                ($context['week2_position'] - $context['week1_position']) .
                                "</b>";
                            }
                            else {
                                return "<b style='color:#BBB'>+0</b>";
                            }
                        }
                        else if(is_numeric($context['week1_position'])){
                            return "<b style='color:#3cc051; font-size: 10px'>new</i></b>";
                        }
                        else if(is_numeric($context['week2_position'])){
                            return "<b style='color:#ed4e2a; font-size: 10px'>down</i></b>";
                        }
                    });



                $table->addField_WebmasterName("webmaster", "Webmaster");

                $table->addField_Text("week1_value", "Current")
                    ->setDescription("Earnings top webmasters in the last week (7 days), not counting today")
                    ->setTextAlignCenter();

                $table->addField_Text("week2_value", "Previous")
                    ->setDescription("Earnings top webmasters in the previous week (7 days), not counting today")
                    ->setTextAlignCenter();

                $table->addField_Pattern("", "")
                    ->setWidthMin()
                    ->addDecoratorFunction(function($value, $context){
                        if($context['week1_value'] > $context['week2_value']){
                            return "<span style='color:#3cc051'>+" .
                            ($context['week1_value'] - $context['week2_value']) .
                            "</span>";
                        }
                        else if($context['week1_value'] < $context['week2_value']){
                            return "<span style='color:#ed4e2a'>" .
                            ($context['week1_value'] - $context['week2_value']) .
                            "</span>";
                        }
                        else {
                            return "<span style='color:#BBB'>+0</span>";
                        }

                    });

                echo $table->render();
                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">


                <h4 class='h_border' style="">
                    <a href='/account/profile/webmaster-agent'>Your profile for webmasters</a>
                </h4>
                <?=(new View_User_WebmasterAgent(Users::getCurrent()->id))->render()?>

                <h4 class='h_border' style="clear: both; padding-top: 60px">
                    <a href='/account/webmasters/'>Your Webmasters</a>
                </h4>
                <?=Report_Webmaster_WebmastersList::render() ?>

            </div>

        </div>

        <? return ob_get_clean();
    }
}