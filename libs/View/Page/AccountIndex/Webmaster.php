<?php

class View_Page_AccountIndex_Webmaster extends View_Abstract {
    /**
     * @var Webmaster_Object
     */
    protected $webmaster;

    /**
     * @param $webmasterID
     * @throws Exception если не найден вебмастр с таким id
     */
    function __construct($webmasterID){
        $webmasterID = (int)$webmasterID;

        $this->webmaster = Webmaster_Object::getObject($webmasterID);

        if(!$this->webmaster->id){
            throw new Exception("Webmaster `{$webmasterID}` not found");
        }
    }

    public function render(){
        ob_start();

        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'>
                    <a href='/account/reports/webmaster/'><?=Translate::t("Summary Report")?></a>
                </h4>
                <?

                $all = Report_Webmaster_Summary::getSummaryDays(
                    date('Y-m-d', strtotime('-14Days')),
                    date('Y-m-d'),
                    null,
                    $this->webmaster->id
                );

                $data   = $all['data'];
                $total  = $all['total'];

                $blank_style = 'color:#CCC';

                $decorator = new Decorator_Date();
                $decorator->markWeekends(1);

                if(count($data)){
                    foreach($data as $k => $el){
                        if($el['leads_all']){
                            $data[$k]['leads_all'] = "<a href='" . Http_Url::convert([
                                    'date' => [
                                        'from' => $el['date'],
                                        'till' => $el['date'],
                                    ],
                                    'status' => null,
                                ], '/account/reports/webmaster/leads') . "'>{$el['leads_all']}</a>";
                        }
                        else {
                            $data[$k]['leads_all'] = "<span style='{$blank_style}'>{$el['leads_all']}</span>";
                        }

                        $data[$k]['epl']      = is_null($el['epl'])       ? "<span style='{$blank_style}'>-</span>" : $el['epl'];

                        $data[$k]['date'] = "<a style='text-decoration:none' href='" . Http_Url::convert([
                            'date' => [
                                'from' => $el['date'],
                                'till' => $el['date'],
                            ],
                            'status' => null,
                        ], '/account/reports/webmaster/summary-hourly') . "'>" . $decorator->render($el['date']) . "</a>";
                    }
                }

                $table = new Table();
                $table->setPagingDisabled();
                $table->setData($data);

                $table->addField_Text('date',       'Date');

                $table->addField_Text('leads_all',  'Leads')
                    ->setTextAlignCenter();

                $table->addField_Text('epl',        'EPL')
                    ->setTextAlignCenter()
                    ->setDescription("Total revenue, including revenue from advertisements, earned from each lead.");

                $table->addField_Text('leads_money_wm',   'Earnings')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0');


                $table->addFooter($total);

                echo $table->render();

                ?>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'><?=Translate::t("Account Setup Steps")?></h4>

                <?

                $table = new View_Table_Simple();


                // Account Status //////////////
                $accountStatus = new View_Table_Statuses();

                if($this->webmaster->isActive()){
                    $accountStatus->addOk("<span  style='font-size: 110%;'>" . Translate::t('Active') . "</span>");
                }
                else {
                    $accountStatus->addImportant("
                        <b style='font-size: 110%;'>" . Translate::t((new Webmaster_Status())->getLabel($this->webmaster->getStatus())) . "</b>
                        <p style='font-size: 90%; color: #888'>" .
                            Translate::t("Contact your account representative to activate this account") .
                        "</p>"
                    );
                }

                $table->add('1. ' . Translate::t("Account Status"), $accountStatus->render());


                // Lead Channels //////////////
                $table->add(
                    '2. ' . Translate::t("Lead Channels"),
                    (new View_Table_Statuses())
                        ->addNeutral(
                            "<a style='font-size: 110%;' href='/account/webmasters/channels/'>" .
                                Translate::t("Setup Options") .
                            "</a>"
                        )->render()
                );


                // Synchronization //////////////
                $synchronizationStatus = new View_Table_Statuses();
                $pbLink = "<a href='/account/webmasters/sync/postback' style='font-size: 110%;'>" . Translate::t("Realtime Postbacks") . "</a>";

                if(false){
                    $synchronizationStatus->addOk($pbLink);
                }
                else {
                    $synchronizationStatus->addNeutral($pbLink);
                }

                $synchronizationStatus->addBlank(
                    "<a href='/account/webmasters/sync/api' style='font-size: 80%'>" .
                        Translate::t("Sync by API") .
                    "</a>"
                );

                $table->add('3. ' . Translate::t("Synchronization"), $synchronizationStatus);

                // Payments //////////////

                $paymentsStatus = new View_Table_Statuses();
                $paymentsLink = "<a href='/account/webmasters/payments/' style='font-size: 110%; white-space:nowrap'>" .
                    Translate::t("Payment Settings") .
                    "</a>";

                if(Webmaster_Payments_Object::getObject($this->webmaster->id)->type){
                    $paymentsStatus->addOk($paymentsLink);
                }
                else {
                    $paymentsStatus->addImportant($paymentsLink);
                }

                $paymentsStatus->addBlank("<a href='/account/webmasters/payments/history' style='font-size: 80%'>" .
                    Translate::t("Payment History") .
                "</a>");

                $table->add('4. ' . Translate::t("Payment"), $paymentsStatus);

                /////////////////////////////

                echo $table->render();

                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style="margin-bottom: 70px; white-space: nowrap; ">
                <?if($this->webmaster->getAgentID()) {?>
                    <h4 class='h_border'><?=Translate::t("Your Account Representative")?></h4>
                    <?=(new View_User_WebmasterAgent($this->webmaster->getAgentID()))->render()?>
                <?}?>
            </div>
        </div>

        <? return ob_get_clean();
    }
}