<?php

class View_Page_AccountIndex_BuyerAgent extends View_Abstract {
    protected $content;

    public function render(){
        ob_start();
        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'>
                    <a href='/account/reports/buyer/'>Summary Report</a>
                </h4>
                <?

                $from = date('Y-m-d', strtotime('-13 Days'));
                $till = date('Y-m-d');

                $all = Report_Buyer_Summary::getSummaryDays($from, $till);

                $data   = $all['data'];
                $total  = $all['total'];


                $blank_style = 'color:#CCC';

                $decorator = new Decorator_Date();
                $decorator->markWeekends(1);

                if(count($data)){
                    foreach($data as  $k => $el){
                        if($el['posts_all']){
                            $data[$k]['posts_all'] = "<a href='" . Http_Url::convert([
                                    'date' => [
                                        'from' => $el['date'],
                                        'till' => $el['date'],
                                    ],
                                    'status' => null,
                                ], '/account/reports/buyer/posts') . "'>{$el['posts_all']}</a>";
                        }
                        else {
                            $data[$k]['posts_all'] = "<span style='{$blank_style}'>{$el['posts_all']}</span>";
                        }

                        if($el['posts_send_errors']){
                            $data[$k]['posts_send_errors'] = "<a style='color:#bb2413' href='" . Http_Url::convert([
                                    'date' => [
                                        'from' => $el['date'],
                                        'till' => $el['date'],
                                    ],
                                    'status' => Lead_Processing_Sender_Result_Element_Post::STATUS_SEND_ERROR,
                                ], '/account/reports/buyer/posts') . "'>{$el['posts_send_errors']}</a>";
                        }
                        else {
                            $data[$k]['posts_send_errors'] = "<span style='{$blank_style}'>{$el['posts_send_errors']}</span>";
                        }

                        $data[$k]['convert']  = is_null($el['convert'])   ? "<span style='{$blank_style}'>-</span>" : "{$el['convert']} %";
                        $data[$k]['epl']      = is_null($el['epl'])       ? "<span style='{$blank_style}'>-</span>" : $el['epl'];

                        $data[$k]['date'] = "<a style='text-decoration:none' href='" . Http_Url::convert([
                                'date' => [
                                    'from' => $el['date'],
                                    'till' => $el['date'],
                                ],
                                'status' => null,
                            ], '/account/reports/buyer/summary-hourly') . "'>" . $decorator->render($el['date']) . "</a>";
                    }
                }

                $table = new Table();
                $table->setPagingDisabled();
                $table->setData($data);

                $table->addField_Text('date',       'Date');
                $table->addField_Text('posts_all',  'Leads')    ->setTextAlignCenter();

                if($all['total']['posts_error']){
                    $table->addField_Text('posts_error',            'Errors')
                        ->setTextAlignCenter();
                }

                if($all['total']['posts_unknown']){
                    $table->addField_Text('posts_unknown',          'UE')
                        ->setDescription("Unknown")
                        ->setTextColor('#bb2413')
                        ->setTextAlignCenter();
                }

                if($all['total']['posts_send_errors']){
                    $table->addField_Text('posts_send_errors',      'SE')
                        ->setDescription("Send Errors")
                        ->setTextColor('#bb2413')
                        ->setTextAlignCenter();
                }

                if($all['total']['posts_response_errors']){
                    $table->addField_Text('posts_response_errors',  'RE')
                        ->setDescription("Response Errors")
                        ->setTextColor('#bb2413')
                        ->setTextAlignCenter();
                }

                $table->addField_Text('epl',        'IPP')
                    ->setDescription("Income from post per Posts")
                    ->setTextAlignCenter();

                $table->addField_Text('posts_money_total',  'Value')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0');

                // суммарные данные
                foreach(Report_Buyer_Summary::$postsFieldsTypes as $f){
                    if($total[$f['name']]){
                        $total[$f['name']] = "<a style='color:#000' href='" . Http_Url::convert([
                                'date' => [
                                    'from' => $from,
                                    'till' => $till,
                                ],
                                'status' => $f['status'],
                            ], '/account/reports/buyer/posts') . "'>{$total[$f['name']]}</a>";
                    }
                }

                $table->addFooter($total);

                echo $table->render();

                ?>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'><a href='/account/reports/buyer/dynamic?tab=buyer'>TOP Buyers</a></h4>
                <?
                $table = new Table(
                    Report_Buyer_EarningsDynamic::getArray()
                );

                $table->setPagingDisabled();

                $table->addField_Pattern("", "")
                    ->setWidthMin()
                    ->addDecoratorFunction(function($value, $context){
                        if(is_numeric($context['week1_position']) && is_numeric($context['week2_position'])){
                            if($context['week1_position'] < $context['week2_position']){
                                return "<b style='color:#3cc051'>+" .
                                ($context['week2_position'] - $context['week1_position']) .
                                "</b>";
                            }
                            else if($context['week1_position'] > $context['week2_position']){
                                return "<b style='color:#ed4e2a'>" .
                                ($context['week2_position'] - $context['week1_position']) .
                                "</b>";
                            }
                            else {
                                return "<b style='color:#BBB'>+0</b>";
                            }
                        }
                        else if(is_numeric($context['week1_position'])){
                            return "<b style='color:#3cc051; font-size: 10px'>new</i></b>";
                        }
                        else if(is_numeric($context['week2_position'])){
                            return "<b style='color:#ed4e2a; font-size: 10px'>down</i></b>";
                        }
                    });



                $table->addField_BuyerName("buyer", "Buyer");

                $table->addField_Text("week1_value", "Current")
                    ->setDescription("Income from top buyers in the last week (7 days), not counting today")
                    ->setTextAlignCenter();

                $table->addField_Text("week2_value", "Previous")
                    ->setDescription("Income from top buyers in the previous week (7 days), not counting today")
                    ->setTextAlignCenter();

                $table->addField_Pattern("", "")
                    ->setWidthMin()
                    ->addDecoratorFunction(function($value, $context){
                        if($context['week1_value'] > $context['week2_value']){
                            return "<span style='color:#3cc051'>+" .
                            ($context['week1_value'] - $context['week2_value']) .
                            "</span>";
                        }
                        else if($context['week1_value'] < $context['week2_value']){
                            return "<span style='color:#ed4e2a'>" .
                            ($context['week1_value'] - $context['week2_value']) .
                            "</span>";
                        }
                        else {
                            return "<span style='color:#BBB'>+0</span>";
                        }

                    });

                echo $table->render();

                ?>

                <h4 class='h_border' style="clear: both; padding-top: 60px">
                    <a href='/account/buyers/'>All Buyers</a>
                </h4>
                <?=Report_Buyer_BuyersList::render() ?>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4" style="margin-bottom: 70px">
                <h4 class='h_border'>
                    <a href='/account/profile/commissions'>My Commissions</a>
                </h4>
                <?
                $table = new Table(Pool_Summary::getMyCommision());
                $table->setPagingDisabled();

                $table->addField_Date("date", "Date");
                $table->addField_Text("value", "Value")
                    ->setTextAlignCenter()
                    ->setFooterAutoSum();

                echo $table->render();
                ?>

            </div>

        </div>

        <? return ob_get_clean();
    }
}