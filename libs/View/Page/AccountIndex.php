<?php

class View_Page_AccountIndex extends View_Abstract {
    public function render(){
        if(UserAccess::isAdmin()){
            return (new View_Page_AccountIndex_Admin())->render();
        }

        if(UserAccess::isAgentBuyer()){
            return (new View_Page_AccountIndex_BuyerAgent())->render();
        }

        if(UserAccess::isAgentWebmaster()){
            return (new View_Page_AccountIndex_WebmasterAgent())->render();
        }

        if(UserAccess::isWebmaster()){
            return (new View_Page_AccountIndex_Webmaster(Users::getCurrent()->company_id))->render();
        }

        /**************************/

        return new View_Page_ComingSoon();
    }
}