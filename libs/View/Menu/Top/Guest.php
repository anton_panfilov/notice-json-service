<?php

class View_Menu_Top_Guest extends View_Menu_Top_Abstract {
    public function __construct(){
        $this->left[] = ['Главная',        '/'];

        $this->left[] = ['Банки',     '/banks/',
            "
                <table><tr>
                    <td>
                        <h4>Справочники</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/banks/bik'>БИК</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];
    }
}