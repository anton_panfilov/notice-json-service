<?php

class View_Menu_Top_Admin extends View_Menu_Top_Abstract {
    public function __construct(){
        $this->left[] = ['Main',        '/account/'];

        $this->left[] = ['Profile',     '/account/profile/',
            "
                <table><tr>
                    <td>
                        <h5>User</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/profile/'>My Profile</a></div>
                            <div><a href='/account/profile/password-change'>Change Password</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];


        $this->left[] = ['Reports',     '/account/reports/',
            "
                <table><tr>
                    <td>
                        <h4>Webmaster Reports</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/reports/webmaster/'>Daily Summary</a></div>
                            <div><a href='/account/reports/webmaster/summary-hourly'>Hourly Summary</a></div>
                            <div><a href='/account/reports/webmaster/leads'>Leads Details</a></div>
                            <div><a href='/account/reports/webmaster/calls'>Calls Details</a></div>
                            <div><a href='/account/reports/webmaster/cpa'>Cost per Actions</a></div>
                            <div><a href='/account/reports/webmaster/returns'>Leads Returns</a></div>
                            <div><a href='/account/reports/webmaster/leads-errors'>Leads Errors</a></div>
                        </div>

                        <h5>Performance</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/reports/webmaster/top-list'>Top List</a></div>
                            <div><a href='/account/reports/webmaster/dynamic'>Dynamic</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Buyer Reports</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/reports/buyer/'>Daily Summary</a></div>
                            <div><a href='/account/reports/buyer/summary-hourly'>Hourly Summary</a></div>
                            <div><a href='/account/reports/buyer/posts'>Posts Details</a></div>
                            <div><a href='/account/reports/buyer/cpa'>Cost per Actions</a></div>
                            <div><a href='/account/reports/buyer/returns'>Returns</a></div>
                        </div>

                        <h5>Performance</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/reports/buyer/top-list'>Top List</a></div>
                            <div><a href='/account/reports/buyer/dynamic'>Dynamic</a></div>
                            <div><a href='/account/reports/buyer/convert'>Convert</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];


        $this->left[] = ['Leads',     '/account/leads/',
            "
                <table><tr>
                    <td>
                        <h4>Leads</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/leads/'>Lead Search</a></div>
                            <div><a href='/account/leads/prices'>Prices</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Servers</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/leads/servers'>Servers & IPs</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = ['Webmasters',  '/account/webmasters/',
            "
                <table><tr>
                    <td>
                        <h4>Companies</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/'>All Webmasters</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Channels</h4>
                        <div style='padding-left: 10px'>
                            <a href='/account/webmasters/channels/'>All Lead Channels</a>
                        </div>

                        <h5 style='padding-top:15px'>Web Channels</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/channels/public-websites'>Public Websites</a></div>
                            <div><a href='/account/webmasters/channels/banners'>" . $this->t("Banners") . "</a></div>
                            <div><a href='/account/webmasters/channels/javascript-forms'>Javascript Forms</a></div>
                        </div>

                        <h5 style='padding-top:15px'>Server POST API</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/channels/server-post-channels'>Post Channels</a></div>
                            <div><a href='/account/webmasters/channels/server-post-create'>Create New Channel</a></div>
                        </div>

                        <h5 style='padding-top:15px'>Calls</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/channels/call-channels'>Phones List</a></div>
                            <div><a href='/account/webmasters/channels/call-create'>Add Phone Number</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Payments</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/payments/history'>Pays History</a></div>
                            <div><a href='/account/webmasters/payments/changes-history'>Billing Details Changes</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Tools</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/webmasters/tracking'>Server to Server Tracking</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = ['Buyers',      '/account/buyers/',
            "
                <table><tr>
                    <td>
                        <h4>Buyers Companies</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/buyers/create'>Create Buyer</a></div>
                        </div>
                        <h4></h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/buyers/'>Buyers List</a></div>
                            <div><a href='/account/buyers/?btn_agents='>Agents Permissions</a></div>
                            <div><a href='/account/buyers/upload-postbacks-list'>Upload Postbacks List</a></div>
                            <div><a href='/account/buyers/update-pending-to-reject'>Update Pendings to Reject</a></div>
                        </div>
                    </td>
                    <td>
                        <h4>Postings</h4>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/buyers/postings/create'>Create Posting</a></div>
                            <div><a href='/account/buyers/postings/'>Postings List</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = ['Users',       '/account/users/',
            "
                <table><tr>
                    <td>
                        <h5>By Roles</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/users/?role=" . User_Role::ADMIN . "'>Admin</a></div>
                            <div><a href='/account/users/?role=" . User_Role::AGN_BUYER . "'>Buyer Agent</a></div>
                            <div><a href='/account/users/?role=" . User_Role::AGN_WM . "'>Webmaster Agent</a></div>
                            <div><a href='/account/users/?role=" . User_Role::BUYER . "'>Buyer</a></div>
                            <div><a href='/account/users/?role=" . User_Role::WM . "'>Webmaster</a></div>
                        </div>
                    </td>
                    <td>
                        <h5>Create User</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/users/create-admin'>Create Admin</a></div>
                            <div><a href='/account/users/create-buyer-agent'>Create Buyer Agent</a></div>
                            <div><a href='/account/users/create-webmaster-agent'>Create Webmaster Agent</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];
    }
}