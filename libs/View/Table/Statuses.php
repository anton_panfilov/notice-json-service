<?php

class View_Table_Statuses extends View_Abstract {
    protected $elements = [];

    public function addBlank($text = ''){
        $this->elements[] = [
            "status" => "",
            "text"   => $text,
        ];
        return $this;
    }

    public function addOk($text = ''){
        $this->elements[] = [
            "status" => "<span class='btn btn-xs btn-success text-center' style='width: 24px'><i class='fa fa-check'></i></span>",
            "text"   => $text,
        ];
        return $this;
    }

    public function addImportant($text = ''){
        $this->elements[] = [
            "status" => "<span class='btn btn-xs btn-danger text-center' style='width: 24px'><i class='fa fa-warning'></i></span>",
            "text"   => $text,
        ];
        return $this;
    }

    public function addWarning($text = ''){
        $this->elements[] = [
            "status" => "<span class='btn btn-xs btn-warning text-center' style='width: 24px'><i class='fa fa-exclamation'></i></span>",
            "text"   => $text,
        ];
        return $this;
    }

    public function addNeutral($text = ''){
        $this->elements[] = [
            "status" => "<span class='btn btn-xs btn-default text-center' style='width: 24px'><i class='fa fa-info'></i></span>",
            "text"   => $text,
        ];
        return $this;
    }

    public function render(){
        if(!count($this->elements)) return "";

        Site::addCSS("table/statuses.css");

        ob_start();

        echo "<table class='tbl_statuses'><tbody>";
        foreach($this->elements as $el){
            echo "<tr>
                <td>{$el['status']}</td>
                <td>{$el['text']}</td>
            </tr>";
        }
        echo "</tbody></table>";

        return ob_get_clean();
    }
}