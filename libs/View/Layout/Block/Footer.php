<?php

class View_Layout_Block_Footer extends View_Abstract {
    public function render(){
        if(Boot::isShowErrors()){
            return (new View_Layout_Block_Debug())->render('18px');
        }
        return '';
    }
}