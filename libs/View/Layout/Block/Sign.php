<?php

class View_Layout_Block_Sign extends View_Abstract {
    public function render(){
        return Translate::t("JSON-Service.com") . " &copy; " . date('Y');
    }
}