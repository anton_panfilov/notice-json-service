<?php

class View_Layout_Block_Sidebar extends View_Abstract {
    public function render(){
        if(Controller_Boot::controllerParamIs('sidebar')){
            echo '<aside id="left-panel"  class="hidden-print ">' .
                /*(new View_Menu_Top)->renderLeft() .*/
                Controller_Boot::controllerParamGet('sidebar') .
                '</aside>';
        }
    }
}