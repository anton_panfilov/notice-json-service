<?php

class View_Layout_MainLayout extends View_Layout_Abstract {
    protected function renderCSS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteCSSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . htmlspecialchars($file) . "\" />\r\n";
                }
            }
        }
        return $data;
    }

    protected function renderJS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteJSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<script src=\"" . htmlspecialchars($file) . "\" type=\"text/javascript\"></script>\r\n";
                }
            }
        }
        return $data;
    }

    public function render(){
        ob_start();
       ?>
<!DOCTYPE html>
<html lang="en-us" class="whitehtml">
<head>
    <meta charset="utf-8">
    <title><?=Controller_Boot::controllerParamGet('html_title', 'JSON Service')?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/smartadmin-skins.min.css">

    <link rel="stylesheet" type="text/css" href="/static/css/site.css">

    <?=$this->renderCSS()?>

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="/static/img/logo/json-ld-data.png" type="image/x-icon">
    <link rel="icon" href="/static/img/logo/json-ld-data.png" type="image/x-icon">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="/static/smart/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/static/smart/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/static/smart/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/static/smart/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script>
        if (!window.jQuery) {
            document.write('<script src="/static/smart/js/libs/jquery-2.0.2.min.js"><\/script>');
        }
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="/static/smart/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
    </script>
</head>
<body class="smart-style-3 <?if(!Controller_Boot::controllerParamIs('sidebar')){?>menu-on-top<?}?>">
<header id="header" class="hidden-print">
    <div id="logo-group">
        <span id="logo" style="text-align: center; position: relative; ">
            <a
                style="font-size: 20px; line-height: 24px; white-space: nowrap; color:#EEE; text-decoration: none"
                <? if(Users::getCurrent()->id){ ?> href="/account/" <?}?>
            >
                JSON Service
            </a>
        </span>
    </div>
    <div class="pull-right" style="background: #2C3742">
        <?if(Controller_Boot::controllerParamIs('sidebar')){?>
            <div id="hide-menu" class="btn-header pull-right">
                <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
            </div>
        <?}?>



        <?if(Users::getCurrent()->id){?>
            <div class="btn-header pull-right">
                <span> <a href="/logout" title="Logout"><i class="fa fa-sign-out"></i></a> </span>
            </div>

            <div class="pull-right hidden-mobile hidden-tablet hidden-md hidden-sm">
                <a href="/account/profile/" class="btn btn-labeled btn-primary" style="margin-top: 8px">
                    <span class="btn-label"><i class="glyphicon glyphicon-user"></i></span><?=Users::getCurrent()->login ?>
                </a>
            </div>
        <?}?>

        <?=Site::renderRightMenuBlock()?>
    </div>
    <?=(new View_Menu_Top())->render()?>


</header>
<?= new View_Layout_Block_Sidebar() ?>

<!-- MAIN PANEL -->
<div id="main" role="main">
    <?=new View_Layout_Block_Breadcrumb()?>
    <div id="content">
        <?=new View_Layout_Block_Header()?>
        <?=$this->content?>

    </div>
</div>
<div class="page-footer hidden-print">
    <div class="row">
        <div class="col-xs-12 col-sm-12 text-center ">
            <div style="float: right">
                <div class="lang_bottom">
                    <?=Translate::renderLanguageSelectBtn('dropup')?>
                </div>
            </div>
            <?if(Boot::isShowErrors()){?>
                <div style="float: right; padding: 0 10px 0 10px">
                    <div class="txt-color-white inline-block">
                        <a style="color: #DDD;" href="javascript: jQuery('#footerDebugBlock').toggle();"><?=sprintf("%0.4f sec", Boot::getRuntime())?></a>
                    </div>
                </div>
            <?}?>
            <span class="txt-color-white"><?=new View_Layout_Block_Sign()?></span>
        </div>

    </div>
    <div id="footerDebugBlock" style="display: none">
        <?= new View_Layout_Block_Footer()?>
    </div>
</div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="/static/smart/js/plugin/pace/pace.min.js"></script>

<?=$this->renderJS()?>

<script src="/static/smart/js/bootstrap/bootstrap.min.js"></script>
<script src="/static/smart/js/app.min.js"></script>
<script src="/static/js/top-menu.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
        <?= Controller_Boot::controllerParamGet('SiteAddJSCode') ?>
    })
</script>
</body>
</html>
        <?
        return ob_get_clean();
    }
}