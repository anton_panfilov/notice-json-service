<?php

class View_Panel_Preview extends View_Abstract {
    protected $elements = [];

    public function add($label, $text){
        $this->elements[] = [
            'label' => Translate::t($label),
            'text'  => $text,
        ];
        return $this;
    }

    public function render(){
        if(!count($this->elements)) return "";

        ob_start();

        echo "<div class='preview_div'><table>";
        foreach($this->elements as $el){
            echo "<tr>
                <td>{$el['label']}</td>
                <td>{$el['text']}</td>
            </tr>";
        }
        echo "</table></div>";

        return ob_get_clean();
    }
}