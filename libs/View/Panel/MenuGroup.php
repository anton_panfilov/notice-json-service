<?php

class View_Panel_MenuGroup {
    CONST TYPE_SEPARATOR = 1;

    protected $label;
    protected $icon;

    protected $elements = [];

    protected $cache_is_active;

    public function __construct($label, $icon = 'icon-file'){
        $this->label = Translate::t($label);
        $this->icon = $icon;
    }

    public function add($label, $url, $icon = null){
        $this->cache_is_active = null;

        $this->elements[] = [
            'label' => Translate::t($label),
            'url'   => $url,
            'icon'  => $icon,
        ];

        return $this;
    }

    public function isActive(){
        if(is_null($this->cache_is_active)){
            $this->cache_is_active = false;
            if(count($this->elements)){
                foreach($this->elements as $el){
                    if(isset($el['url']) && explode('?', $el['url'], 2)[0] == $_SERVER['DOCUMENT_URI']){
                        $this->cache_is_active = true;
                        break;
                    }
                }
            }
        }
        return $this->cache_is_active;
    }

    public function render(){
        ob_start();

        if(count($this->elements)){
            echo "<a style='cursor: pointer'>";
            echo "<i class='fa fa-lg fa-fw {$this->icon}'></i>";
            echo "<span class='title'>{$this->label}</span>";



            echo "</a>";

            echo "<ul class='sub-menu'>\r\n";
            foreach($this->elements as $el){
                echo "<li" . ((explode('?', $el['url'], 2)[0] == $_SERVER['DOCUMENT_URI']) ? " class='active'" : "") . ">" .
                    "<a href='" . htmlspecialchars($el['url']) . "'>" .
                        ($el['icon'] ? "<i class='{$el['icon']}'></i> " : "") .
                        "{$el['label']}" .
                    "</a>" .
                "</li>\r\n";
            }
            echo "</ul>";
        }

        return ob_get_clean();
    }
}