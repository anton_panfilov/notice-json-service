<?php

class Form_Validator_MinMax extends Form_Validator_Abstract {
    protected $min;
    protected $max;
    
    public function __construct($min = null, $max = null, $minMessage = null, $maxMessage = null){
        $this->min = is_null($min) ? null : (int)$min;
        $this->max = is_null($max) ? null : (int)$max;
       
        if($minMessage){
            $this->_messages[0] = $minMessage;
        }
        
        if($maxMessage){
            $this->_messages[1] = $maxMessage;
        }
    } 
    
    protected $_messages = array(
        0 => 'Minimum of {min}',
        1 => 'Maximum of {max}',
    );     
    
    public function isValid($value){
        $value = (int)$value;
        
        if($this->min !== null && $value < $this->min){
            $this->_error(0, array(
                'min' => $this->min
            )); 
            return false;    
        }
        
        if($this->max !== null && $value > $this->max){
            $this->_error(1, array(
                'max' => $this->max
            ));     
            return false;
        }
        
        return true;
    }   
}