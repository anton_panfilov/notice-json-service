<?php

/**
* Валидатор по Hostname очень простой, потому что лучше пропустить лишнее, чем не отфильтровать важное
*/

class Form_Validator_Hostname extends Form_Validator_Regexp {
    public function __construct($errorMessage = null){
        // значнеие по умолчанию
        $this->_messages[0] = 'Invalid Hostname';

        // инизилизация регулярки
        parent::__construct(
            '/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/i',
            $errorMessage
        );
    }
}