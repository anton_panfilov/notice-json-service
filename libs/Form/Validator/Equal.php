<?php

class Form_Validator_Equal extends Form_Validator_Abstract {
    protected $element;
    
    public function __construct($element, $errorMessage = null){
        $this->element = $element;
        if($errorMessage){
            $this->_messages[0] = $errorMessage;
        }
    } 
    
    protected $_messages = array(
        0 => 'Value does not match the field "{element}"',
    );     
    
    public function isValid($value){
        if($value != $this->_form->getElement($this->element)->getValue()){
            $this->_error(0, array(
                'element' => $this->_form->getElement($this->element)->getLabel()
            ));
            return false;
        }
        return true;
    }   
}