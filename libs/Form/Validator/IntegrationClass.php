<?php

class Form_Validator_IntegrationClass extends Form_Validator_Abstract {
   public function isValid($value){
        $res = @Json::decode(file_get_contents(
            "https://" . Conf::main()->domain_account . "/api/posting/check-integration-class?class=" .
            urlencode($value),
            false,
            stream_context_create([
                'http' => [
                    'ignore_errors' => true
                ]
            ])
        ));

        if(!isset($res['result']) || (!$res['result'] && !isset($res['reason']))){
            $this->_errors = ['An unknown error occurred while scanning class `' . $value . '`'];
            return false;
        }
        else if(!$res['result']){
            $this->_errors = [$res['reason']];
            return false;
        }

        return true;
    }
}