<?php

class Form_Element_Posting extends Form_Element_AutoComplite {

    protected function accessSelectModify(Db_Statment_Select $select){
        // просмотр по правам
        $buyers = UserAccess::getBuyersAccess();
        if(is_array($buyers)){
            if(count($buyers)) $select->where("`buyerID` in ('" . implode("','", $buyers) . "')");
            else               $select->where("1=0");
        }

        return $select;
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = mb_strtolower(trim($string));

        // получение данных по запросу
        $select = Db::processing()->select("buyers_channels_list", ['postingID', 'postingName', 'buyerName'])
            ->order("postingID")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where(
                "`postingID` rlike ? or LOWER(`postingName`) rlike ? or LOWER(`buyerName`) rlike ?",
                [$stringRegexp, $stringRegexp, $stringRegexp]
            );
        }

        try {
            $data = $this->accessSelectModify($select)->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['postingID']] = "{$el['postingID']}: {$el['postingName']} ({$el['buyerName']})";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        $select = Db::processing()->select('buyers_channels_list')->where("postingID=?", $value);
        return (bool)$this->accessSelectModify($select)->fetchOne();
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            $select = Db::processing()->select('buyers_channels_list')->where("postingID=?", $this->getValue());

            if($el = $this->accessSelectModify($select)->fetchRow()){
                return "{$el['postingID']}: {$el['postingName']} ({$el['buyerName']})";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
