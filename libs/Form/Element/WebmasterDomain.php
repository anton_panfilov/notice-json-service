<?php

class Form_Element_WebmasterDomain extends Form_Element_AutoComplite {

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::processing()->select("webmaster_domains", ['id', 'domain'])
        ->order("id")
        ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or `domain` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = $el['domain'];
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        return (bool)Db::processing()->fetchOne("select id from webmaster_domains where id=?", $value);
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            if($el = Db::processing()->fetchRow("select `id`, `domain` from webmaster_domains where id=? limit 1", $this->getValue())){
                return $el['domain'];
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
