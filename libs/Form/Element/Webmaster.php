<?php

class Form_Element_Webmaster extends Form_Element_AutoComplite {

    protected function accessSelectModify(Db_Statment_Select $select){
        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            // full
        }
        else if(UserAccess::isAgentWebmaster()){
            $select->where('agent=?', Users::getCurrent()->id);
        }
        else {
            $select->where('1=0');
        }

        return $select;
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::processing()->select("webmaster_accounts", ['id', 'name'])
        ->order("id")
        ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or `name` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $this->accessSelectModify($select)->fetchAll();
        }
        catch(Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = "{$el['id']}: {$el['name']}";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        $select = Db::processing()->select('webmaster_accounts', 'id')
            ->where('id=?', $value);

        return (bool)$this->accessSelectModify($select)->fetchOne();
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            $select = Db::processing()->select('webmaster_accounts', ['id', 'name'])
                ->where('id=?', $this->getValue());

            if($el = $this->accessSelectModify($select)->fetchRow()){
                return "{$el['id']}: {$el['name']}";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
