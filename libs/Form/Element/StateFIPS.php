<?php

class Form_Element_StateFIPS extends Form_Element_AutoComplite {

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select("geo_states", ['fips', 'code', 'title'])
            ->order("code")
            ->limit(15);

        $temp = explode(" - ", $string);
        if(
            count($temp) == 3 &&
            $temp[0] > 0 &&
            strlen($temp[1]) == 2 &&
            Geo::getStateCode((int)$temp[0]) == $temp[1]
        ){
            $select->where("`fips` rlike ? and `code` rlike ?", [(int)$temp[0], $temp[1]]);
        }
        else {

            if (strlen($string)) {
                // подготовка строки для регулярного выражения
                $stringRegexp = "";
                foreach (str_split($string) as $char) {
                    $stringRegexp .= "\\" . $char;
                }

                $select->where("`fips`=? or `code` rlike ? or `title` rlike ?", [(int)$string, $stringRegexp, $stringRegexp]);
            }
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['fips']] = sprintf("%02d", $el['fips']) . " - {$el['code']} - {$el['title']}";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        return (bool)Db::site()->fetchOne("select `fips` from geo_states where `fips`=?", $value);
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            if($el = Db::site()->fetchRow(
                "select `fips`, `code`, `title` from geo_states where `fips`=? limit 1",
                $this->getValue())
            ){
                return sprintf("%02d", $el['fips']) . " - {$el['code']} - {$el['domain']}";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
