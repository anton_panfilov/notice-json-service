<?php

class Form_Element_Radio extends Form_Element_Abstract {
    /**
     * @var Html_Attribs
     */
    protected $attribs;

    /**
     * @var Html_Attribs
     */
    protected $attribs_label;

    protected $options = array();

    protected function init(){
        parent::init();

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("type", 'radio');

        $this->attribs_label = new Html_Attribs();
    }

    /**
     * @return Html_Attribs
     */
    public function getAttribs(){
        return $this->attribs;
    }

    /**
     * @return Html_Attribs
     */
    public function getLabelAttribs(){
        return $this->attribs_label;
    }

    /**
     * @param $width
     * @return $this
     */
    public function setLabelWidth($width){
        $this->getLabelAttribs()->setCSS('width', $width);
        return $this;
    }

    /**
     * Устанвоить список возможных значений
     *
     * @param array $options
     * @return Form_Element_Radio
     */
    public function setOptions(array $options){
        $this->options = $options;
        $this->addValidator(new Form_Validator_InArray(array_keys($this->options)), 'inArray');

        return $this;
    }


    public function render(){
        $result = "";
        if(count($this->options)){
            $i = 0;
            foreach($this->options as $k => $v){
                $attr = clone $this->attribs;

                $attr->setAttrib("id", "{$this->name}_{$i}");
                $attr->setAttrib("value", $k);

                if((string)$k == (string)$this->getValue()) $attr->setAttrib("checked", "checked");
                else                        $attr->setAttrib("checked");

                // conditions - setting condition trigger for element
                if($this->form->getConditional($this->name)){
                    $attr->setAttrib("onChange", "document.ap_form_conditionals.run('{$this->name}');");
                }

                $result.= "<label" . $this->attribs_label->render() . "><input" . $attr->render() . "/>" . Translate::t($v) . "</label>";

                $i++;
            }
        }

        return $result;
    }

    /**
     * Добавление условия, когда при определеннных значениях этого поля, показываються или скрываются другие элементы
     *
     * @param string|array $values    Значения элемента, при которых будут показываться значения
     *                                Можно передать как одно значение (string), так и массив значений (array)
     *                                При передаче массива, элементы будут отображаться при совпадении хотябы с один значением
     * @param string|array $elements  Поля, котоорые будут показываться при определенных выше значениях
     *                                Можно передать как имя одного элемента (string), так и массив имен (array)
     * @return Form_Element_Select
     */
    public function addConditionalFields_InArray($values, $elements){
        $this->form->addConditionalFileds_Array($this->name, $values, $elements);
        return $this;
    }

    /**
     * Добавление условия, когда при определеннных значениях этого поля, показываються или скрываются другие элементы
     *
     * @param string|array $values    Значения, при которых элементы будут скрываться
     *                                Можно передать массив, тогда для всех этих значений поле будет скрыто
     * @param string|array $elements  Поля, котоорые будут показываться при определенных выше значениях
     *                                Можно передать как имя одного элемента (string), так и массив имен (array)
     * @return Form_Element_Select
     */
    public function addConditionalFields_NotInArray($values, $elements){
        $this->form->addConditionalFileds_NotArray($this->name, $values, $elements);
        return $this;
    }

    /**
     * Получение значения переменной в javascript
     *
     * @return string
     */
    public function javaScriptFunction_GetValue(){
        return "jQuery(\"input[name='{$this->name}']:checked\").val()";
    }
}
