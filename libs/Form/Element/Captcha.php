<?php

class Form_Element_Captcha extends Form_Element_Abstract {
    /**
    * Капчам может работать в таком режиме что не требуется вводить её заново некоторое время или до определенного момента
    */
    protected $one_check = false;
    protected $one_check_seconds = 600;
    protected $one_check_beforeFormSuccess = true;

    protected function init(){
        parent::init();

        $this->setLabel("Prove you’re not a robot");
        $this->addValidator(new Form_Validator_Capcha($this->name));
        $this->setRenderTypeBigValue();
    }

    public function render(){
        return Captcha::renderCaptchaHtmlBlock($this->name);
    }

    /**
    * Настрйока поведения капчи после правильного заполнения
    *
    * @param mixed $flag                - требуется ли капча каждый раз или прячется после правильного ввода
    * @param mixed $seconds             - на какое время прячется
    * @param mixed $beforeFormSuccess   - удаляется ли сессия после правильного заполнения формы
    *
    * @return self
    */
    public function setOneCheckSession($flag = true, $seconds = 3600, $beforeFormSuccess = true){
        $this->one_check                    = (bool)$flag;
        $this->one_check_seconds            = max(0, (int)$seconds);
        $this->one_check_beforeFormSuccess  = (bool)$beforeFormSuccess;

        return $this;
    }

    protected function lateInit(){
        if($this->one_check && isset($_COOKIE[Captcha::getCookieSessionName($this->name)])){
            $a = explode("-", $_COOKIE[Captcha::getCookieSessionName($this->name)], 2);

            if(
                count($a) == 2 &&
                strlen($a[1]) == 32 &&
                Db::site()->fetchOne(
                    "select id from captcha_one_check_session where id=? and `key`=? and ip=? and name_hash=? and `life`>=?",
                    [
                        (int)substr($a[0], 0, 15),
                        $a[1],
                        inet_pton($_SERVER['REMOTE_ADDR']),
                        sprintf("%u", crc32($this->name)),
                        date('Y-m-d H:i:s'),
                    ]
                )
            ){
                $this->setUse(0);
            }
            else {
                setcookie(
                    Captcha::getCookieSessionName($this->name),
                    null,
                    null,
                    "/"
                );
            }
        }

    }

    public function isValid($params, $notErrors = false){
        // $notErrors - игнорируется

        if($this->one_check && $this->getUse()){
            if(parent::isValid($params)){
                // создание сессии
                $this->setUse(0);

                $key = String_Hash::randomString(32);
                $cookie_name = Captcha::getCookieSessionName($this->name);

                Db::site()->insert("captcha_one_check_session", [
                    'life'      => date('Y-m-d H:i:s', time() + $this->one_check_seconds),
                    'ip'        => inet_pton($_SERVER['REMOTE_ADDR']),
                    'name_hash' => sprintf("%u", crc32($this->name)),
                    'key'       => $key,
                ]);

                $_COOKIE[$cookie_name] = Db::site()->lastInsertId() . "-" . $key;

                setcookie(
                    $cookie_name,
                    $_COOKIE[$cookie_name],
                    time() + $this->one_check_seconds,
                    "/"
                );

                setcookie(
                    Captcha::getCookieName($this->name),
                    null,
                    null,
                    "/"
                );

                return true;
            }
            return false;
        }

        return parent::isValid($params);
    }

    public function formValidationComplite(){
        if($this->one_check && $this->one_check_beforeFormSuccess){
            if(isset($_COOKIE[Captcha::getCookieSessionName($this->name)])){
                // Удаление из базы
                $a = explode("-", $_COOKIE[Captcha::getCookieSessionName($this->name)], 2);

                if(count($a) == 2){
                    Db::site()->delete("captcha_one_check_session",
                        "id=? and `key`=? and ip=? and name_hash=?",
                        (int)substr($a[0], 0, 15),
                        $a[1],
                        inet_pton($_SERVER['REMOTE_ADDR']),
                        sprintf("%u", crc32($this->name))
                    );
                }
            }

            // Удаление из кук
            setcookie(
                Captcha::getCookieSessionName($this->name),
                null,
                null,
                "/"
            );
        }
    }
}
