<?php

class Form_Element_File extends Form_Element_Input {
    protected function init(){
        parent::init();
        $this->attribs->setAttrib("type", "file");
    }
    
    public function setMultiple ($m = true){
        if($m) {
            $this->attribs->setAttrib("name", $this->name .'[]');
            $this->attribs->setAttrib("multiple", "multiple");
        } else {
            $this->attribs->setAttrib("name", $this->name);
            $this->attribs->setAttrib("multiple");
        }
        return $this;
    }
    
    public function setAccept ($a = null){
        if(isset($a) && $a != "") $this->attribs->setAttrib("accept", $a);
        else                      $this->attribs->setAttrib("accept");
        return $this;
    }

    public function isValid ($params, $notErrors = false) {return true;}
}
