<?php

class Form_Filter_Trim extends Form_Filter_Abstract {
    public function filter($value){
        return trim($value);
    }   
}