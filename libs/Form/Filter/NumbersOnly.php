<?php

class Form_Filter_NumbersOnly extends Form_Filter_Abstract {
    public function filter($value){
        return preg_replace("/[^\d]/", "", $value);
    }   
}