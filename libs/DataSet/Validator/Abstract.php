<?php

abstract class DataSet_Validator_Abstract extends Validator_Abstract {
    /**
     * @var DataSet_Result_ValidationDataSet
     */
    protected $validationResult;

    /**
     * Массив соответсвий кодов ошибок, элементам которым они принадлежат
     *
     * @var array
     */
    protected $_elementsMap = [];

    public function setValidationResult(DataSet_Result_ValidationDataSet $result){
        $this->validationResult = $result;
        return $this;
    }

    /**
     * Получить код эклмента к которому адресуеться ошибка
     * Для специальных валидаторов нужно составить мапы соответсвия, для того что бы грамотно работали переводы
     *
     * @param $code
     * @return string
     */
    public function getElementByErrorCode($code){
        return isset($this->_elementsMap[$code]) ? $this->_elementsMap[$code] : "root";
    }
}