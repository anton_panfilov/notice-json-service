<?php

class DataSet_Result_ValidationDataSet {
    protected $success = true;
    protected $errors  = [];

    public function addError($element, $code, $message, $values){
        $this->success = false;

        $this->errors[] = [
            'element'   => $element,
            'message'   => $message,
            'code'      => $code,
            'values'    => $values,
        ];

        return $this;
    }

    public function isSuccess(){
        return (bool)$this->success;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function isErrorByName($element){
        if(count($this->getErrors())){
            foreach($this->getErrors() as $error){
                if($error['element'] == $element){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Добавление результтата от элемента
     *
     * @param $name
     * @param DataSet_Result_ValidationElement $result
     * @return $this
     */
    public function addResultElement($name, DataSet_Result_ValidationElement $result){
        if(!$result->isSuccess()){
            $this->success = false;

            $errors = $result->getErrors();
            if(count($errors)){
                foreach($errors as $error){
                    $this->addError($name, $error['code'], $error['message'], $error['values']);
                }
            }
        }

        return $this;
    }

    public function addResultDataSet($name, DataSet_Result_ValidationDataSet $result){
        if(!$result->isSuccess()){
            $this->success = false;

            $errors = $result->getErrors();
            if(count($errors)){
                foreach($errors as $error){
                    $this->addError($name . "." . $error['element'], $error['code'], $error['message'], $error['values']);
                }
            }
        }

        return $this;
    }
}