<?php

class DataSet_Result_ValidationElement {
    protected $success = true;
    protected $errors  = [];

    /*******************************************************/

    /**
     * Установка реультата в режим ошибки.
     * Это делатеься отдельно, так так может быть ситуация что текста ошибки нет и надо просто показать ошибку без описания
     *
     * @return $this
     */
    public function setError(){
        $this->success = false;
        return $this;
    }

    /**
     * Добавление ошибки и перевод результата в режим ошибки
     *
     * @param $code
     * @param $message
     * @param $values
     * @return $this
     */
    public function addError($code, $message, $values){
        $this->success = false;

        $this->errors[] = [
            'message'   => $message,
            'code'      => $code,
            'values'    => $values,
        ];

        return $this;
    }

    /*******************************************************/

    public function isSuccess(){
        return (bool)$this->success;
    }

    public function getErrors(){
        return $this->errors;
    }
}