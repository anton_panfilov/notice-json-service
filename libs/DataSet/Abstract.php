<?php

class DataSet_Abstract {

    protected $_label;

    /**
     * @var DataSet_Validator_Abstract[]
     */
    protected $_validators = [];

    /******************************************************************/

    protected function _addValidator(DataSet_Validator_Abstract $validator, $index = null){
        if(is_null($index)){
            $index = get_class($validator);
        }

        if(isset($this->_validators[$index])){
            throw new Exception("Validator index `{$index}` already use");
        }

        $this->_validators[$index] = $validator;

        return $this;
    }

    /******************************************************************/

    /**
     * @param null $data null|array|stringJSON
     */
    public function __construct($data = null){
        $this->_init();

        $vars = get_object_vars($this);
        if(count($vars)){
            foreach($vars as $k => $name){
                if(substr($k, 0, 1) != '_'){
                    if(is_null($this->$k)){
                        $this->$k = new DataSet_Element_String();
                    }
                }
            }
        }

        if(!is_null($data)){
            $this->_setData($data);
        }
    }

    protected function _init(){}

    /******************************************************************/

    public function _getTranslateMap(){
        $result = [];

        $vars = get_object_vars($this);
        if(count($vars)){
            foreach($vars as $k => $name){
                if(substr($k, 0, 1) != '_'){
                    $temp = [];

                    if($this->$k instanceof DataSet_Element_Abstract){
                        $temp = $this->$k->getTranslateMap();
                    }
                    else if($this->$k instanceof DataSet_Abstract){
                        $temp = $this->$k->_getTranslateMap();
                    }

                    if(count($temp)){
                        foreach($temp as $tmk => $tmv){
                            $result["{$k}.{$tmk}"] = $tmv;
                        }
                    }
                }
            }
        }

        if(count($this->_validators)){
            foreach($this->_validators as $k => $v){
                $temp = $v->getTranslateMap();
                if(count($temp)){
                    foreach($temp as $code => $message){
                        $result[$v->getElementByErrorCode($code) . "." . $k . "." . $code] = $message;
                    }
                }
            }
        }

        ksort($result);

        return $result;
    }

    /**
     * Получить массив всех данных, по которым можно составить документцию об этом теле лида
     *
     * @return array
     */
    public function _getDocumentation(){
        $result = [];


        return $result;
    }

    public function getLabel(){
        return $this->_label;
    }

    public function setLabel($label){
        $this->_label = $label;
        return $this;
    }

    /**
     * Получить HTML документацию по этому телу лида
     *
     * @param null $first
     * @param array $excluded Массив полей, которые надо исключить из документации
     *                          (используеться в телефонных продуктах)
     * @return string
     */
    public function _renderDocumentation($first = null, $excluded = []){
        ob_start();
        $els = 0;
        $vars = get_object_vars($this);
        if(count($vars)){
            foreach($vars as $k => $name){
                $fullK = strlen($first) ? "{$first}.{$k}" : "{$k}";

                if(substr($k, 0, 1) != '_' && !in_array($fullK, $excluded)){
                    if($this->$k instanceof DataSet_Element_Abstract){
                        $els++;
                        echo $this->$k->_renderDocumentation($k, $first);
                    }
                    else if($this->$k instanceof DataSet_Abstract){
                        echo $this->$k->_renderDocumentation(
                            $fullK, $excluded
                        );
                    }
                }
            }
        }

        $res = ob_get_clean();

        ob_start();
        if($els > 0 && strlen($first)){
            ?>
                <tr style="background: #F2F2F2">
                    <td colspan="3" style="vertical-align: middle"><b><?=$first?></b></td>
                    <td colspan="1" style="font-size: 150%"><?=$this->getLabel()?></td>
                </tr>
            <?
        }

        return ob_get_clean() . $res;
    }

    /******************************************************************/

    /**
     * Загрузить данные из массива или из JSON объекта прдставленного в виде строки
     *
     * @param $arrayOrJson
     * @throws Exception неверный формат данных
     */
    public function _setData($arrayOrJson){
        // преобразование данных к массиву
        if(is_string($arrayOrJson)){
            $arrayOrJson = Json::decode($arrayOrJson);
        }
        else if($arrayOrJson instanceof DataSet_Abstract){
            $arrayOrJson = $arrayOrJson->_getArray();
        }

        if(is_array($arrayOrJson)){
            $arrayOrJson = $this->_dataModification($arrayOrJson);

            $vars = get_object_vars($this);
            if(count($vars)){
                foreach($vars as $k => $name){
                    if(substr($k, 0, 1) != '_'){
                        if(isset($arrayOrJson[$k])){
                            if($this->$k instanceof DataSet_Element_Abstract){
                                $this->$k->setValue($arrayOrJson[$k], $arrayOrJson);
                            }
                            else if($this->$k instanceof DataSet_Abstract){
                                $this->$k->_setData($arrayOrJson[$k], $arrayOrJson);
                            }
                        }
                    }
                }
            }
        }
        else {
            throw new Exception("Invalid Data Format");
        }
    }

    /**
     * Модификация массива данных
     *
     * @param $data
     * @return mixed
     */
    public function _dataModification($data){
        return $data;
    }

    /**
     * Экспортировать в массив
     *
     * @param bool $stringMode - Все значения бадат в виде строк, для отрисовок
     * @param bool $includeBlank
     * @return array
     * @throws Exception
     */
    public function _getArray($stringMode = false, $includeBlank = true){
        $result = array();
        $values = get_object_vars($this);
        if(count($values)){
            foreach($values as $k => $v){
                if(substr($k, 0, 1) != "_"){
                    if($v instanceof DataSet_Abstract){
                        $result[$k] = $v->_getArray($stringMode, $includeBlank);
                    }
                    else if($v instanceof DataSet_Element_Abstract){
                        $v = $stringMode ? $v->getValueString() : $v->getValue();
                        if($includeBlank || strlen($v)){
                            $result[$k] = $v;
                        }
                    }
                    else {
                        throw new Exception("Invalid param `{$k}``");
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Экспортировать в строку с JSON
     *
     * @param int $options
     * @return string JSON
     */
    public function _getJson($options = JSON_UNESCAPED_UNICODE){
        return json_encode($this->_getArray(), $options);
    }

    /******************************************************************/


    /**
     * Проверить текущие данные
     *
     * Ответ:
     * 1. правильно или нет
     * 2. массив ошибок
     *  (элемент) - ошибка
     *
     *
     */
    public function _isValid(){
        $result = new DataSet_Result_ValidationDataSet();

        $values = get_object_vars($this);
        if(count($values)){
            foreach($values as $k => $v){
                if(substr($k, 0, 1) != "_"){
                    if($v instanceof DataSet_Abstract){
                        $result->addResultDataSet($k, $v->_isValid());
                    }
                    else if($v instanceof DataSet_Element_Abstract){
                        $result->addResultElement($k, $v->isValid($this->_getArray()));
                    }
                    else {
                        throw new Exception("Invalid param `{$k}``");
                    }
                }
            }
        }

        if(count($this->_validators)){
            foreach($this->_validators as $k => $v){
                /*$result->addError()*/
                $v->setValidationResult($result);
                if(!$v->isValid(null, $this->_getArray())){
                    if(count($v->getErrors())){
                        foreach($v->getErrors() as $el){
                            $result->addError(
                                $v->getElementByErrorCode($el['code']),
                                $k . "." . $el['code'],
                                $el['message'],
                                $el['values']
                            );
                        }
                    }
                    else {
                        throw new Exception("Not errors description");
                    }
                }
            }
        }

        return $result;
    }

    /******************************************************************/

    /**
     * Получить HTML предсталвение данных
     *
     * @param bool $includeBlank
     * @return string
     */
    public function _renderText($includeBlank = false){
        $r = "";
        $data = $this->_getArrayForRender($includeBlank);

        if(count($data)){
            $r.= "<table style='width: 100%'>";
            $r.= $this->_renderFromArray($data);
            $r.= "</table>";
        }
        else {
            $r = 'not data';
        }

        return $r;
    }

    /**
     * @param $data
     * @return string
     */
    protected  function _renderFromArray($data){
        $r = "";
        if(is_array($data) && count($data)){
            foreach($data as $k => $v){
                if(is_array($v) && isset($v['type'])){
                    if($v['type'] == 'array'){
                        $r.= "<tr>";
                        if(isset($v['label']) && strlen($v['label'])){
                            $r.= "<td colspan='2' style='border-bottom: #EEE solid 1px; padding: 5px'>";
                                $r.= "<h3 style='padding:0px; margin:0px; line-height: 100%; '>" .
                                        $v['label'] .
                                    "</h3>";
                            $r.= "</td>";
                        }

                        $r.= "</tr>";
                        $r.= $this->_renderFromArray($v['array']);
                        $r.= "<tr><td style='padding-top: 30px' colspan='2'></td></tr>";
                    }
                    else if($v['type'] == 'value'){
                        $r.= "<tr>";
                        $r.= "<td style='padding: 5px; text-align: left; color: #999; vertical-align: top; white-space: nowrap; width:1%'>" . (strlen($v['label']) ? $v['label'] : $k) . "</td>";
                        $r.= "<td style='padding: 5px; text-align: left; color: #000; vertical-align: top; padding-left: 20px'>{$v['value']}</td>";
                        $r.= "</tr>";
                    }
                    else {
                        $r.= "";
                    }
                }

            }
        }

        return $r;
    }

    /**
     * @param bool $includeBlank
     * @return array
     * @throws Exception
     */
    protected function _getArrayForRender($includeBlank = false){
        $result = [];
        $values = get_object_vars($this);
        if(count($values)){
            foreach($values as $k => $v){
                if(substr($k, 0, 1) != "_"){
                    if($v instanceof DataSet_Abstract){
                        $r = $v->_getArrayForRender($includeBlank);
                        if(count($r)){
                            $result[$k] = [
                                'type'  => 'array',
                                'label' => $v->getLabel(),
                                'array' => $r,
                            ];
                        }
                    }
                    else if($v instanceof DataSet_Element_Abstract){
                        if($includeBlank || strlen($v->getValueString())){
                            $result[$k] = [
                                'type'  => 'value',
                                'label' => $v->getLabel(),
                                'value' => $v->getValueString(),
                            ];
                        }
                    }
                    else {
                        throw new Exception("Invalid param `{$k}`");
                    }
                }
            }
        }

        return $result;
    }

    /******************************************************************/

    /**
     * Отрисовать данные в форме, которая может изменить их
     *
     */
    public function _renderForm(){

    }

    /******************************************************************/
}