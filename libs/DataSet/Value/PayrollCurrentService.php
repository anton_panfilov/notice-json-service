<?php

class DataSet_Value_PayrollCurrentService extends DataSet_Value_Set {
    const IN_HOUSE                      = 1;
    const ACCOUNTING_BOOKKEEPING        = 2;
    const PAYROLL                       = 3;
    const HR                            = 4;
    const NA                            = 5;
    const OTHER                         = 6;

    static protected $set = [
        self::IN_HOUSE                  => 'In House',
        self::ACCOUNTING_BOOKKEEPING    => 'Accounting Bookkeeping',
        self::PAYROLL                   => 'Payroll',
        self::HR                        => 'HR',
        self::NA                        => 'NA',
        self::OTHER                     => 'Other',
    ];
}

