<?php

class DataSet_Value_MaritalStatus extends DataSet_Value_Set {
    const MARRIED           = 1; // в браке
    const CIVIL_MARRIAGE    = 2; // гражданский брак
    const DIVORCED          = 3; // разведен
    const SINGLE            = 4; // одинок

    static protected $set = [
        self::MARRIED           => 'В браке',
        self::CIVIL_MARRIAGE    => 'Гражданский брак',
        self::DIVORCED          => 'В разводе',
        self::SINGLE            => 'Холостой (Незамужная)',
    ];
}

