<?php

class DataSet_Value_MortgageType extends DataSet_Value_Set {
    const REFINANCE = 1;    // Под залог имеющейся недвижимости
    const NEW_HOME  = 2;    // На преобретение новой недвижимости

    static protected $set = [
        self::REFINANCE => 'Под залог имеющейся недвижимости',
        self::NEW_HOME  => 'На преобретение новой недвижимости',
    ];
}

