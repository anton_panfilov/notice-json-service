<?php

class DataSet_Value_Gender extends DataSet_Value_Set {
    const MALE   = 1; // м
    const FEMALE = 2; // ж

    static protected $set = [
        self::MALE      => 'Мужской',
        self::FEMALE    => 'Женский',
    ];
}

