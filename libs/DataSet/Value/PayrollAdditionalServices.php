<?php

class DataSet_Value_PayrollAdditionalServices extends DataSet_Value_Set {
    const DIRECT_DEPOSIT        = 1;
    const PAY_CARDS             = 2;
    const CHECK_SIGNING         = 3;
    const CHECK_STUFFING        = 4;
    const A401K                 = 5;
    const ONLINE_ACCESS         = 6;
    const SECTION_125           = 7;
    const TAX_FILING            = 8;

    static protected $set = [
        self::DIRECT_DEPOSIT    => 'Direct Deposit',
        self::PAY_CARDS         => 'Pay Cards',
        self::CHECK_SIGNING     => 'Check Signing',
        self::CHECK_STUFFING    => 'Check Stuffing',
        self::A401K             => '401(k) plan administration',
        self::ONLINE_ACCESS     => 'Internet‑enabled access',
        self::SECTION_125       => 'Section 125/cafeteria plan administration',
        self::TAX_FILING        => 'Tax filing services',
    ];
}

