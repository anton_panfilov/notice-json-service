<?php

class DataSet_Value_RoofShade extends DataSet_Value_Set {
    const NONE       = 1; // нет теней
    const A_LITTLE   = 2; // немного
    const A_LOT      = 3; // много
    const UNKNOWN    = 4; // не знаю


    static protected $set = [
        self::NONE     => 'None',
        self::A_LITTLE => 'A Little',
        self::A_LOT    => 'A Lot',
        self::UNKNOWN  => 'Unknown',
    ];
}

