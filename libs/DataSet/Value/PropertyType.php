<?php

class DataSet_Value_PropertyType extends DataSet_Value_Set {
    const COMMERCIAL    = 1;
    const RESIDENTIAL   = 2;


    static protected $set = [
        self::COMMERCIAL  => 'Commercial',
        self::RESIDENTIAL => 'Residential',
    ];
}

