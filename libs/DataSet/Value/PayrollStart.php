<?php

class DataSet_Value_PayrollStart extends DataSet_Value_Set {
    const ASAP          = 1;
    const WEEKS_2       = 2;
    const MONTH         = 3;
    const MONTHS_2      = 4;
    const OTHER         = 5;

    static protected $set = [
        self::ASAP      => 'ASAP',
        self::WEEKS_2   => 'In Two Weeks',
        self::MONTH     => 'In One Month',
        self::MONTHS_2  => 'In Two Months',
        self::OTHER     => 'Other',
    ];
}

