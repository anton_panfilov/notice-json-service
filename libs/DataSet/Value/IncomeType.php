<?php

class DataSet_Value_IncomeType extends DataSet_Value_Set {
    const EMPLOYEE          = 1;    // Штатный сотрудник
    const INDIVIDUAL_OWNER  = 2;    // Предприниматель
    const STUDENT           = 3;    // Студент
    const PENSIONER         = 4;    // Пенсионер
    const UNEMPLOYED        = 5;    // Безработный

    static protected $set = [
        self::EMPLOYEE          => 'Штатный сотрудник',
        self::INDIVIDUAL_OWNER  => 'Предприниматель',
        self::STUDENT           => 'Студент',
        self::PENSIONER         => 'Пенсионер',
        self::UNEMPLOYED        => 'Безработный',
    ];
}

