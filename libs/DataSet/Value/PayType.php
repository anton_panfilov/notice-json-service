<?php

class DataSet_Value_PayType extends DataSet_Value_Set {
    const HOURLY        = 1;
    const SALARY        = 2;
    const MIXED         = 3;

    static protected $set = [
        self::HOURLY    => 'Hourly',
        self::SALARY    => 'Salary',
        self::MIXED     => 'Mixed',
    ];
}

