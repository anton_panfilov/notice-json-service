<?php

abstract class DataSet_Value_Set {
    static protected $set = [];

    /**
     * Полуичть массив ключей
     *
     * @return array
     */
    static public function getKeys(){
        return array_keys(static::$set);
    }

    /**
     * Полуичть массив ключей с лейблами
     *
     * @return array
     */
    static public function getLabels(){
        return static::$set;
    }
}