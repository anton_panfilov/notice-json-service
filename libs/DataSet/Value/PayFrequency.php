<?php

class DataSet_Value_PayFrequency extends DataSet_Value_Set {
    const WEEKLY            = 1;
    const BIWEEKLY          = 2;
    const SEMIMONTHLY       = 3;
    const MONTHLY           = 4;

    static protected $set = [
        self::WEEKLY        => 'Weekly',
        self::BIWEEKLY      => 'Biweekly',
        self::SEMIMONTHLY   => 'Semimonthly',
        self::MONTHLY       => 'Monthly',
    ];
}

