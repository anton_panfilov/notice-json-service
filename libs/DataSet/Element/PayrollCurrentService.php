<?php

class DataSet_Element_PayrollCurrentService extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PayrollCurrentService::getKeys());
        $this->setLabel("Current Service");
    }

    public function to($in_house, $accounting_bookkeeping, $payroll, $hr, $na, $other){
        return $this->toAbstract([
            DataSet_Value_PayrollCurrentService::IN_HOUSE               => $in_house,
            DataSet_Value_PayrollCurrentService::ACCOUNTING_BOOKKEEPING => $accounting_bookkeeping,
            DataSet_Value_PayrollCurrentService::PAYROLL                => $payroll,
            DataSet_Value_PayrollCurrentService::HR                     => $hr,
            DataSet_Value_PayrollCurrentService::NA                     => $na,
            DataSet_Value_PayrollCurrentService::OTHER                  => $other,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PayrollCurrentService::getLabels()[$this->getValue()]) ?
            DataSet_Value_PayrollCurrentService::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PayrollCurrentService::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}