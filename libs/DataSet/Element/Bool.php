<?php

class DataSet_Element_Bool extends DataSet_Element_String {
    protected $_docType = 'Bool';

    public function __construct(){
        $this->addFilter(new Filter_Trim());
        $this->addFilter(new Filter_BoolInt());
        $this->addValidator(new Validator_InArray(['1', '0']));
    }

    public function to($yes, $no, $unknown = null){
        if($this->getValue() == '1') return $yes;
        if($this->getValue() == '0') return $no;

        return $unknown;
    }

    public function getValueString(){
        return $this->getValue() ? "Yes" : "No";
    }

    protected function _renderDocumentationDescription(){
        return '<ul style="list-style-type:circle;">
                <li>1 <span style="color: #CCC">или</span> 0</li>
                <li>yes <span style="color: #CCC">или</span> no</li>
                <li>true <span style="color: #CCC">или</span> false</li>
            </ul>';
    }
}