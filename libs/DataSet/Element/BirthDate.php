<?php

class DataSet_Element_BirthDate extends DataSet_Element_String {
    protected $_docType = 'Date';

    public function __construct($min = null, $max = null){
        $this->addFilter(new Filter_Trim());
        $this->addValidator(new Validator_YearsOld($min, $max));
    }

    public function getAge(){
        $value = $this->getValue();

        if(strlen($value) == 10){
            list($year, $month, $day) = array(substr($value, 0, 4), substr($value, 5, 2), substr($value, 8, 2));
            if(
                $year  === sprintf("%04d", (int)$year) &&
                $month === sprintf("%02d", (int)$month) &&
                $day   === sprintf("%02d", (int)$day)
            ){
                if(checkdate($month, $day, $year)){
                    list($currentYear, $currentMonth, $currentDay) = sscanf(date("Y-m-d"), '%d-%d-%d');
                    $age = $currentYear - $year;
                    if($currentMonth < $month || ($currentMonth == $month && $currentDay < $day)){
                        $age--;
                    }

                    return $age;
                }
            }
        }

        return null;
    }

    /**
     * Дата в формает DD.MM.YYYY
     *
     * @return string
     */
    public function formatDMY(){
        if(strlen($this->getValue())){
            return substr($this->getValue(), 8, 2) . "." . substr($this->getValue(), 5, 2) . "." . substr($this->getValue(), 0, 4);
        }
        return "";
    }

    public function getValueString(){
        return (new Decorator_Date())
            ->render($this->getValue());
    }

    protected function _renderDocumentationDescription(){
        $add = '';
        if(isset($this->validators['Validator_YearsOld'])){
            $s = $this->validators['Validator_YearsOld']->getSettings();
            if(strlen($s['min']) && strlen($s['max'])){
                $add = "<p>Диапозон возраста: {$s['min']} - {$s['max']} лет</p>";
            }
            else if(strlen($s['min'])){
                $add = "<p>Минимальный возраст: {$s['min']} лет</p>";
            }
            else if(strlen($s['max'])){
                $add = "<p>Максимальный возраст: {$s['max']} лет</p>";
            }
        }

        return "<p>Дата Рождения в формате YYYY-MM-DD</p>" . $add;
    }
}