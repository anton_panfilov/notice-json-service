<?php

class DataSet_Element_NumericAmount extends DataSet_Element_Numeric {
    protected $_docType = 'int';

    public function __construct($min = null, $max = null){
        $this->addFilterFunction(function($value){
            $value = trim($value);
            if(substr($value, -3, 1) == '.' && is_numeric(substr($value, -2, 2))){
                $value =substr($value, 0, -3);
            }
            return $value;
        });
        $this->addFilter(new Filter_NumbersOnly());

        if(!is_null($min) || !is_null($max)){
            $this->addValidator(new Validator_NumericBetween($min, $max));
        }
    }
}