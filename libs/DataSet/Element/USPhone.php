<?php

class DataSet_Element_USPhone extends DataSet_Element_Numeric {
    public function __construct(){
        $this->addFilter(new Filter_RUMobilePhoneRepair());
        $this->addValidator(new Validator_Length(10, 10));
        $this->addValidator(new Validator_NumericBetween(2000000000));
    }

    public function getValueString(){
        if(strlen($this->getValue()) == 10){
            return sprintf("%s.%s.%s",
                substr($this->getValue(), 0, 3),
                substr($this->getValue(), 3, 3),
                substr($this->getValue(), 6, 4)
            );
        }
        return $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        return "
            <p>10 digits. USA Phone Number</p>
            <p>Example: 8182007000</p>
        ";
    }
}