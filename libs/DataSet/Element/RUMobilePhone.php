<?php

class DataSet_Element_RUMobilePhone extends DataSet_Element_Numeric {
    public function __construct(){
        $this->addFilter(new Filter_RUMobilePhoneRepair());
        $this->addValidator(new Validator_Length(10, 10));
        $this->addValidator(new Validator_NumericBetween(9000000000));
    }

    public function getValueString(){
        if(strlen($this->getValue()) == 10){
            return sprintf("+7.%s.%s.%s",
                substr($this->getValue(), 0, 3),
                substr($this->getValue(), 3, 3),
                substr($this->getValue(), 6, 4)
            );
        }
        return $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        return "
            <p>10 цифор. Номер мобильного телефона, зарегистрированный в РФ.</p>
            <p>Пример: 9002007000</p>
        ";
    }
}