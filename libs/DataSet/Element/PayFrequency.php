<?php

class DataSet_Element_PayFrequency extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PayFrequency::getKeys());
        $this->setLabel("Pay Frequency");
    }

    public function to($weekly, $biweekly, $semimonthly, $monthly){
        return $this->toAbstract([
            DataSet_Value_PayFrequency::WEEKLY      => $weekly,
            DataSet_Value_PayFrequency::BIWEEKLY    => $biweekly,
            DataSet_Value_PayFrequency::SEMIMONTHLY => $semimonthly,
            DataSet_Value_PayFrequency::MONTHLY     => $monthly,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PayFrequency::getLabels()[$this->getValue()]) ?
            DataSet_Value_PayFrequency::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PayFrequency::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}