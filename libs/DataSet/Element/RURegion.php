<?php

class DataSet_Element_RURegion extends DataSet_Element_String {
    protected $_docType = 'digits';

    public function __construct(){
        $this->addFilter(new Filter_NumbersOnly());
        $this->addFilter(new Filter_Region());
        $this->addValidator(new Validator_InArray(Geo::getRegionsCodes(), 'Invalid Region code'));
    }

    /**
     * Название региона + (его номер)
     *
     * @return mixed|string
     */
    public function getValueString(){
        return isset(Geo::getRegions()[$this->getValue()]) ?
            (Geo::getRegions()[$this->getValue()] . " ({$this->getValue()})") :
            $this->getValue();
    }

    /**
     * Название региона
     *
     * @return string or int
     */
    public function getRegionString(){
        return isset(Geo::getRegions()[$this->getValue()]) ?
            Geo::getRegions()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(Geo::getRegions() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return
            "<p>Числовой код региона по базе <a href='http://fias.nalog.ru/Public/NewsPage.aspx' target='_blank'>ФИАС</a></p>" .
            "<div style='float:left; width: 300px'>" . implode("</div><div style='float:left; width: 300px'>", $result) . "</div>";
    }
}