<?php

class DataSet_Element_Date extends DataSet_Element_String {
    protected $_docType = 'Date';

    public function __construct(){
        $this->addFilter(new Filter_Trim());
        $this->addValidator(new Validator_Date());
    }

    public function getValueString(){
        return (new Decorator_Date())->render($this->getValue());
    }

    protected function _renderDocumentationDescription(){
        return "<p>Дата в формате YYYY-MM-DD</p>";
    }

    /**
     * Дата в формает DD.MM.YYYY
     *
     * @return string
     */
    public function formatDMY(){
        return substr($this->getValue(), 8, 2) . "." . substr($this->getValue(), 5, 2) . "." . substr($this->getValue(), 0, 4);
    }
}