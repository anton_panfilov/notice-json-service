<?php

class DataSet_Element_PassportUnitCode extends DataSet_Element_Digits {
    public function __construct($min = null, $max = null){
        parent::__construct(6, 6);
    }

    /**
     * @return string XXX-XXX or ""
     */
    public function formatStandard(){
        if(strlen($this->value) == 6){
            return substr($this->value, 0, 3) . "-" . substr($this->value, 3, 3);
        }
        return "";
    }

    /**
     * @return string XXX-XXX or ""
     */
    public function getValueString(){
        return $this->formatStandard();
    }
}