<?php

class DataSet_Element_PayrollStart extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PayrollStart::getKeys());
        $this->setLabel("Start");
    }

    public function to($asap, $weeks_2, $month, $months_2, $other){
        return $this->toAbstract([
            DataSet_Value_PayrollStart::ASAP     => $asap,
            DataSet_Value_PayrollStart::WEEKS_2  => $weeks_2,
            DataSet_Value_PayrollStart::MONTH    => $month,
            DataSet_Value_PayrollStart::MONTHS_2 => $months_2,
            DataSet_Value_PayrollStart::OTHER    => $other,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PayrollStart::getLabels()[$this->getValue()]) ?
            DataSet_Value_PayrollStart::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PayrollStart::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}