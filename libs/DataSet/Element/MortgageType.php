<?php

class DataSet_Element_MortgageType extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_MortgageType::getKeys());
    }

    public function to($refinance, $new_home){
        return $this->toAbstract([
            DataSet_Value_MortgageType::REFINANCE   => $refinance,
            DataSet_Value_MortgageType::NEW_HOME    => $new_home,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_MortgageType::getLabels()[$this->getValue()]) ?
            DataSet_Value_MortgageType::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_MortgageType::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}