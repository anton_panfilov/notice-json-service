<?php

class DataSet_Element_RUName extends DataSet_Element_String {
    public function __construct(){
        $this
            ->addFilter(new Filter_NameRepair())
            ->addValidator(new Validator_Length(2, 128));
    }

    protected function _renderDocumentationDescription(){
        return "
            <p>Может быть написано только русскими буквами</p>
        ";
    }
}