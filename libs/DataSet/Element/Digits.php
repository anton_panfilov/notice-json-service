<?php

class DataSet_Element_Digits extends DataSet_Element_String {
    protected $_docType = 'digits';

    public function __construct($min = null, $max = null){
        $this->addFilter(new Filter_NumbersOnly());

        if(!is_null($min) || !is_null($max)){
            $this->addValidator(new Validator_Length($min, $max));
        }
    }

    protected function _renderDocumentationDescription(){
        $return = "Набор цифр. ";

        if(isset($this->validators['Validator_Length'])){
            $s = $this->validators['Validator_Length']->getSettings();
            if(strlen($s['min']) && strlen($s['max'])){
                if($s['min'] != $s['max']){
                    $return.= "<span style='color:#AAA'>Количество:</span> " .
                        $s['min'] . " - " . $s['max'];
                }
                else {
                    $return.= "<span style='color:#AAA'>Количество:</span> " . $s['min'];
                }
            }
            else if(strlen($s['min'])){
                $return.= "<span style='color:#AAA'>Минимальное количество:</span> " . $s['min'];
            }
            else if(strlen($s['max'])){
                $return.= "<span style='color:#AAA'>Максимальное количество:</span> " . $s['max'];
            }
        }

        return $return;
    }
}