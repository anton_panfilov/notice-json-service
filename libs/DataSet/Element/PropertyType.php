<?php

class DataSet_Element_PropertyType extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PropertyType::getKeys());
    }

    public function to($commercial, $residential){
        return $this->toAbstract([
            DataSet_Value_PropertyType::COMMERCIAL  => $commercial,
            DataSet_Value_PropertyType::RESIDENTIAL => $residential,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PropertyType::getLabels()[$this->getValue()]) ?
            DataSet_Value_PropertyType::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PropertyType::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}