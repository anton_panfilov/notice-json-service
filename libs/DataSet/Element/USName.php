<?php

class DataSet_Element_USName extends DataSet_Element_String {
    public function __construct(){
        $this
            ->addFilter(new Filter_USNameRepair())
            ->addValidator(new Validator_Length(2, 128));
    }

    protected function _renderDocumentationDescription(){
        return "
            <p>Only Latin Symbols</p>
        ";
    }
}