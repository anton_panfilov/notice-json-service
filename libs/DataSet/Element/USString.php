<?php

class DataSet_Element_USString extends DataSet_Element_String {
    public function __construct(){
        $this
            ->addFilter(new Filter_USStringRepair())
            ->addValidator(new Validator_Length(1, 255))
            ->setLabel("Address");
    }

    protected function _renderDocumentationDescription(){
        return "";
    }
}