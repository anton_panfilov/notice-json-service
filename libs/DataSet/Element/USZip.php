<?php

class DataSet_Element_USZip extends DataSet_Element_String {
    public function __construct(){
        $this
            ->addFilter(new Filter_USZipRepair())
            ->addValidator(new Validator_Length(5, 5))
            ->setLabel("ZIP");
    }

    protected function _renderDocumentationDescription(){
        return "5 Digits. Example: 01234, 98765";
    }
}