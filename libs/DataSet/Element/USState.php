<?php

class DataSet_Element_USState extends DataSet_Element_String {
    protected $_docType = 'string';

    public function __construct(){
        $this->addFilter(new Filter_USStateRepair());
        $this->addValidator(new Validator_InArray(Geo::getStatesCodesList(), 'Invalid State'));
        $this->setLabel("State");
    }

    /**
     * Название региона + (его номер)
     *
     * @return mixed|string
     */
    public function getValueString(){
        return isset(Geo::getStatesList()[$this->getValue()]) ?
            (Geo::getStateTitle($this->getValue()) . " ({$this->getValue()})") :
            $this->getValue();
    }

    /**
     * Название региона
     *
     * @return string or int
     */
    public function getRegionString(){
        return isset(Geo::getStatesList()[$this->getValue()]) ?
            Geo::getStateTitle($this->getValue()) :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(Geo::getStatesList() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        $style = "float:left; width: 200px";
        return "<p style='{$style}'>" . implode("</p><p style='{$style}'>", $result) . "</p>";
    }
}