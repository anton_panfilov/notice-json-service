<?php

class DataSet_Element_PayType extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PayType::getKeys());
        $this->setLabel("Pay Type");
    }

    public function to($hourly, $salary, $mixed){
        return $this->toAbstract([
            DataSet_Value_PayType::HOURLY => $hourly,
            DataSet_Value_PayType::SALARY => $salary,
            DataSet_Value_PayType::MIXED  => $mixed,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PayType::getLabels()[$this->getValue()]) ?
            DataSet_Value_PayType::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PayType::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}