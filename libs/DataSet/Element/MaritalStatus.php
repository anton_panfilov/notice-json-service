<?php

class DataSet_Element_MaritalStatus extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_MaritalStatus::getKeys());
    }

    public function to($married, $civil_marriage, $divorced, $single){
        return $this->toAbstract([
            DataSet_Value_MaritalStatus::MARRIED         => $married,
            DataSet_Value_MaritalStatus::CIVIL_MARRIAGE  => $civil_marriage,
            DataSet_Value_MaritalStatus::DIVORCED        => $divorced,
            DataSet_Value_MaritalStatus::SINGLE          => $single,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_MaritalStatus::getLabels()[$this->getValue()]) ?
            DataSet_Value_MaritalStatus::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_MaritalStatus::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}