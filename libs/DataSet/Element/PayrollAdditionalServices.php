<?php

class DataSet_Element_PayrollAdditionalServices extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_PayrollAdditionalServices::getKeys());
    }

    public function to($direct_deposit, $pay_cards, $check_signing, $check_stuffing, $a401k, $online_access, $section_125, $tax_filing){
        return $this->toAbstract([
            DataSet_Value_PayrollAdditionalServices::DIRECT_DEPOSIT => $direct_deposit,
            DataSet_Value_PayrollAdditionalServices::PAY_CARDS      => $pay_cards,
            DataSet_Value_PayrollAdditionalServices::CHECK_SIGNING  => $check_signing,
            DataSet_Value_PayrollAdditionalServices::CHECK_STUFFING => $check_stuffing,
            DataSet_Value_PayrollAdditionalServices::A401K          => $a401k,
            DataSet_Value_PayrollAdditionalServices::ONLINE_ACCESS  => $online_access,
            DataSet_Value_PayrollAdditionalServices::SECTION_125    => $section_125,
            DataSet_Value_PayrollAdditionalServices::TAX_FILING     => $tax_filing,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_PayrollAdditionalServices::getLabels()[$this->getValue()]) ?
            DataSet_Value_PayrollAdditionalServices::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_PayrollAdditionalServices::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}