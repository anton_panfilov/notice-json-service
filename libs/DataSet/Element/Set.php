<?php

class DataSet_Element_Set extends DataSet_Element_String {
    protected $_docType = 'Set';
    protected $array;

    public function __construct($array){
        $this->array = $array;
        $this->addFilter(new Filter_Trim());
        $this->addFilter(new Filter_CaseFromArray($array));
        $this->addValidator(new Validator_InArray($array));
    }

    protected function toAbstract($map, $default = null){
        return isset($map[$this->getValue()]) ? $map[$this->getValue()] : $default;
    }

    protected function _renderDocumentationDescription(){
        if(isset($this->validators['Validator_InArray'])){
            return implode(", ", $this->validators['Validator_InArray']->getSettings()['array']);
        }
        return "";
    }
}