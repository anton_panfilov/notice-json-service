<?php

class DataSet_Element_Gender extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_Gender::getKeys());
    }

    public function to($male, $female, $unknown = null){
        return $this->toAbstract([
            DataSet_Value_Gender::MALE    => $male,
            DataSet_Value_Gender::FEMALE  => $female,
        ], $unknown);
    }

    public function getValueString(){
        return isset(DataSet_Value_Gender::getLabels()[$this->getValue()]) ?
            DataSet_Value_Gender::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_Gender::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}