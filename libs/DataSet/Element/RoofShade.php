<?php

class DataSet_Element_RoofShade extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_RoofShade::getKeys());
    }

    public function to($none, $little, $lot, $unknown){
        return $this->toAbstract([
            DataSet_Value_RoofShade::NONE     => $none,
            DataSet_Value_RoofShade::A_LITTLE => $little,
            DataSet_Value_RoofShade::A_LOT    => $lot,
            DataSet_Value_RoofShade::UNKNOWN  => $unknown,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_RoofShade::getLabels()[$this->getValue()]) ?
            DataSet_Value_RoofShade::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_RoofShade::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}