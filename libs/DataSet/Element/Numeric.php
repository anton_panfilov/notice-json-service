<?php

class DataSet_Element_Numeric extends DataSet_Element_String {
    protected $_docType = 'int';

    public function __construct($min = null, $max = null){
        $this->addFilter(new Filter_NumbersOnly());

        if(!is_null($min) || !is_null($max)){
            $this->addValidator(new Validator_NumericBetween($min, $max));
        }
    }

    protected function _renderDocumentationDescription(){
        if(isset($this->validators['Validator_NumericBetween'])){
            $s = $this->validators['Validator_NumericBetween']->getSettings();
            if(strlen($s['min']) && strlen($s['max'])){
                return "<span style='color:#AAA'>Диапозон допустимых значений:</span> " .
                    number_format($s['min']) . " - " . number_format($s['max']);
            }
            else if(strlen($s['min'])){
                return "<span style='color:#AAA'>Минимум:</span> " . number_format($s['min']);
            }
            else if(strlen($s['max'])){
                return "<span style='color:#AAA'>Максимум:</span> " . number_format($s['max']);
            }
        }

        return "";
    }

    /**
     * Привести число к списку фиксорованных значений
     * Например 1000, 2000, 3000, 4000, 5000
     *
     * @param $array
     * @return mixed
     */
    public function mapValues($array) {
        if(func_num_args() > 1) $array = func_get_args();
        if(!is_array($array))   $array = [$array];

        sort($array, SORT_NUMERIC);

        Value::export($array);

        foreach ($array as $v) {
            if($v >= $this->getValue()){
                return $v;
            }
        }

        return end($array);
    }

    /**
     * Привести число к списку фиксорованных значений, где значения не равны цифрам
     *
     * Например:
     *  $1,000 - $2,000
     *  $2,000 - $3,000
     *  $3,000 - $4,000
     *  $4,000 - $5,000
     *
     * @param array $array Пример: [2000 => "$1,000 - $2,000", 3000 => "$2,000 - $3,000"]
     * @return mixed
     */
    public function mapValuesLabels($array) {
        if(func_num_args() > 1) $array = func_get_args();
        if(!is_array($array))   $array = [$array];

        ksort($array, SORT_NUMERIC);

        foreach ($array as $k => $v) {
            if($k >= $this->getValue()){
                return $v;
            }
        }

        return end($array);
    }
}