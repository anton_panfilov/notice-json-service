<?php

abstract class DataSet_Element_Abstract {
    protected $_docType = 'unknown';
    protected $_docDescription = null;
    protected $_docDescriptionAdd = null;

    protected $label;
    protected $value;

    /**
     * @var bool
     */
    protected $required = true;

    /**
     * @var Validator_Abstract[]
     */
    protected $validators   = [];

    /**
     * @var Filter_Abstract[]
     */
    protected $filters      = [];

    /********************************************/

    public function setValue($value, $context = []){
        if($value == '' && !$this->isRequired()){
            $this->value = null;
            return $this;
        }

        if(count($this->filters)){
            foreach($this->filters as $filter){
                $value = $filter->filter($value, $context);
            }
        }

        $this->value = $value;
        return $this;
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setRequired($flag = true){
        $this->required = (bool)$flag;
        return $this;
    }

    /**
     * Получить реальное значение (может быть массивом, строкой, null и т.д.) Используеться для сериализации
     *
     * @return mixed
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isRequired(){
        return $this->required;
    }

    /**
     * Получить значение в виде строки.
     * Используеться для предтсавления на страницу
     * Когда значение сложное, то эту функцию надо переопределять и приводить значение к строке
     *
     * @return string
     */
    public function getValueString(){
        return (string)$this->getValue();
    }

    public function __toString(){
        return (string)$this->getValueString();
    }

    /********************************************/

    /**
     * Установить название поля для документации
     *
     * @param $label
     * @return $this
     */
    public function setLabel($label){
        $this->label = $label;
        return $this;
    }

    public function getLabel(){
        return $this->label;
    }

    /********************************************/

    /**
     * @param Filter_Abstract $filter
     * @return $this
     */
    public function addFilter(Filter_Abstract $filter){
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * @param callable $function
     * @return $this
     */
    public function addFilterFunction(Closure $function){
        return $this->addFilter(new Filter_Function($function));
    }

    /********************************************/

    /**
     * Добавление валидатора
     * Если добавляеться несколько одинаковых валидаторов, то второсу надо присваивать уникальный индекс.
     * Это необходимо для возможности составления переводов, что бы было понятно к какому валидатору относиться перевод
     *
     * @param Validator_Abstract $validator
     * @param null $index
     * @return $this
     * @throws Exception
     */
    public function addValidator(Validator_Abstract $validator, $index = null){
        if(is_null($index)){
            $index = get_class($validator);
        }

        if(isset($this->validators[$index])){
            throw new Exception("Validator index `{$index}` already use");
        }

        $this->validators[$index] = $validator;

        return $this;
    }

    /********************************************/

    /**
     * @param $context
     * @return DataSet_Result_ValidationElement
     */
    public function isValid($context){
        $result = new DataSet_Result_ValidationElement();

        if($this->value == '' && !$this->isRequired()){
            return $result;
        }

        if(count($this->validators)){
            foreach($this->validators as $vCode => $validator){
                $res = $validator->isValid($this->value, $context);

                if(!$res){
                    $errors = $validator->getErrors();
                    if(count($errors)){
                        foreach($errors as $error){
                            $result->addError(
                                $vCode . "." . $error['code'],
                                $error['message'],
                                $error['values']
                            );
                        }
                    }
                    else {
                        $result->setError();
                    }

                    break;
                }
            }
        }

        return $result;
    }

    public function getTranslateMap(){
        $result = [];

        if(count($this->validators)){
            foreach($this->validators as $k => $el){
                if(count($el->getTranslateMap())){
                    foreach($el->getTranslateMap() as $valID => $valMessage){
                        $result["{$k}.{$valID}"] = $valMessage;
                    }
                }
            }
        }

        return $result;
    }

    /*****************************************************************/

    /**
     * Устанвока текста комментарий дял документации
     * Если не уставнолен, то буде тпо умолчанию для этого типа
     * А установленный перебивает тот что есть
     *
     * @param $text
     * @return $this
     */
    public function setDocDescription($text){
        $this->_docDescription = $text;
        return $this;
    }

    /**
     * Дополнение к документации
     *
     * @param $text
     * @return $this
     */
    public function setDocDescriptionAdd($text){
        $this->_docDescriptionAdd = $text;
        return $this;
    }

    public function _renderDocumentation($name, $path){
        ob_start();
        ?>
        <tr>
            <td><span style="color: #888"><?=$path?>.</span><?=$name?></td>
            <td><?if($this->isRequired()){?><span class="label label-success"><?=Translate::t("Required")?></span><?}?></td>
            <td><b><?=$this->_docType?></b></td>
            <td><?
                if(strlen($this->getLabel())){
                     echo "<p><b>" . $this->getLabel() . "</b></p>";
                }

                if(strlen($this->_docDescription)){
                    echo $this->_docDescription;
                }
                else {
                    echo $this->_renderDocumentationDescription();
                }

                if(strlen($this->_docDescriptionAdd)){
                    echo " " . $this->_docDescriptionAdd;
                }
            ?></td>
        </tr>
        <?
        return ob_get_clean();
    }

    protected function _renderDocumentationDescription(){
        return '';
    }
}