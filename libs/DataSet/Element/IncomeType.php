<?php

class DataSet_Element_IncomeType extends DataSet_Element_Set {
    protected $_docType = 'int';

    public function __construct(){
        parent::__construct(DataSet_Value_IncomeType::getKeys());
    }

    public function to($employee, $individual_owner, $pensioner, $student, $unemployed){
        return $this->toAbstract([
            DataSet_Value_IncomeType::EMPLOYEE          => $employee,
            DataSet_Value_IncomeType::INDIVIDUAL_OWNER  => $individual_owner,
            DataSet_Value_IncomeType::PENSIONER         => $pensioner,
            DataSet_Value_IncomeType::STUDENT           => $student,
            DataSet_Value_IncomeType::UNEMPLOYED        => $unemployed,
        ]);
    }

    public function getValueString(){
        return isset(DataSet_Value_IncomeType::getLabels()[$this->getValue()]) ?
            DataSet_Value_IncomeType::getLabels()[$this->getValue()] :
            $this->getValue();
    }

    protected function _renderDocumentationDescription(){
        $result = [];

        foreach(DataSet_Value_IncomeType::getLabels() as $k => $v){
            $result[] = "{$k} - <span style='color: #AAA'>{$v}</span>";
        }

        return "<p>" . implode("</p><p>", $result) . "</p>";
    }
}