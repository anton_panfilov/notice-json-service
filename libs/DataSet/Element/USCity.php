<?php

class DataSet_Element_USCity extends DataSet_Element_String {
    public function __construct(){
        $this
            ->addFilter(new Filter_USCityRepair())
            ->addValidator(new Validator_Length(2, 128))
            ->setLabel("City");
    }

    protected function _renderDocumentationDescription(){
        return "US City. Example: Los Angeles";
    }
}