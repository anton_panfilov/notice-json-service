<?php

class DataSet_Element_Email extends DataSet_Element_String {
    public function __construct(){
        $this->addFilter(new Filter_Trim());
        $this->addValidator(new Validator_Email());
    }
}