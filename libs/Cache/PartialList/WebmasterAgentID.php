<?php

class Cache_PartialList_WebmasterAgentID extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT `id`, `agent` FROM `webmaster_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}