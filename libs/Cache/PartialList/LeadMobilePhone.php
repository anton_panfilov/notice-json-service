<?php

class Cache_PartialList_LeadMobilePhone extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::logs()->fetchPairs(
            "SELECT id, `mobile_phone` FROM `lead_search_by_contacts` WHERE `id` in ('" . implode("','", $ids) . "')"
        );
    }
}