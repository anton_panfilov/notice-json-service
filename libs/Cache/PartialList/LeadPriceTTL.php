<?php

class Cache_PartialList_LeadPriceTTL extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::logs()->fetchPairs(
            "SELECT id, money_ttl FROM `lead_data_head` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}