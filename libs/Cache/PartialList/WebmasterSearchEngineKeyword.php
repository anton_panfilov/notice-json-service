<?php

class Cache_PartialList_WebmasterSearchEngineKeyword extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `keyword` FROM `webmaster_referer_keywords` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}