<?php

class Cache_PartialList_WebmasterDomain extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `domain` FROM `webmaster_domains` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}