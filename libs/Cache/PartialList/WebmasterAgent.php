<?php

class Cache_PartialList_WebmasterAgent extends Cache_PartialList_Abstract {
    static protected function select($ids){
        $res = [];
        $data = Db::processing()->fetchAll(
            "select `id`, `agent` from `webmaster_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );

        if(count($data)){
            $users = [];
            foreach($data as $el){
                $users[] = $el['agent'];
            }
            Cache_PartialList_UserLogin::load($users);

            foreach($data as $el){
                $res[$el['id']] = Cache_PartialList_UserLogin::get($el['agent']);

                if(UserAccess::isAdmin()){
                    $res[$el['id']] = "<a href='/account/users/user/webmaster-agent?id={$res[$el['id']]}'>{$res[$el['id']]}</a>";
                }
            }
        }

        return $res;
    }
}