<?php

class Cache_PartialList_WebmasterDomainPage extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `page` FROM `webmaster_domains_pages` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}