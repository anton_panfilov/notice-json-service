<?php

class Cache_PartialList_WebmasterChannel extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `label` from `webmaster_channels_server_post` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}