<?php

class Cache_PartialList_LeadVerification extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::site()->fetchPairs(
            "select `id`, `status` from `call_center_leads_verification` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}