<?php

class Cache_PartialList_LeadEmail extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::logs()->fetchPairs(
            "SELECT id, `email` FROM `lead_search_by_contacts` WHERE `id` in ('" . implode("','", $ids) . "')"
        );
    }
}