<?php

class Cache_PartialList_WebmasterName extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `name` from `webmaster_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}