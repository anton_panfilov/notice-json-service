<?php

class Cache_PartialList_LeadID extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::logs()->fetchPairs(
            "SELECT id, CONCAT(`num`, '.', `webmaster`) FROM `lead_ids_index` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}