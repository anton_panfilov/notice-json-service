<?php

class Cache_PartialList_LeadSalesRedirects extends Cache_PartialList_Abstract {
    static protected function select($ids){
        $index = Db::logs()->fetchUnique(
            "select lead, sold, createRedirect, goodRedirect from lead_sold_redirect_cache where lead in (" . implode(",", $ids) . ")"
        );

        $result = [];
        $postings = [];

        foreach($ids as $lead){
            $result[$lead] = [];

            if(isset($index[$lead]) && strlen(trim($index[$lead]['sold'], ","))){
                $soldArray      = array_unique(explode(',', substr($index[$lead]['sold'],           0, -1)));
                $createArray    = array_unique(explode(',', substr($index[$lead]['createRedirect'], 0, -1)));
                $goodArray      = array_unique(explode(',', substr($index[$lead]['goodRedirect'],   0, -1)));

                foreach($soldArray as $posting){
                    if(in_array($posting, $goodArray)){
                        $result[$lead][$posting] = "good";
                    }
                    else if(in_array($posting, $createArray)){
                        $result[$lead][$posting] = "create";
                    }
                    else {
                        $result[$lead][$posting] = "sold";
                    }

                    $postings[] = $posting;
                }
            }
        }

        Cache_PartialList_PostingFullName::load($postings);

        return $result;
    }

    static public function get($id){
        $result = parent::get($id);
        $full = UserAccess::isAdmin() || UserAccess::isAgentBuyer();

        if(count($result)){
            if(!$full){
                $create = 0;
                $good = 0;
                foreach($result as $type){
                    if($type == 'good'){
                        $good++;
                    }
                    else if($type == 'create'){
                        $create++;
                    }
                }

                if($create || $good){
                    if($create > 0){
                        if($good + $create == 1){
                            return "<b style='color:#F30'>No</b>";
                        }
                        else {
                            return "<b style='color:#F30'>No:</b> <b>" . ($good + $create) . "</b> / <span style='color:#F30'>{$good}</span>";
                        }
                    }
                    else {
                        if($good > 1){
                            return "<b style='color:#090'>All Yes</b> ({$good})";
                        }
                        else {
                            return "<b style='color:#090'>Yes</b>";
                        }
                    }
                }
                else {
                    return "<span style='color:#CCC'>-</span>";
                }
            }
            else {
                $classes = [
                    'good'      => 'text-success',
                    'create'    => 'text-danger',
                    'sold'      => 'text-muted',
                ];
                $buyers = [];
                foreach($result as $buyer => $type){
                    $buyers[] = "<span class='" . $classes[$type] . "'>" .
                        Cache_PartialList_PostingFullName::get($buyer) .
                    "</span>";
                }
                return implode(", ", $buyers);
            }
        }
        else {
            return "<span style='color:#CCC'>-</span>";
        }
    }
}