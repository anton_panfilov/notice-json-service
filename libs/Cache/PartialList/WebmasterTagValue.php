<?php

class Cache_PartialList_WebmasterTagValue extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `value` FROM `webmaster_tags_values` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}