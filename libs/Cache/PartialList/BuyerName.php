<?php

class Cache_PartialList_BuyerName extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `name` from `buyers_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}