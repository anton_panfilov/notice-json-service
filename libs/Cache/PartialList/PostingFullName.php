<?php

class Cache_PartialList_PostingFullName extends Cache_PartialList_Abstract {
    static protected function select($ids){
        $res = [];
        $data = Db::processing()->fetchAll(
            "select `id`, `buyer`, `name` from `buyers_channels` where `id` in ('" . implode("','", $ids) . "')"
        );

        if(count($data)){
            $buyers = [];
            foreach($data as $el){
                $buyers[] = $el['buyer'];
            }
            Cache_PartialList_BuyerName::load($buyers);

            foreach($data as $el){
                $res[$el['id']] = Cache_PartialList_BuyerName::get($el['buyer']) . " : " . $el['name'];
            }
        }

        return $res;
    }
}