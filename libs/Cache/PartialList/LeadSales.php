<?php

class Cache_PartialList_LeadSales extends Cache_PartialList_Abstract {
    static protected function select($ids){
        $result = [];
        $postings = [];

        $temp = Db::logs()->fetchUnique(
            "SELECT lead, buyer, posting FROM `lead_sender_sales_postings` WHERE lead IN ('" . implode("','", $ids) . "')"
        );

        if(count($temp)){
            foreach($temp as $el){
                $result[$el['lead']][] = [
                    'buyer' => $el['buyer'],
                    'posting' => $el['posting'],
                ];
                $postings[] = $el['posting'];
            }
        }

        foreach($ids as $id){
            if(!isset($result[$id])){
                $result[$id] = [];
            }
        }

        if(count($postings)){
            $postings = array_unique($postings);
            Cache_PartialList_PostingFullName::load($postings);
        }

        return $result;
    }
}