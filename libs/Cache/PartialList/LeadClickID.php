<?php

class Cache_PartialList_LeadClickID extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::logs()->fetchPairs(
            "SELECT id, `click_id` FROM `lead_search_click_id` WHERE `id` in ('" . implode("','", $ids) . "')"
        );
    }
}