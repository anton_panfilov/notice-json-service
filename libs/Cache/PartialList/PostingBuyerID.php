<?php

class Cache_PartialList_PostingBuyerID extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `buyer` from `buyers_channels` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}