<?php

class Cache_PartialList_BuyerStatus extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `status` from `buyers_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}