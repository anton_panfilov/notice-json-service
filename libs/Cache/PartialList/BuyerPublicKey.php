<?php

class Cache_PartialList_BuyerPublicKey extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "select `id`, `public_key` from `buyers_accounts` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}