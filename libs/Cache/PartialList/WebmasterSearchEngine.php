<?php

class Cache_PartialList_WebmasterSearchEngine extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `search_engine` FROM `webmaster_referer_search_engines` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}