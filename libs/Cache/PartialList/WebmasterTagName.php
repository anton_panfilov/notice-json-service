<?php

class Cache_PartialList_WebmasterTagName extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::processing()->fetchPairs(
            "SELECT id, `name` FROM `webmaster_tags_names` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}