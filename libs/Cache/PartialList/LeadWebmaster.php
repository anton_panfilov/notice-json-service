<?php

class Cache_PartialList_LeadWebmaster extends Cache_PartialList_Abstract {
    static protected function select($ids){
        $data = Db::logs()->fetchPairs(
            "select `id`, `webmaster` from `lead_data_head` where `id` in ('" . implode("','", $ids) . "')"
        );

        $res = [];
        if(count($data)){
            $webmasters = [];
            foreach($data as $webmaster){
                $webmasters[] = $webmaster;
            }
            Cache_PartialList_WebmasterName::load($webmasters);

            foreach($data as $id => $webmaster){
                $res[$id] = Cache_PartialList_WebmasterName::get($webmaster);

                $res[$id] = "<a href='/account/webmasters/webmaster/?id={$webmaster}'>{$res[$id]}</a>";
            }
        }

        return $res;
    }
}