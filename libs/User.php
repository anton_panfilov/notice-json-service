<?php

class User extends Db_Model {
    static protected $_table = 'users';
    static protected $_structure = [
        'first_name',
        'last_name',

        // не обновляються при update, если явно не указанны
        ['login'],
        ['email'],
        ['phone'],
        ['company_id'],
        ['lang'],
        ['status'],
        ['role'],
        ['create_date'],

        ['login_key'],
        ['secure_key'],
        ['password_hash'],
    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $id;
    public $create_date;
    public $status = User_Status::ACTIVE;
    public $role = 0; // по умолчанию не определенно, что бы исключить логические ошибки неопределения роли при создании пользователя
    public $login;
    public $email;
    public $phone;
    public $company_id;
    public $first_name;
    public $last_name;
    public $lang;

    /**
     * Строка для защиты сессии логирования, меянеться при подборе пароля
     *
     * @var string
     */
    protected $login_key;

    /**
     * Рандомная стрка, присваиваеться при регистрации и никогда не меняеться
     *
     * @var string
     */
    protected $secure_key;


    protected $password_hash;

    /*********************************/

    public function __construct(){
        // при создании нового объекта пользователя, локализация задаеться по умолчанию
        // не ставиться текущая локализация, потому что объект может создавать не обязательно этот же пользователь
        $this->lang = Conf::main()->locale_default;
    }

    /*********************************/

    public function createNew(
        $login,
        $password,
        $role,
        $email,
        $first_name = null,
        $last_name = null,
        $company = null,
        $status = User_Status::ACTIVE,
        $phone = null,
        $lang = null
    ){
        $this->login            = $login;
        $this->role             = (int)$role;
        $this->email            = $email;
        $this->phone            = (new Form_Filter_Phone())->filter($phone);
        $this->first_name       = $first_name;
        $this->last_name        = $last_name;
        $this->company_id       = $company;

        $this->login_key        = String_Hash::randomString(16);
        $this->secure_key       = String_Hash::randomString(16);
        $this->password_hash    = '';

        if(strlen($lang) == 2){
            $this->lang = $lang;
        }

        $this->status           = $status;

        $this->insert();

        $this->setPassword($password, true);

        User_Log::changeStatus($this->id, $this->status, "Assigned when creating");

        static::setObject($this);
    }

    /**
     * @param $password
     * @param bool $save
     * @return $this
     */
    public function setPassword($password, $save = true){
        $this->password_hash = String_Hash::createHash($password . $this->secure_key);
        if($save) $this->update('password_hash');
        return $this;
    }

    /**
     * @param $password
     * @return bool
     */
    public function isValidPassword($password){
        return String_Hash::isValidHash($password . $this->secure_key, $this->password_hash);
    }

    /**
     * Разрешено ли этому пользователю пользоваться аккаунтом
     *
     * @return bool
     */
    public function isActive(){
        return ($this->status == User_Status::ACTIVE);
    }

    public function isVerify(){
        return ($this->status == User_Status::VERIFY);
    }

    /**
     * Получить 4 хеша,
     *  - 2 первых построенны на статичных данных пользователя и меняються только при полууспешных подобрах пароля
     *  - 2 последних построенны на тех же данных + IP + UserAgent
     *
     * @return array
     */
    public function getLoginHashes(){
        $h1 = "{$this->id}{$this->secure_key}{$this->login_key}";
        $h2 = "{$h1}" .
            (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "")
            /*. (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "")*/;

        return [
            crc32("{$h1}-h1_1"),
            crc32("{$h1}-h2"),
            crc32("{$h2}-h3_1"),
            crc32("{$h2}-h4"),

        ];
    }

    /**
     * Получить ссылку на аватар заданного размера
     *
     * @param int $size
     * @return string
     */
    public function getAvatarLink($size = 100){
        $size = max(16, (int)$size);
        if(isset($_SERVER['HTTPS']) && strlen($_SERVER['HTTPS'])){
            return "https://www.gravatar.com/avatar/" . md5(mb_strtolower(trim($this->email))) . "?s={$size}&d=mm";
        }
        return "https://www.gravatar.com/avatar/" . md5(mb_strtolower(trim($this->email))) . "?s={$size}&d=mm";
    }

    /**
     * Получить поолное имя имю и фамилию
     *
     * @param bool $translate
     * @return string
     */
    public function getFullName($translate = false){
        if($translate){
            return Translate::t($this->first_name, 'names') . " " . Translate::t($this->last_name, 'names');
        }
        return $this->first_name . " " . $this->last_name;
    }

    public function verificationComplete($isAuto = true, $comment = ''){
        if($this->id && $this->status == User_Status::VERIFY){
            $this->status = User_Status::ACTIVE;
            $this->update('status');

            User_Log::changeStatus($this->id, $this->status, $comment);

            Db::site()->insert("users_verification_log", [
                "user"      => $this->id,
                "creator"   => Users::getCurrent()->id,
                "ip"        => inet_pton($_SERVER['REMOTE_ADDR']),
                "type"      => $isAuto ? "auto" : "manual",
                "comment"   => (string)$comment,
            ]);
        }
    }

    /**
     * Получить хеш с секретным кодом в соли
     *
     * @param $string
     * @return string
     */
    public function getSecureKeyHash($string){
        return md5($this->secure_key . $string);
    }

    /***************************************************/

    /**
     * Тригер вызываеться при блокировке пользователя
     */
    public function lockTrigger(){
        if($this->role == User_Role::AGN_BUYER){
            User_BuyerAgents::lockBuyerAgentTrigger($this->id);
        }
    }
}