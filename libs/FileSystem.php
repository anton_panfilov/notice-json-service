<?php

class FileSystem {
    static public function rglob($pattern, $path, $flags = null){
        if(!in_array(substr($path, strlen($path), 1), ["/", "\\"])){
            $path.= DIRECTORY_SEPARATOR;
        }

        $files = self::rglob_recursive($pattern, $path, $flags = null);

        if(count($files)){
            foreach($files as $k => $v){
                $files[$k] = substr($v, strlen($path));
            }
        }

        return $files;
    }


    static protected function rglob_recursive($pattern, $path, $flags = null){
        $paths = glob($path . '*', GLOB_MARK | GLOB_ONLYDIR | GLOB_NOSORT);
        $files = glob($path . $pattern, $flags);

        foreach ($paths as $path){
            $files = array_merge($files, self::rglob_recursive($pattern, $path, $flags, $path));
        }

        return $files;
    }
}