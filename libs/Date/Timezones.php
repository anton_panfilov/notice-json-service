<?php

class Date_Timezones {
    /**
     * @var DateTime[]
     */
    static protected $dateTimesTimezonesObjects = [];

    static protected $list_cache;

    static public function getTimezonesList(){
        if(is_null(self::$list_cache)){
            $dic = 'timezones';

            self::$list_cache = [
                'America/Los_Angeles'   => 'Pacific (Los Angeles)',
                'America/Phoenix'       => 'Mountain no DST (Phoenix)',
                'America/Denver'        => 'Mountain (Denver)',
                'America/Chicago'       => 'Central (Chicago)',
                'America/New_York'      => 'Eastern (New_York)',
                /*
                'Europe/Kaliningrad'    => '–1 ' .   Translate::t('Kaliningrad Time',      $dic),
                'Europe/Moscow'         => '+0 ' .   Translate::t('Moscow Time',           $dic),
                'Asia/Yekaterinburg'    => '+2 ' .   Translate::t('Yekaterinburg Time',    $dic),
                'Asia/Novosibirsk'      => '+3 ' .   Translate::t('Omsk Time',             $dic),
                'Asia/Krasnoyarsk'      => '+4 ' .   Translate::t('Krasnoyarsk Time',      $dic),
                'Asia/Irkutsk'          => '+5 ' .   Translate::t('Irkutsk Time',          $dic),
                'Asia/Yakutsk'          => '+6 ' .   Translate::t('Yakutsk Time',          $dic),
                'Asia/Vladivostok'      => '+7 ' .   Translate::t('Vladivostok Time',      $dic),
                'Asia/Magadan'          => '+8 ' .   Translate::t('Magadan Time',          $dic),
                */
            ];
        }
        return self::$list_cache;
    }

    static public function getTimezoneLabel($timezone, $default = null){
        if(isset(self::getTimezonesList()[$timezone])) return self::getTimezonesList()[$timezone];
        if(!is_null($default)) return $default;
        return $timezone;
    }

    static public function getDateForTimezone($format, $timezone, $time = null){
        if(!isset(self::$dateTimesTimezonesObjects[$timezone])){
            self::$dateTimesTimezonesObjects[$timezone] = new DateTime('now', new DateTimeZone($timezone));

            if(is_numeric($time)){
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp($time);
            }
        }
        else {
            if(is_numeric($time)){
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp($time);
            }
            else {
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp(time());
            }
        }

        return self::$dateTimesTimezonesObjects[$timezone]
            ->format($format);
    }
}