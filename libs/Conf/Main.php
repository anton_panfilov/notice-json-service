<?php

class Conf_Main extends Conf_Abstract {

    /********************************************************************/

    /**
     * Массив возможных локализаций
     *
     * @var array
     */
    public $locales = [];

    /**
     * Локализация по умолчанию
     *
     * @var string
     */
    public $locale_default = 'en';

    /**
     * Режим обучения переводов
     * (не рекомендуеться включать на рабочем сервере, что бы не вызывать конфликты)
     *
     * @var bool
     */
    public $localeLearningMode = false;



    /********************************************************************/

    /**
     * Публичный домен с лендингом
     *
     * @var string ex: account.domain.com
     */
    public $domain;


    /**
     * Домен со статистикой
     *
     * @var string ex: account.domain.com
     */
    public $domain_account;


    /**
     * Домен, на котором стоит приемник лидов
     *
     * @var string ex: leads.domain.com
     */
    public $domain_leads;

    /**
     * Домен, на котором стоят формы
     *
     * @var string ex: forms.domain.com
     */
    public $domain_forms;

    /**
     * @var string ex: static.domain.com
     */
    public $domain_static;


    /********************************************************************/

    public $timezone;

    /********************************************************************/

    public $hostname_for_smtp;

    public $memcached;
}