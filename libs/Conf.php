<?php

class Conf {
    static protected $configs = [];

    /**
     * @param $name
     * @param $className
     * @return mixed
     * @throws Exception
     */
    static protected function getConfigArray($name, $className){
        if(!isset(self::$configs[$name])){
            $class_name = null;

            if(is_file($file = Boot::$dir_conf . "/{$name}.php")){
                /** @noinspection PhpIncludeInspection */
                $confArray = include $file;

                // Подгрузка данных из локального конфига
                if(is_file($file = Boot::$dir_conf . "/local/{$name}.php")){
                    /** @noinspection PhpIncludeInspection */
                    $confArrayAdd = include $file;
                    $confArray = array_merge($confArray, $confArrayAdd);
                }

                self::$configs[$name] = new $className($confArray);
            }
            else {
                throw new Exception("config `{$name}` not found");
            }
        }

        return self::$configs[$name];
    }

    /************************************************************************************************/

    /**
     * @return Conf_Main
     */
    static public function main() {
        return self::getConfigArray('main', 'Conf_Main');
    }

    /**
     * @return Conf_Processing
     */
    static public function processing() {
        return self::getConfigArray('processing', 'Conf_Processing');
    }

    /**************/

    /**
     * @return Conf_Mysql
     */
    static public function mysql_site() {
        return self::getConfigArray('mysql_site', 'Conf_Mysql');
    }

    /**
     * @return Conf_Mysql
     */
    static public function mysql_processing() {
        return self::getConfigArray('mysql_processing', 'Conf_Mysql');
    }

    /**
     * @return Conf_Mysql
     */
    static public function mysql_reports() {
        return self::getConfigArray('mysql_reports', 'Conf_Mysql');
    }

    /**
     * @return Conf_Mysql
     */
    static public function mysql_logs() {
        return self::getConfigArray('mysql_logs', 'Conf_Mysql');
    }

    /**
     * @return Conf_Mysql
     */
    static public function mysql_scheduler() {
        return self::getConfigArray('mysql_scheduler', 'Conf_Mysql');
    }

    /************************************************************************************************/
}