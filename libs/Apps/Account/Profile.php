<?php

class Apps_Account_Profile extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/profile');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('Profile', "/account/profile/");
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('Username',   Users::getCurrent()->login);
        $preview->add('Name',       Users::getCurrent()->getFullName());

        if(UserAccess::isWebmaster()){
            $preview->add('Webmaster ID',   Users::getCurrent()->company_id);
            $preview->add(
                'Status',
                (new Webmaster_Status())
                    ->setInLine()
                    ->render(
                        Webmaster_Object::getObject(Users::getCurrent()->company_id)->getStatus()
                    )
            );
        }
        else {
            $preview->add('Role',       (new User_Role())->getLabel(Users::getCurrent()->role));
        }

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $menu->add('User Profile',      "/account/profile/",                        'fa-user');
        $menu->add('Change Password',   "/account/profile/password-change",         'fa-lock');

        if(UserAccess::isWebmaster()){
            $menu->add('Company Profile', "/account/profile/webmaster-profile",     'fa-briefcase');
        }

        if(UserAccess::isAgentWebmaster()){
            $menu->add('Webmaster Agent',   "/account/profile/webmaster-agent",     'fa-group');
            $menu->add('Sign Up Link',     "/account/profile/sign-up-link",         'fa-link');
        }

        if(UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
            $menu->add('My Commissions',   "/account/profile/commissions",          'fa-dollar');
        }

        $this->addToSidebar("<img style='margin:10px; border-radius: 5% !important; height: 203px' src='" .
            Users::getCurrent()->getAvatarLink(203) .
        "'>");

        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /************************************************************/

    function indexAction(){
        $this->setPageTitle("User Profile");

        $form = new Form();
        $form->addButton('Save');

        $form->addElementStatic("Username", Users::getCurrent()->login);
        $form->addElementStatic("Email",    Users::getCurrent()->email);
        $form->addElementStatic("Phone",    Users::getCurrent()->phone);
        $form->addElementStatic("Password", "<a href='/account/profile/password-change'>" . Translate::t("Change Password") . "</a>");

        $form->addElementText_FirstAndLastName('first_name', 'last_name');
        $form->getElement('first_name')->setValue(Users::getCurrent()->first_name);
        $form->getElement('last_name')->setValue(Users::getCurrent()->last_name);


        $form->addElementStatic(
            'Picture',
            "<img
                style='border-radius: 50% !important; margin-right: 7px'
                src='" . Users::getCurrent()->getAvatarLink(28) . "'
            > " .
            "<a href='https://gravatar.com/' target='_blank'>" . Translate::t("Change your profile picture at Gravatar.com") . "</a>"
        );

        if(UserAccess::isWebmaster()){
            $form->addElementStatic(
                "Company Profile",
                "<a href='/account/profile/webmaster-profile'>" . Translate::t("Go to the company profile") . "</a>"
            );
        }

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            Users::getCurrent()->first_name = $form->getValue('first_name');
            Users::getCurrent()->last_name  = $form->getValue('last_name');
            Users::getCurrent()->update(['first_name', 'last_name']);
        }

        echo $form;
    }

    function passwordChangeAction(){
        $this->setPageTitle("Change Password");

        $form = new Form();
        $form->addButton('Save');

        $form->addElementPasswordNew('password', '');
        $form->getElement('password')->setLabel('New Password');
        $form->addElementCaptcha('new_password')
            ->setOneCheckSession(true);


        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            Users::getCurrent()->setPassword($form->getValue('password'));
        }

        echo $form;
    }

    function webmasterProfileAction(){
        if(!UserAccess::isWebmaster()){
            $this->setFollow_Error404();
            return;
        }

        $this->setPageTitle("Webmaster Company Profile");

        echo Webmaster_Object::getObject(Users::getCurrent()->company_id)->renderDetailsForm();
    }

    function webmasterAgentAction(){
        if(!UserAccess::isAgentWebmaster()){
            $this->setFollow_Error404();
            return;
        }
        else {
            $webmasterAgent = User_WebmasterAgent::getObject(Users::getCurrent()->id);
        }

        $this->setPageTitle("Webmaster Agent Profile");

        $form = new Form();
        $form->addButton('Save');

        $form->addElementText_FirstAndLastName('first_name', 'last_name')
            ->setRenderTypeBigValue();
        $form->getElement('first_name')->setValue($webmasterAgent->getUserObject()->first_name);
        $form->getElement('last_name')->setValue($webmasterAgent->getUserObject()->last_name);

        $form->addSeparator('Profile Picture');
        $form->addElementStatic('Email for Gravatar', Users::getCurrent()->email)
            ->setRenderTypeBigValue();
        $form->addElementStatic(
            'Picture',
            "<img
                style='border-radius: 50% !important; margin-right: 7px'
                src='" . Users::getCurrent()->getAvatarLink(28) . "'
            > " .
            "<a href='https://gravatar.com/' target='_blank'>Change your profile picture at Gravatar.com</a>"
        )
            ->setRenderTypeBigValue();

        $form->addSeparator('Contacts Information');

        $form->addElementText('phone', 'Phone')
            ->setValue($webmasterAgent->phone)
            ->setRequired(false)
            ->setRenderTypeBigValue();

        $form->addElementText('phone_mobile', 'Mobile Phone')
            ->setValue($webmasterAgent->phone_mobile)
            ->setRequired(false)
            ->setRenderTypeBigValue();

        $form->addElementText_Email('email', 'Email')
            ->setValue($webmasterAgent->email)
            ->setRequired(false)
            ->setRenderTypeBigValue();


        $form->addElementText('skype', 'Skype')
            ->setValue($webmasterAgent->skype)
            ->setRenderTypeBigValue()
            ->setRequired(false);

        if($form->isValidAndPost()){
            $form->addSuccessMessage("<i class='icon-save margin-right-10'></i> Saved");

            $webmasterAgent->getUserObject()->first_name  = $form->getValue('first_name');
            $webmasterAgent->getUserObject()->last_name   = $form->getValue('last_name');

            $webmasterAgent->getUserObject()->update(['first_name', 'last_name']);


            $webmasterAgent->phone        = $form->getValue('phone');
            $webmasterAgent->phone_mobile = $form->getValue('phone_mobile');
            $webmasterAgent->skype        = $form->getValue('skype');
            $webmasterAgent->email        = $form->getValue('email');

            $webmasterAgent->update(['phone', 'phone_mobile', 'skype', 'email']);
        }

        echo
            '<div class="row-fluid">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                    ' . $form->render() . '
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                    ' . (new View_User_WebmasterAgent($webmasterAgent->id))->render() . '
                </div>
            </div>';
    }

    /*******************************************************************************************/

    function commissionsAction(){
        $this->setTitle("My Commissions - Summary Report");

        $form = new Form();
        $form->setMethodGET();

        $form->addElementDateRange('date');

        $form->isValidNotErrors();

        echo $form->render();

        /*************************************************/

        $index = Db::reports()->fetchPairs(
            "SELECT `date`, SUM(`value`) FROM `pool_summary` WHERE `user` = ? AND `date` BETWEEN ? AND ?",
            Users::getCurrent()->id, $form->getValue("date")['from'], $form->getValue("date")['till']
        );

        $all = [];

        $date = $form->getValue("date")['till'];
        $fromTime = strtotime($form->getValue("date")['from']);

        $today = date('Y-m-d');

        while(true){
            $all[$date] = [
                'date'  => $date,
                'value' => isset($index[$date]) ? ($index[$date]+0) : 0,
            ];

            if($date == $today){
                $all[$date]['value'] = "<small style='color: #AAA'>not calculated</small>";
            }

            $ts = strtotime("{$date} - 1Day");
            $date = date('Y-m-d', $ts);
            if($ts < $fromTime){
                break;
            }
        }

        $tableDays = new Table($all);
        $tableDays->setPagingDisabled();

        $tableDays->addField_Date("date", "Date");

        $tableDays->addField_Text("value", "Commissions")
            ->setFooterAutoSum();

        /*************************************************/

        $tablePools = new Table(Db::reports()->fetchAll(
            "SELECT `pool`, SUM(`value`) as value, SUM(`total`) as total FROM `pool_summary` WHERE `user` = ? AND `date` BETWEEN ? AND ?",
            Users::getCurrent()->id, $form->getValue("date")['from'], $form->getValue("date")['till']
        ));

        $tablePools->setPagingDisabled();


        $tablePools->addField_SetArray('pool', 'Pool', new Pool_Type());
        $tablePools->addField_Text("value", "Commissions")
            ->setFooterAutoSum()
            ->setTextBold()
            ->addDecoratorFunction(function($v){
                return $v + 0;
            });

        $tablePools->addField_Text("total", "Source Total")
            ->setTextColor("#BBB")
            ->addDecoratorFunction(function($v){
                return $v + 0;
            });

        $tablePools->addField_Function(function($c){
            if($c['total'] > 0){
                return round($c['value'] * 100 / $c['total'], 3) . " <small>%</small>";
            }
            return "-";
        }, "Rate")
            ->setTextColor("#BBB");

        echo "
            <div>
                <div class='span6'>
                    <h4>Grouped by days</h4>
                    " . $tableDays->render() . "
                </div>
                <div class='span6'>
                    <h4>Grouped by pools</h4>
                    " . $tablePools->render() . "
                </div>
            </div>
        ";
    }

    /**
     * Получение ссылки для решистрации вебмастров, по которой они будут регистироваться на вас
     */
    function signUpLinkAction(){
        if(!UserAccess::isAgentWebmaster()){
            $this->setFollow_Error404();
            return;
        }

        $webmasterAgent = User_WebmasterAgent::getObject(Users::getCurrent()->id);

        $this->setTitle("My Sign Up Link");

        echo "
            <p>Link for webmasters sing up under your account:</p>
            <p>
                <form  class='smart-form' >

                <label class='input'> <i class='icon-append fa fa-link'></i>
                    <input
                        type='text'
                        value='" . htmlspecialchars($webmasterAgent->getSignUpLink()) ."'
                        onclick=';jQuery(this).select()'
                        readonly='readonly'
                    >
                </label>
                <form>
            </p>
        ";
    }
}