<?php

class Apps_Account_Users_Index extends Controller_Abstract {
    /**************************************************************************************/

    function postLoad(){
        $menu = new View_Panel_Menu();

        if(UserAccess::isAdmin()){
            $menu->addGroup('Create User', 'fa-user')
                ->add('Admin',              '/account/users/create-admin')
                ->add('Buyer Agent',        '/account/users/create-buyer-agent')
                ->add('Webmaster Agent',    '/account/users/create-webmaster-agent');
        }

        $menu->add('Users List',            '/account/users/',       'fa-list');

        if(UserAccess::isAdmin()){
            $menu->addGroup('Cash Pool', 'fa-sitemap')
                ->add('Summary Report',     '/account/users/pool-summary')
                ->add('Settings',           '/account/users/pool-settings');
        }

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('Users List');

        $form = new Form();
        $form->setMethodGET();

        $form->addElementSelect('role', 'Role', (new User_Role)->getArrayToSelect('All...'));

        $form->addElementSelect('status', 'Status', (new User_Status)->getArrayToSelect('All...'));

        $form->addElementText('search', 'Email or Name')
            ->setRequired(false);

        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::site()->select(
            "users",
            ['id', 'create_date', 'status', 'role', 'login', 'email', 'company_id', 'first_name', 'last_name']
        )
            ->order("id desc");

        if($form->getValue('status'))   $select->where("`status`=?", $form->getValue('status'));

        if(UserAccess::isAdmin()){
            if($form->getValue('role'))     $select->where("`role`=?", $form->getValue('role'));
        }
        else if(UserAccess::isAgentBuyer()){
            $select->where("`role`=?", User_Role::BUYER);
        }
        else {
            $select->where("1=0");
        }

        if(strlen($form->getValue('search'))){
            $search = explode(" ", $form->getValue('search'));

            $emails = [];
            $names = [];
            foreach($search as $el){
                $el = trim($el);
                if(strlen($el)){
                    if(strpos($el, '@') !== false){
                        $emails[] = $el;
                    }
                    else {
                        $names[] = Db::site()->escape($el);
                    }
                }
            }

            if(count($emails)){
                $select->where_in('`email`=?', $emails);
            }

            if(count($names)){
                $values = implode(",", $names);
                $select->where("(`login` in ({$values}) or `first_name` in ({$values}) or `last_name` in ({$values}))");
            }
        }

        ////////////////////////////////
        // table
        $table = new Table($select);
        $table->setPagingAuto(100);

        $table->addField_Text('id', 'ID')->setLink(
            "/account/users/user/?id={id}"
        );
        $table->addField_Avatar('email', '', 'avatar');
        $table->addField_SetArray('role', 'Role', (new User_Role()));
        $table->addField_SetArray('status', 'Status', new User_Status());
        $table->addField_Text('login', 'Login')->setLink(
            "/account/users/user/?id={id}"
        );

        $table->addField_Text('email', 'Email');
        $table->addField_Pattern('{first_name} {last_name}', 'Name');
        $table->addField_Date('create_date', 'Create');


        echo $form->render() . $table->render();
    }

    /*******************************************************************************************/

    function getCreateUserForm(){
        $form = new Form();
        $form->addButton("Create");

        Site::addJSCode("
            document.setLogiFromName = function(){
                var first = jQuery('#first_name').val();
                var last = jQuery('#last_name').val();

                if(first.length && last.length){
                    jQuery('#login').val(
                        first.toLowerCase() + '.' + last.toLowerCase()
                    );
                    jQuery('#login').blur();
                }
            };

            jQuery('#first_name').blur(function(){
                document.setLogiFromName();
            });

            jQuery('#last_name').blur(function(){
                document.setLogiFromName();
            });

            jQuery('#login').keyup(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

            jQuery('#login').change(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

            jQuery('#login').blur(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

        ");

        $form->addElementText_FirstAndLastName('first_name', 'last_name');
        $form->addElementText_Email('email', 'Email');

        $form->addSeparator();
        $form->addElementText_NewLogin('login');

        $form->addElementRadio("password_type", "", [
            'random'    => 'Random Password',
            'input'     => 'Input Password',
        ])
            ->setValue('random');

        $form->addElementPasswordNew_Ligths('password', '');

        $randomPassLen = 16;
        $random = (isset($_POST['randomPassword']) && strlen($_POST['randomPassword']) == $randomPassLen) ?
            $_POST['randomPassword'] :
            String_Hash::randomString($randomPassLen);

        $form->addElementHidden("randomPassword", $random);
        $form->addElementStatic("",
            "
                <p><a href='https://" . Conf::main()->domain_account . "/'>https://" . Conf::main()->domain_account . "/</a></p>
                <p>Login: <b id='login_str_doc'></b></p>
                <p>Password: <b>{$random}</b></p>
            ",
            'randomText'
        );

        $form->addConditionalFileds_Array('password_type', 'input',  ['password', 'password_confirm']);
        $form->addConditionalFileds_Array('password_type', 'random', 'randomText');

        $form->addSeparator();
        $form->addElementCheckbox('sendCredentialsToEmail', 'Send Credentials to Email')
            ->setValue('1');

        return $form;
    }

    function createAdminAction(){
        if(!UserAccess::isAdmin()){
            $this->setFollow_Error403();
            return;
        }

        $this->setTitle('Create New Admin');
        $this->setPageDescription(
            '
                <p>The administrator has full rights in the system, including myself can create new administrators</p>
            '
        );

        $form = $this->getCreateUserForm();

        if($form->isValidAndPost()){
            $password = ($form->getValue('password_type') == 'input') ?
                $form->getValue('password') :
                (
                    isset($_POST['randomPassword']) ?
                        $_POST['randomPassword'] :
                        String_Hash::randomString(16)
                );

            $user = new User();
            $user->createNew(
                $form->getValue('login'),
                $password,
                User_Role::ADMIN,
                $form->getValue('email'),
                $form->getValue('first_name'),
                $form->getValue('last_name')
            );

            if($form->getValue('sendCredentialsToEmail')){
                $mail = new Email_Object(4, [
                    'name'      => $user->getFullName(),
                    'link'      => 'https://' . Conf::main()->domain_account,
                    'username'  => $user->login,
                    'password'  => $password,
                ]);
                $mail->addTo($user->email, $user->getFullName());
                $mail->send();
            }

            header('location: /account/users/user/?id=' . $user->id);
        }

        echo $form->render();
    }

    /*******************************************/

    function createBuyerAgentAction(){
        if(!UserAccess::isAdmin()){
            $this->setFollow_Error403();
            return;
        }

        $this->setTitle('Create New Buyer Agent');

        $form = $this->getCreateUserForm();

        if($form->isValidAndPost()){
            $password = ($form->getValue('password_type') == 'input') ?
                $form->getValue('password') :
                (
                isset($_POST['randomPassword']) ?
                    $_POST['randomPassword'] :
                    String_Hash::randomString(16)
                );

            $user = new User();
            $user->createNew(
                $form->getValue('login'),
                $password,
                User_Role::AGN_BUYER,
                $form->getValue('email'),
                $form->getValue('first_name'),
                $form->getValue('last_name')
            );

            Db::site()->insert("users_buyer_agents", [
                'id' => $user->id,
            ]);

            if($form->getValue('sendCredentialsToEmail')){
                $mail = new Email_Object(4, [
                    'name'      => $user->getFullName(),
                    'link'      => 'https://' . Conf::main()->domain_account,
                    'username'  => $user->login,
                    'password'  => $password,
                ]);
                $mail->addTo($user->email, $user->getFullName());
                $mail->send();
            }

            header('location: /account/users/user/?id=' . $user->id);
        }

        echo $form->render();
    }

    function createWebmasterAgentAction(){
        if(!UserAccess::isAdmin()){
            $this->setFollow_Error403();
            return;
        }

        $this->setTitle('Create New Webmaster Agent');

        $form = $this->getCreateUserForm();

        $form->addSeparator('Webmaster Agent Settings');

        $form->addElementCheckbox('show_in_sign_up', 'Show in Sing Up Form');
        $form->addElementCheckbox('random_distribution', 'Part in random distribution of webmasters');

        if($form->isValidAndPost()){
            $password = ($form->getValue('password_type') == 'input') ?
                $form->getValue('password') :
                (
                isset($_POST['randomPassword']) ?
                    $_POST['randomPassword'] :
                    String_Hash::randomString(16)
                );

            $user = new User();
            $user->createNew(
                $form->getValue('login'),
                $password,
                User_Role::AGN_WM,
                $form->getValue('email'),
                $form->getValue('first_name'),
                $form->getValue('last_name')
            );

            Db::site()->insert("users_webmaster_agents", [
                'id'                    => $user->id,
                'show_in_sign_up'       => $form->getValue('show_in_sign_up'),
                'random_distribution'   => $form->getValue('random_distribution') ? 10 : 0,
            ]);

            if($form->getValue('sendCredentialsToEmail')){
                $mail = new Email_Object(4, [
                    'name'      => $user->getFullName(),
                    'link'      => 'https://' . Conf::main()->domain_account,
                    'username'  => $user->login,
                    'password'  => $password,
                ]);
                $mail->addTo($user->email, $user->getFullName());
                $mail->send();
            }

            header('location: /account/users/user/webmaster-agent?id=' . $user->id);
        }

        echo $form->render();
    }

    /*******************************************************************************************/

    function poolSummaryAction(){
        $this->setPageTitle("Pool Summary");

        $form = new Form();
        $form->setMethodGET();
        $form->addButton("Pool Summary", "summary");

        $form->addElementDateRange("date");

        $form->addElementSelect(
            "pool",
            "Pool",
            (new Pool_Type())->getArrayToSelect("All")
        )
            ->setRequired(false);

        $form->addElement_BuyerAgent("agent_b", "Buyer Agent")
            ->setRequired(false);

        $form->addElement_WebmasterAgent("agent_wm", "Webmaster Agent")
            ->setRequired(false);

        $form->addConditionalFileds_Array("pool", [Pool_Type::AGENT_BUYER], ["agent_b"]);
        $form->addConditionalFileds_Array("pool", [Pool_Type::AGENT_WEBMASTER], ["agent_wm"]);

        $form->isValidNotErrors();

        echo $form->render();

        $select = Db::reports()->select("pool_summary", [
            'date',
            'pool',

            'value' => new Db_Expr("sum(`value`)"),
            'total' => new Db_Expr("sum(`total`)"),
        ])
            ->where("`date` BETWEEN ? and ?", $form->getValue("date")['from'], $form->getValue("date")['till']);



        if($form->getValue("pool")){
            $select
                ->group("date")
                ->where("`pool`=?", $form->getValue("pool"));

            if($form->getValue("agent_b"))      $select->where("`user`=?", $form->getValue("agent_b"));
            if($form->getValue("agent_wm"))     $select->where("`user`=?", $form->getValue("agent_wm"));

            $index = $select->fetchUnique();

            $all = [];

            $date = $form->getValue("date")['till'];
            $fromTime = strtotime($form->getValue("date")['from']);

            $total = [
                'value' => 0,
                'total' => 0,
                'rate'  => 0,
            ];

            while(true){
                $all[$date] = [
                    'date'  => $date,
                    'value' => isset($index[$date]) ? $index[$date]['value'] : 0,
                    'total' => isset($index[$date]) ? $index[$date]['total'] : 0,
                ];

                if($all[$date]['total'] > 0){
                    $all[$date]['rate'] = round(($all[$date]['value'] * 100 / $all[$date]['total']), 2) . " <small>%</small>";
                }
                else {
                    $all[$date]['rate'] = "-";
                }

                $ts = strtotime("{$date} - 1Day");
                $date = date('Y-m-d', $ts);
                if($ts < $fromTime){
                    break;
                }
            }

            $table = new Table($all);
            $table->setPagingDisabled();

            $table->addField_Date('date', 'Date')
                ->setWidth("25%");

            $table->addField_Text('value', 'Commission')
                ->addDecoratorFunction(function($v){
                    return $v + 0;
                })
                ->setTextAlignCenter()
                ->setWidth("25%")
                ->setFooterAutoSum();


            $table->addField_Text('total', 'Total')
                ->addDecoratorFunction(function($v){
                    return $v + 0;
                })
                ->setTextAlignCenter()
                ->setWidth("25%")
                ->setTextColor("#999")
                ->setFooterAutoSum();

            $table->addField_Text('rate', 'Rate')
                ->setTextAlignCenter()
                ->setWidth("25%")
                ->setTextColor("#AAA");



            $tableUsers = Db::reports()->fetchAll(
                "select `user`, sum(`value`) as `value_sum`, sum(`total`) as `total` from pool_summary " .
                " where `date` BETWEEN ? and ? and `pool`=? group by `user` order by value_sum desc",
                $form->getValue("date")['from'], $form->getValue("date")['till'], $form->getValue("pool")
            );

            if(count($tableUsers) > 1){
                $tableUsers = new Table($tableUsers);
                $tableUsers->setPagingDisabled();

                $tableUsers->addField_UserLogin('user');

                $tableUsers->addField_Text('value_sum', 'Commission')
                    ->addDecoratorFunction(function($v){
                        return $v + 0;
                    })
                    ->setTextAlignCenter()
                    ->setWidth("25%")
                    ->setFooterAutoSum();


                $tableUsers->addField_Text('total', 'Total')
                    ->addDecoratorFunction(function($v){
                        return $v + 0;
                    })
                    ->setTextAlignCenter()
                    ->setWidth("25%")
                    ->setTextColor("#999")
                    ->setFooterAutoSum();

                $tableUsers->addField_Function(function($c){
                    if($c['total'] > 0){
                        return round($c['value_sum'] * 100 / $c['total'], 3) . " <small>%</small>";
                    }
                    return "-";
                }, "Rate")
                    ->setTextAlignCenter()
                    ->setWidth("25%")
                    ->setTextColor("#BBB");

                echo "
                    <div>
                        <div class='span6'><h4>Group by Date</h4>" . $table->render() . "</div>
                        <div class='span6'><h4>Group by Users</h4>" . $tableUsers->render() . "</div>
                    </div>
                ";
            }
            else {
                echo $table->render();
            }


        }
        else {
            $select->group("date, pool");
            $temp = $select->fetchAll();
            $index = [];
            $pools = [];

            if(count($temp)){
                foreach($temp as $el){
                    if(!in_array($el['pool'], $pools)){
                        $pools[] = $el['pool'];
                    }

                    if(!isset($index[$el['date']])) $index[$el['date']] = [];
                    $index[$el['date']][$el['pool']] = $el['value'];
                }
            }


            $all = [];

            $date = $form->getValue("date")['till'];
            $fromTime = strtotime($form->getValue("date")['from']);

            $tblTotal = [];
            if(count($pools)){
                foreach($pools as $pool){
                    $tblTotal['p'.$pool] = 0;
                }
            }
            $tblTotal['total'] = 0;

            while(true){
                $add = [
                    'date'  => $date,
                ];

                $total = 0;
                if(count($pools)){
                    foreach($pools as $pool){
                        $add['p'.$pool] = isset($index[$date][$pool]) ? $index[$date][$pool]+0 : 0;
                        $total+= $add['p'.$pool];
                        $tblTotal['p'.$pool]+= $add['p'.$pool];
                    }
                }
                $add['total'] = round($total, 2);
                $tblTotal['total']+= $add['total'];

                $all[$date] = $add;

                $ts = strtotime("{$date} - 1Day");
                $date = date('Y-m-d', $ts);
                if($ts < $fromTime){
                    break;
                }
            }

            if(count($pools)){
                foreach($pools as $pool){
                    $tblTotal['p'.$pool] = "<a href='" .
                            Http_Url::convert(['pool' => $pool]) .
                        "'>" . round($tblTotal['p'.$pool], 2) . "</a>";
                }
            }
            $tblTotal['total'] = round($tblTotal['total'], 2);

            $table = new Table($all);
            $table->setPagingDisabled();

            $table->addField_Date('date', 'Date');

            if(count($pools)){
                foreach($pools as $pool){
                    $table->addField_Text('p'.$pool, (new Pool_Type())->getLabel($pool))
                        ->setTextAlignCenter();
                }
            }

            $table->addField_Text('total', 'Total')
                ->setTextAlignCenter()
                ->setTextColor('#172');

            $table->addFooter($tblTotal);

            echo $table->render();
        }
    }

    /***********************************************************************************************/

    protected function poolSettings_main(){
        $r = "";

        $settings = Pool_Settings::loadSettings();
        $settingsAutoChange = Pool_Settings::loadAutoChangeSettings();
        $groups = (new Pool_Type())->getArrayToSelect();

        $r.= "<h4><a href='" . Http_Url::convert(['tab' => 'def']) . "'>Defaults Settings</a></h4>";
        $gArr = [];
        foreach($groups as $k => $v){
            $gArr[] = [
                'label' => $v,
                'rate'  => Pool_Settings::getRate($k),
            ];
        }

        $table = new Table($gArr);
        $table->setPagingDisabled();

        $table->addField_Text("label", "Label")
            ->setWidth("200px");

        $table->addField_Text("rate", "Rate")
            ->addDecoratorFunction(function($v){
                return ($v * 100) . " <small style='color:#999'>% of the company's revenue</small> ";
            });

        $r.= $table->render() . "<br><br>";

        /*********************************************************/

        foreach($groups as $poolID => $poolLabel){
            $agents = Pool_Settings::getAgents($poolID);

            if(count($agents)){
                $r.= "<h4><a href='" . Http_Url::convert(['tab' => "g{$poolID}"]) . "'>{$poolLabel}</a></h4>";


                foreach($agents as $user => $userLogin){
                    $agents[$user] = [
                        'id'    => $user,
                        'login' => $userLogin,
                    ];

                    $agents[$user]['rate'] = Pool_Settings::getRate($poolID, $user);
                    $agents[$user]['unique_rate'] = isset($settings[$poolID][$user]) ? 1 : 0;
                    $agents[$user]['auto_change'] = 0;
                    $agents[$user]['setup'] = [];

                    if(isset($settingsAutoChange[$poolID][$user])){
                        $agents[$user]['auto_change'] = $settingsAutoChange[$poolID][$user]['active'];
                        $agents[$user]['setup'] = $settingsAutoChange[$poolID][$user]['set'] == 'def' ?
                            Pool_Settings::getAutoLoadDefaultSettings($poolID) :
                            $settingsAutoChange[$poolID][$user]['settings'];
                    }
                }


                $table = new Table($agents);
                $table->setPagingDisabled();

                $table->addField_Text("login", "Agent")
                    ->setWidthMin();;

                $table->addField_Function(
                    function($c){
                        return $c['unique_rate'] ?
                            "<span style='color:#111; font-size: 14px'>" . ($c['rate'] * 100) . "</span> <small style='color:#999'>%</small>" :
                            "<span style='color:#777'>" . ($c['rate'] * 100) . "</span> <small style='color:#999'>%</small>";
                    },
                    "Rate"
                )
                    ->setTextAlignCenter()
                    ->setWidth('100px');


                $table->addField_SetArray("auto_change", "")
                    ->addValue_Green("1", "On")
                    ->addValue_White("0", "Off");

                $table->addField_Function(
                    function($c){
                        if(is_array($c['setup']) && count($c['setup'])){
                            $r = [];
                            foreach($c['setup'] as $k => $v){
                                $r[] = "<div style='width:200px; float: left'>" .
                                        ($v * 100) . " <small style='color:#999'>%</small>" .
                                        " <small style='color:#888; padding: 0 7px 0 7px'>from</small> " .
                                        " <small style='color:#777'>$</small> " . number_format($k, 0) . "" .
                                    "</div>";
                            }

                            $r = implode(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", $r);
                        }
                        else {
                            $r = "<span style='color:#BBB'>not setup</span>";
                        }

                        return $r;
                    },
                    "Auto Change Setup"
                )
                    ->setDescription(
                        "Income of the company with webmasters agent this week - the percentage of the next week"
                    );


                $r.= $table->render() . "<br><br>";
            }
        }

        return $r;
    }

    /***********************************************************************************/

    protected function poolSettings_def(){
        $form = new Form();

        $groups = (new Pool_Type())->getArrayToSelect();

        foreach($groups as $id => $label){
            $form->addElementText("rate_{$id}", $label)
            ->setTextPostInput(
                    "<span style='font-size: 18px; margin-left: 10px; color: #999;'>%</span>" .
                    "<span style='font-size: 10px; margin-left: 10px; color: #AAA; line-height: 20px'>of the company's revenue</span>"
                )
                ->setWidth("100px")
                ->setValue(Pool_Settings::getRate($id, 0) * 100)
                ->addValidator(new Form_Validator_MinMax(0, 99))
                ->addValidator(new Form_Validator_Regexp(
                    "/^[-]?[0-9]*(\\.[0-9]{0,4})?\$/",
                    "Invalid price value. Fractional part should have no more than 4 digits"));
        }

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            foreach($groups as $id => $label){
                Pool_Settings::setRate($id, 0, $form->getValue("rate_{$id}") / 100);
            }
        }

        return $form->render();
    }

    /***********************************************************************************/

    protected function poolSettings_AgentsAbstract($poolType){
        $form = new Form();

        $settings           = Pool_Settings::loadSettings();
        $settingsAutoChange = Pool_Settings::loadAutoChangeSettings();

        $agents = Pool_Settings::getAgents($poolType);

        $form->addElementText("rate_0", "<b>Default " . (new Pool_Type())->getLabel($poolType) . " Rate</b>")
            ->setTextPostInput(
                "<span style='font-size: 18px; margin-left: 10px; color: #999; line-height: 20px'>%</span>" .
                "<span style='font-size: 10px; margin-left: 10px; color: #AAA; line-height: 20px'>of the company's revenue</span>"
            )
            ->setWidth("100px")
            ->setValue($settings[$poolType][0] * 100)
            ->addValidator(new Form_Validator_MinMax(0, 99))
            ->addValidator(new Form_Validator_Regexp(
                "/^[-]?[0-9]*(\\.[0-9]{0,4})?\$/",
                "Invalid price value. Fractional part should have no more than 4 digits"));

        $form->addSeparator("Agents Rates");

        foreach($agents as $id => $label){
            $form->addElementText("rate_{$id}", $label)
                ->setTextPostInput("<span style='font-size: 18px; margin-left: 10px; color: #999;'>%</span>")
                ->setWidth("100px")
                ->setValue(
                    isset($settings[$poolType][$id]) ?
                        $settings[$poolType][$id] * 100 :
                        ""
                )
                ->setRequired(false)
                ->addValidator(new Form_Validator_MinMax(0, 99))
                ->addValidator(new Form_Validator_Regexp(
                    "/^[-]?[0-9]*(\\.[0-9]{0,4})?\$/",
                    "Invalid price value. Fractional part should have no more than 4 digits"));
        }

        $form->addSeparator(
            "Agents Auto Changes",
            "Percentage of agents recalculated 1 per day, based on the previous 30 days."
        );


        $table = new Table(Pool_Summary::getDynamic($poolType));
        $table->setPagingDisabled();

        // id	login	per_0	per_1	per_2	per_3

        $table->addField_Text("login", "Agent");

        $table->startGroup("The company's revenue group by agents");
        $table->addField_Text("per_0", "1 - 30 Days")->setTextAlignCenter();
        $table->addField_Text("per_1", "31 - 60 Days")->setTextAlignCenter();
        $table->addField_Text("per_2", "61 - 90 Days")->setTextAlignCenter();
        $table->addField_Text("per_3", "91 - 120 Days")->setTextAlignCenter();

        $form->addElementStatic("Dynamic", $table->render())
            ->setRenderTypeFullLine();


        $form->addElementTextarea("ac_def", "Default Settings")
            ->setWidth("200")
            ->setHeight("150")
            ->setRequired(false)
            ->setValue(Pool_Settings::getAutoLoadDefaultSettingsString($poolType))
            ->setComment(
                "
                    <p>Income for the 30 days of receipt with an agent - the percentage it receives</p>
                    <p>Example:</p>
                    <p>
                        <div class='alert alert-info'>
                            10000 0.7<br>
                            20000 0.8<br>
                            50000 0.9<br>
                            100000 1<br>
                        </div>
                    </p>
                "
            );

        foreach($agents as $id => $label){
            $form->addSeparator("<small>{$label}</small>");

            $form->addElementSelect("ac_{$id}_active", "Status", [
                '0' => 'OFF',
                '1' => 'ON',
            ])
                ->setRequired(false)
                ->setValue(
                    isset($settingsAutoChange[$poolType][$id]) ?
                        $settingsAutoChange[$poolType][$id]['active'] : '0'
                );

            $form->addElementSelect("ac_{$id}_set", "Settings Type", [
                'def' => 'Default Settings',
                'unique' => 'Unique Settings',
            ])
                ->setRequired(false)
                ->setValue(
                    isset($settingsAutoChange[$poolType][$id]) ?
                        $settingsAutoChange[$poolType][$id]['set'] : 'def'
                );

            /*
            $form->addMultiElement(["ac_{$id}_active", "ac_{$id}_set"], 'Status / Settings Type')
                ->setTemplate("{ac_{$id}_active} {ac_{$id}_set}");
            */

            $form->addElementTextarea("ac_{$id}_settings", "Settings for {$label}")
                ->setWidth("200")
                ->setHeight("150")
                ->setRequired(false)
                ->setValue(
                    isset($settingsAutoChange[$poolType][$id]) ?
                        Pool_Settings::renderAutoChangeSettings(
                            $settingsAutoChange[$poolType][$id]['settings']
                        ) : ''
                );;

            $form->addConditionalFileds_Array("ac_{$id}_active", "1", "ac_{$id}_set");
            $form->addConditionalFileds_Array("ac_{$id}_set", "unique", "ac_{$id}_settings");
        }

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            Pool_Settings::setRate($poolType, 0, $form->getValue('rate_0') / 100);

            Pool_Settings::setAutoLoadSettings(
                $poolType,
                0,
                1,
                'def',
                $form->getValue('ac_def')
            );

            foreach($agents as $id => $label){
                if(strlen($form->getValue("rate_{$id}"))){
                    Pool_Settings::setRate($poolType, $id, $form->getValue("rate_{$id}") / 100);
                }
                else {
                    Pool_Settings::setRate($poolType, $id, null);
                }

                Pool_Settings::setAutoLoadSettings(
                    $poolType,
                    $id,
                    $form->getValue("ac_{$id}_active"),
                    $form->getValue("ac_{$id}_set"),
                    $form->getValue("ac_{$id}_settings")
                );
            }
        }

        return $form->render();
    }

    /******************************************************************/

    protected function poolSettings_g1(){
        return $this->poolSettings_AgentsAbstract(Pool_Type::AGENT_WEBMASTER);
    }

    /***********************************************************************************/

    protected function poolSettings_g2(){
        return $this->poolSettings_AgentsAbstract(Pool_Type::AGENT_BUYER);
    }

    /***********************************************************************************/

    /**
     * Programmers Pool
     *
     * @return string
     */
    protected function poolSettings_g3(){
        $pool = 3;

        /////////////////////////////////////////////

        $formCreate = new Form();
        $formCreate->addButton("Add new user");
        $formCreate->addElementHidden("createForm", "1");

        $formCreate->addElementText("name", "Name")
            ->addFilter(new Form_Filter_Trim())
            ->addValidator(new Form_Validator_UniqueInDatabase(
                "select id from pool_fixed_users where pool='{$pool}' and `del`=0 and `name`=?",
                Db::processing(),
                "Name already use"
            ))
            ->setRenderTypeBigValue();

        $formCreate->addElementText_Num("koef", "Rate", 0, 999)
            ->setWidth("50px")
            ->setMaxLength(3)
            ->setValue(0);

        if(isset($_POST['createForm']) && $formCreate->isValidAndPost()){
            $formCreate->addSuccessMessage("Created successful");

            Db::processing()->insert("pool_fixed_users", [
                'pool' => $pool,
                'name' => $formCreate->getValue('name'),
                'koef' => $formCreate->getValue('koef'),
            ]);

            $formCreate->setValue('name', '');
            $formCreate->setValue('koef', '0');
        }

        /////////////////////////////////////////////

        $all = Db::processing()->fetchUnique(
            "SELECT `id`, `name`, `koef` FROM `pool_fixed_users` WHERE `del`=0 and `pool`=? ORDER BY `name`",
            $pool
        );

        $allIds = array_keys($all);

        Site::addJSCode("
            document.allRatesIds = " . Json::encode($allIds) . ";

            document.reindexRates = function(){
                var total = 0;
                for(k in document.allRatesIds){
                    total+= parseInt(jQuery('#user_' + document.allRatesIds[k]).val());
                }
                for(k in document.allRatesIds){
                    jQuery('#percent_' + document.allRatesIds[k]).html(
                        parseInt(
                            parseInt(jQuery('#user_' + document.allRatesIds[k]).val() * 100 / total)
                        )
                    );
                }
                console.log(total);
            }

            document.reindexRates();
        ");

        if(isset($_GET['delUser'])){
            Db::processing()->update("pool_fixed_users", ['del' => 1], "id=?", (int)$_GET['delUser']);
            header("location: " . Http_Url::convert(['delUser' => null]));
        }

        $form = new Form();
        $form->addButton("Save");
        $form->addElementHidden("settingsForm", "1");

        if(count($all)){
            /*
            $total = 0;
            foreach($all as $el){
                $total+= $el['koef'];
            }
            */

            foreach($all as $el){
                $form->addElementText_Num("user_{$el['id']}", $el['name'], 0, 999)
                    ->setWidth("50px")
                    ->setMaxLength(3)
                    ->setValue($el['koef'])
                    ->setTextPostInput( // " . round($el['koef']*100 / $total) . "
                        "<span style='padding-left: 50px; font-size: 18px; color: #777'>
                            <span id='percent_{$el['id']}'>...</span> <small style='color: #BBB'>%</small>
                        </span>

                        " .
                        (new Ui_Button("Delete"))
                            ->setCSS("font-size", "11px")
                            ->setCSS("padding", "5px")
                            ->setCSS("float", "right")
                            ->setConfirm("Delete this user?")
                            ->setLink(Http_Url::convert(['delUser' => $el['id']]))
                    )
                    ->setRenderTypeBigValue()
                    ->getAttribs()
                        ->setAttrib("onChange", "document.reindexRates()")
                        ->setAttrib("onKeyUp", "document.reindexRates()")
                ; //
            }
        }

        if(isset($_POST['settingsForm']) && $form->isValidAndPost()){
            $form->addSuccessMessage();

            if(count($all)){
                foreach($all as $el){
                    Db::processing()->update(
                        "pool_fixed_users",
                        ['koef' => $form->getValue("user_{$el['id']}")],
                        "id=?",
                        $el['id']
                    );
                }
            }
        }

        //////////////////////////////////////

        return "<div class='row-fluid'>
            <div class='span6'>
                <h4>Rates Settings</h4>
                " . $form->render() . "
            </div>
            <div class='span6'>
                <h4>Add User</h4>
                " . $formCreate->render() . "
            </div>
        </div>";
    }

    /***********************************************************************************/

    function poolSettingsAction(){
        $this->setPageTitle("Pool Settings");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("main", "All Settings");
        $tabs->addTab("def", "Set Default Rates");

        $tabs->addTab("g" . Pool_Type::AGENT_WEBMASTER, "Webmaster Agents");
        $tabs->addTab("g" . Pool_Type::AGENT_BUYER,     "Buyer Agents");
        $tabs->addTab("g" . Pool_Type::PROGRAMMERS,     "Programmers");

        $method = "poolSettings_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText(
                $this->$method()
            );
        }
        else {
            $tabs->setText(
                (new View_Page_ComingSoon())->render()
            );
        }

        echo $tabs->render();
    }

}