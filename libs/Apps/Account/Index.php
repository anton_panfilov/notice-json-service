<?php

class Apps_Account_Index extends Controller_Abstract {
    function indexAction(){
        Translate::addDictionary("account/index");
        echo (new View_Menu_Top)->renderIndex();
        echo (new View_Page_AccountIndex())->render();
    }

    function testAction(){
        $select = Db::site()->select("_test", ["test", "test2"])
            ->distinct(1)
        ;

        $table = new Table($select);

        echo $table->render();
    }
}