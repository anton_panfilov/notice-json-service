<?php

class Apps_Banks extends Controller_Abstract {

    function postLoad(){
        $menu = new View_Panel_Menu();

        $menu->addGroup("Справочники", "fa-list")
            ->add('Бик',  '/banks/bik');

        $this->addToSidebar(
            $menu->render()
        );

        $this->addBreadcrumbElement("Банки", "/banks/");
    }

    /*************************************************************************************************/

    function indexAction(){
        echo "Все что связанно с банками";
    }


    /*************************************************************************************************/

    function bikAction(){
        $this->setTitle("Справочник БИК");
        echo "
            <p>Данные получены с сайта crb.ru:</p>
            <p><a href='http://www.cbr.ru/mcirabis/Default.aspx?Prtid=bic'>http://www.cbr.ru/mcirabis/Default.aspx?Prtid=bic</a></p>

        ";
        // http://www.cbr.ru/mcirabis/BIK/bik_db_22012015.zip
    }

    /*************************************************************************************************/
}
