<?php

class Services_Banks_Bik {
    /**
     * Обновление базы данных БИК c сайта: http://www.cbr.ru/mcirabis/Default.aspx?Prtid=bic
     * Получнеие архива по дате: http://www.cbr.ru/mcirabis/BIK/bik_db_{DDMMYYYY}.zip
     */
    static public function reindexDB(){
        for($i=0; $i<6; $i++){
            $file = "bik_db_" . date("dmY", strtotime("NOW - {$i}Days")) . ".zip";
            $content = file_get_contents("http://www.cbr.ru/mcirabis/BIK/{$file}");
            if(strlen($content)){
                file_put_contents("/tmp/{$file}", $content);
                exec("unzip -o /tmp/{$file} BNKSEEK.DBF -d /tmp/bik/");
                $db = dbase_open('/tmp/bik/BNKSEEK.DBF', 0);

                $j = 0;
                $all = [];
                if ($db) {
                    $record_numbers = dbase_numrecords($db);
                    for ($i = 1; $i <= $record_numbers; $i++) {
                        $row = dbase_get_record_with_names($db, $i);

                        foreach($row as $k => $v){
                            $v = iconv('cp866', 'UTF-8', $v);
                            $row[$k] = trim($v);
                        }

                        $all[] = [
                            'VKEY'    => isset($row['VKEY'])    ? $row['VKEY']      : "", // 8
                            'REAL'    => isset($row['REAL'])    ? $row['REAL']      : "", // 4
                            'PZN'     => isset($row['PZN'])     ? $row['PZN']       : "", // 2
                            'UER'     => isset($row['UER'])     ? $row['UER']       : "", // 1
                            'RGN'     => isset($row['RGN'])     ? $row['RGN']       : "", // 2
                            'IND'     => isset($row['IND'])     ? $row['IND']       : "", // 6
                            'TNP'     => isset($row['TNP'])     ? $row['TNP']       : "", // 1
                            'NNP'     => isset($row['NNP'])     ? $row['NNP']       : "", // 25
                            'ADR'     => isset($row['ADR'])     ? $row['ADR']       : "", // 30
                            'RKC'     => isset($row['RKC'])     ? $row['RKC']       : "", // 9
                            'NAMEP'   => isset($row['NAMEP'])   ? $row['NAMEP']     : "", // 45
                            'NAMEN'   => isset($row['NAMEN'])   ? $row['NAMEN']     : "", // 30
                            'NEWNUM'  => isset($row['NEWNUM'])  ? $row['NEWNUM']    : "", // 9
                            'NEWKS'   => isset($row['NEWKS'])   ? $row['NEWKS']     : "", // 9
                            'PERMFO'  => isset($row['PERMFO'])  ? $row['PERMFO']    : "", // 6
                            'SROK'    => isset($row['SROK'])    ? $row['SROK']      : "", // 2
                            'AT1'     => isset($row['AT1'])     ? $row['AT1']       : "", // 7
                            'AT2'     => isset($row['AT2'])     ? $row['AT2']       : "", // 7
                            'TELEF'   => isset($row['TELEF'])   ? $row['TELEF']     : "", // 25
                            'REGN'    => isset($row['REGN'])    ? $row['REGN']      : "", // 9
                            'OKPO'    => isset($row['OKPO'])    ? $row['OKPO']      : "", // 8
                            'DT_IZM'  => isset($row['DT_IZM'])  ? $row['DT_IZM']    : "", // 8
                            'CKS'     => isset($row['CKS'])     ? $row['CKS']       : "", // 6
                            'KSNP'    => isset($row['KSNP'])    ? $row['KSNP']      : "", // 20
                            'DATE_IN' => isset($row['DATE_IN']) ? $row['DATE_IN']   : "", // 8
                            'DATE_CH' => isset($row['DATE_CH']) ? $row['DATE_CH']   : "", // 8
                            'VKEYDEL' => isset($row['VKEYDEL']) ? $row['VKEYDEL']   : "", // 8
                            'DT_IZMR' => isset($row['DT_IZMR']) ? $row['DT_IZMR']   : "", // 8
                            'deleted' => isset($row['deleted']) ? $row['deleted']   : "0", // tinyint
                        ];
                    }
                }

                dbase_close($db);
                rmdir("/tmp/bik");

                Db::site()->transactionStart();
                Db::site()->query("TRUNCATE TABLE banks_bik");
                Db::site()->insertMulti('banks_bik', $all);
                Db::site()->transactionCommit();

                break;
            }
        }
    }

    static public function getInfoByBik($bik){
        $bik = sprintf("%09d", (new Form_Filter_NumbersOnly())->filter($bik));
        $r = Db::site()->fetchRow("select * from banks_bik where `NEWNUM`=?", $bik);

        return [
            "bik"        => isset($r['NEWNUM']) ? $r['NEWNUM']   : '',
            "ks"         => isset($r['KSNP'])   ? $r['KSNP']     : '',
            "name"       => isset($r['NAMEP'])  ? $r['NAMEP']    : '',
            "namemini"   => isset($r['NAMEN'])  ? $r['NAMEN']    : '',
            "index"      => isset($r['IND'])    ? $r['IND']      : '',
            "city"       => isset($r['NNP'])    ? $r['NNP']      : '',
            "address"    => isset($r['ADR'])    ? $r['ADR']      : '',
            "phone"      => isset($r['TELEF'])  ? $r['TELEF']    : '',
            "okato"      => isset($r['RGN'])    ? $r['RGN']      : '',
            "okpo"       => isset($r['OKPO'])   ? $r['OKPO']     : '',
            "regnum"     => isset($r['REGN'])   ? $r['REGN']     : '',
            "srok"       => isset($r['TNP'])    ? (int)$r['TNP'] : '',
        ];
    }
}
