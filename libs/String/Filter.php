<?php

class String_Filter {
    /**
     * Оставить в строке $string симворля только из заданного набора $symbolsString ($insensitive - требовательность к регистру)
     * Если указать $replace, то все неподходящие символы будут заменяться на эту строку
     *
     * @param $string
     * @param $symbolsString
     * @param bool $insensitive
     * @param string $replace
     * @return string
     */
    static public function only($string, $symbolsString, $insensitive = false, $replace = ''){
        $string         = (string)$string;
        $symbolsString  = (string)$symbolsString;
        $insensitive    = (bool)$insensitive;

        if(strlen($string)){
            $new = '';
            for($i = 0; $i < strlen($string); $i++){
                $symb = substr($string, $i , 1);
                if($insensitive){
                    if(strpos($symbolsString, $symb) !== false){
                        $new.= $symb;
                    }
                    else {
                        $new.= $replace;
                    }
                }
                else {
                    if(stripos($symbolsString, $symb) !== false){
                        $new.= $symb;
                    }
                    else {
                        $new.= $replace;
                    }
                }
            }
            $string = $new;
        }

        return $string;
    }
}