<?php

class Db {
    /**
     * @var Db_MysqliConnection[]
     */
    static protected $connections = [];

    /**
     * @param $name
     * @param $config
     * @return Db_MysqliConnection
     */
    static public function get($name, $config = null){
        if(!isset(self::$connections[$name])){
            self::$connections[$name] = new Db_MysqliConnection($config);
        }
        return self::$connections[$name];
    }

    /**************************************************************************/

    static protected $site          = 1;
    static protected $processing    = 2;
    static protected $reports       = 3;
    static protected $logs          = 4;
    static protected $scheduler     = 5;

    /**
     * @return Db_MysqliConnection
     */
    static public function site(){
        return self::get(self::$site, Conf::mysql_site());
    }

    /**
     * @return Db_MysqliConnection
     */
    static public function processing(){
        return self::get(self::$processing, Conf::mysql_processing());
    }

    /**
     * @return Db_MysqliConnection
     */
    static public function reports(){
        return self::get(self::$reports, Conf::mysql_reports());
    }

    /**
     * @return Db_MysqliConnection
     */
    static public function logs(){
        return self::get(self::$logs, Conf::mysql_logs());
    }

    /**
     * @return Db_MysqliConnection
     */
    static public function scheduler(){
        return self::get(self::$scheduler, Conf::mysql_scheduler());
    }

    /**************************************************************************/

    /**
     * @return Db_MysqliConnection[]
     */
    static public function getAllCurrentConnections(){
        return self::$connections;
    }

    /**
     * Получить общую информацию из профайлера
     *
     * @return array
     */
    static public function getProfilerSummaryInfo(){
        $all = [];
        if(count($connections = self::getAllCurrentConnections())){
            foreach($connections as $id => $connect){
                $p = $connect->getProfiler();

                $all[$id] = array(
                    'id'                => $id,
                    'database_name'     => $connect->getDatabaseName(),
                    'querys_count'      => $p->getQueriesCount(),
                    'runtime'           => $p->getRuntime(),
                    'connected_time'    => $p->getConnectedTime(),
                    'lond_query_time'   => $p->getLongQueryTime(),
                    'lond_query'        => $p->getLongQuery()
                );
            }
        }

        return $all;
    }

    static public function getDatabaseRuntime(){
        $runtime = 0;

        if(count($connections = self::getAllCurrentConnections())){
            foreach($connections as $connect){
                $runtime+= $connect->getProfiler()->getRuntime();
            }
        }

        return $runtime;
    }
}