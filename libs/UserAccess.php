<?php

class UserAccess {
    /**
     * @return bool
     */
    static public function isAdmin(){
        return Users::getCurrent()->role == User_Role::ADMIN;
    }

}