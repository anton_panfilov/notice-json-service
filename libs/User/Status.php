<?php

class User_Status extends Ui_Set {
    const ACTIVE = 1;
    const VERIFY = 2;
    const LOCK   = 3;

    public function __construct(){
        parent::__construct();

        $this->addValue_Green(  self::ACTIVE,   'Active'    );
        $this->addValue_Yellow( self::VERIFY,   'Verify'    );
        $this->addValue_Red(    self::LOCK,     'Lock'      );


    }
}