<?php

class User_Role extends Ui_Set {
    const ADMIN        = 1;

    const AGN_WM       = 12;
    const AGN_BUYER    = 13;

    const WM           = 20;
    const BUYER        = 30;

    public function __construct(){
        parent::__construct();

        $this->addValue_White(  self::ADMIN,        'Admin'                 );

        $this->addValue_White(  self::AGN_WM,       'Webmaster Agent'       );
        $this->addValue_White(  self::AGN_BUYER,    'Buyer Agent'           );

        $this->addValue_White(  self::WM,           'Webmaster'             );
        $this->addValue_White(  self::BUYER,        'Buyer'                 );
    }
}