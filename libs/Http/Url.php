<?php

class Http_Url {

    /**
     * Получение ссылки с теим же GET переменными что и текущая, дополненные переменными из $addGetValies
     * А также можно поменять сам path, передав ег ов переменную $path
     *
     * @param mixed $replaceGetValues
     * @param mixed $url
     * @param bool $getFromQuery получить или не получить значения из текущего запроса
     *                          если получить, то они дополняются переменными из $replaceGetValues
     *                          иначе используются только $replaceGetValues
     */
    static public function convert($replaceGetValues, $path = null, $getFromQuery = true){
        $url = parse_url($_SERVER['REQUEST_URI']);

        $get = [];
        if($getFromQuery && isset($url['query'])){
            parse_str($url['query'], $get);
        }

        if(is_array($replaceGetValues) && count($replaceGetValues)){
            $get = array_replace_recursive($get, $replaceGetValues);
        }

        if(!strlen($path)){
            $path = isset($url['path']) ? $url['path'] : "/";
        }

        $getString = http_build_query($get);

        if(!strlen($getString)) return $path;

        return $path . "?" . str_replace(
            ["%7B", "%7D", "%5B", "%5D"],
            ["{",   "}",   "[",   "]"],
            $getString
        );
    }

    static public function getCurrentURL(){
        return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? "https" : "http") .
            "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    }
}