<?php

class Result_OneReason {
    /**
     * @var bool
     */
    protected $result       = true;
    protected $error_code   = 0;
    protected $error_string = '';

    public function __construct($result = true, $error_code = 0, $error_string = ''){
        $this->result = (bool)$result;
        $this->error_code   = (int)$error_code;
        $this->error_string = (string)$error_string;
    }

    /*****************************************/

    /**
     * @param $code
     * @param string $message
     * @return $this
     */
    public function setError($code, $message = ''){
        $this->result       = false;
        $this->error_code   = (int)$code;
        $this->error_string = (string)$message;

        return $this;
    }

    public function setSuccess(){
        $this->result       = true;
        $this->error_code   = 0;
        $this->error_string = '';

        return $this;
    }

    /*****************************************/

    /**
     * @return bool
     */
    public function is(){
        return $this->result;
    }

    /**
     * @return int
     */
    public function getErrorCode(){
        return $this->error_code;
    }

    /**
     * @return string
     */
    public function getErrorString(){
        return $this->error_string;
    }

    public function getArray(){
        return [
            'result'        => $this->result,
            'error_code'    => $this->error_code,
            'error_string'  => $this->error_string,
        ];
    }
}