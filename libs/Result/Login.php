<?php

class Result_Login extends Result_OneReason {
    CONST ERROR_UNKNOWN             = 0; // у аккаунта неправильный статус в базе данных и т.п.

    CONST ERROR_INVALID_CREDENTIALS = 1;
    CONST ERROR_ACCOUNT_VERIFY      = 2;
    CONST ERROR_ACCOUNT_LOCK        = 3;

    CONST ERROR_MANY_ERRORS         = 4;
}