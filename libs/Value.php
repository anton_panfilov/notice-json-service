<?php

class Value {
    static public function export($var){
        echo "\n<pre>" . var_export($var, 1) . "</pre>\n";
    }

    static public function is(&$var, $default = null){
        return isset($var) ? $var : $default;
    }

    static protected $global = [];

    static public function setGlobal($name, $value){
        self::$global[$name] = $value;
    }

    static public function getGlobal($name, $default = null){
        return isset(self::$global[$name]) ? self::$global[$name] : $default;
    }
}