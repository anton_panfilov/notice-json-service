<?php

require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();

Captcha::renderCaptcha(
    isset($_GET['name'])        ? $_GET['name']         : "default",
    isset($_GET['background'])  ? $_GET['background']   : "FFFFFF"
);