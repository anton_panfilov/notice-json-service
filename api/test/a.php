<?php

/** @noinspection PhpIncludeInspection */
require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();

Value::export(
    Services_Banks_Bik::getInfoByBik($_GET['bik'])
);
