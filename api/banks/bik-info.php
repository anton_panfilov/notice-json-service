<?php

require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();

$result = [
    'request'   =>
        [
            'bik' => isset($_GET['bik']) ? $_GET['bik'] : "",
        ],
    'response'      => [],
];

if(isset($_GET['bik']) && strlen($_GET['bik']) && $_GET['bik'] > 0) {
    $result['response'] = Services_Banks_Bik::getInfoByBik($_GET['bik']);
}
else {
    $result['error'] = 'Invalid GET value `bik`';
    $result['response']  = Services_Banks_Bik::getInfoByBik("");
}



if(isset($_GET['jsonp']) && is_string($_GET['jsonp'])){
    header("content-type: application/javascript");
    echo "{$_GET['jsonp']}(" . Json::encode($result) . ")";
}
else {
    header("content-type: application/json");
    echo Json::encode($result);
}