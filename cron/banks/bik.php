<?php

require_once __DIR__ . "/../../libs/Boot.php";
Boot::installCore();

if(!Cron::isRun(__FILE__)){
    Cron::start(__FILE__);

    Services_Banks_Bik::reindexDB();
}
