document.group_sales_edit_group_label = function(id){
    var newLabel = window.prompt("Set new label", jQuery('#groupSales_gr' + id).text());

    jQuery('#groupSales_set' + id).hide();
    jQuery('#groupSales_load' + id).show();

    jQuery.getJSON(
        "/account/buyers/group-sales-set-group-label",
        {
            'id': id,
            'label': newLabel
        },
        function(data){

            jQuery('#groupSales_load' + id).hide();
            jQuery('#groupSales_set' + id).show();

            if(data.result){
                jQuery('#groupSales_gr' + id).text(newLabel);
            }
            else {
                alert(data.error_string);
            }
        }
    )
}

document.group_sales_delete_group = function(id){
    var newLabel = window.confirm("Remove empty group `" + jQuery('#groupSales_gr' + id).text() + "`?");

    jQuery('#groupSales_set' + id).hide();
    jQuery('#groupSales_load' + id).show();

    jQuery.getJSON(
        "/account/buyers/group-sales-delete-group",
        {
            'id': id
        },
        function(data){
            jQuery('#groupSales_load' + id).hide();
            jQuery('#groupSales_set' + id).show();

            if(data.result){
                jQuery('#groupSales_all' + id).hide();
            }
            else {
                alert(data.error_string);
            }
        }
    )
}