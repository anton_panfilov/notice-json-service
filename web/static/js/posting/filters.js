document.check_active_cb = function(name){
    if(jQuery('#active_' + name).is(':checked')){
        jQuery('#settings_' + name).show();
        jQuery('#label_' + name).css('color', '');
    }
    else {
        jQuery('#settings_' + name).hide();
        jQuery('#label_' + name).css('color', '#888');
    }
}

document.filter_calendar_render = function(name, anyway){
    val_wt = jQuery('#' + name + '_worktime').val();
    val_sl = jQuery('#' + name + '_sales').val();

    var val_groups_combi_str = '';
    var groups = new Object();

    for(i in document.filter_sales_groups_data){
        group = document.filter_sales_groups_data[i];

        str = jQuery('#filter_sales_group_gr' + group.id).val();

        var use = 1;
        if(jQuery.inArray(document.filter_sales_groups_cur_posting_id.toString(), group.postings) === -1){
            use = 0;
        }

        groups[group.id] = {
            'id':       group.id,
            'label':    group.label,
            'use':      use,
            'data_ui':  str
        };

        val_groups_combi_str+= str;
    }

    if(
        anyway == 1 ||
        document[name + '_conf_groups_combi']   != val_groups_combi_str ||
        document[name + '_conf_worktime']       != val_wt ||
        document[name + '_conf_sales']          != val_sl
    ){
        document[name + '_conf_groups_combi']   = val_groups_combi_str;
        document[name + '_conf_worktime']       = val_wt;
        document[name + '_conf_sales']          = val_sl;

        jQuery('.filter_calendar_loader').show();

        jQuery.post(
            '/account/buyers/postings/filter-datetime',
            {
                'config_worktime':          val_wt,
                'config_sales':             val_sl,
                'sales_groups':             groups,
                'today':                    document.filter_calendar_today,
                'today_sales':              document.filter_calendar_sales,
                'today_sales_of_groups':    document.filter_calendar_sales_of_groups
            },
            function(data){
                if(
                    jQuery('#' + name + '_worktime').val() == data.config_worktime &&
                        jQuery('#' + name + '_sales').val() == data.config_sales
                    ){
                    jQuery('#' + name + '_worktime_desc') .html(data.description_worktime);
                    jQuery('#' + name + '_sales_desc')    .html(data.description_sales);

                    jQuery('#' + name + '_timeline')      .html(data.timeline);

                    for(i in document.filter_sales_groups_data){
                        if(data.description_sales_groups[document.filter_sales_groups_data[i].id] === undefined){
                            jQuery('#filter_sales_desc_' + document.filter_sales_groups_data[i].id).html(
                                "<span style='color:#e51d18'><b>Error:</b> description not rendering</span>"
                            );
                        }
                        else {
                            jQuery('#filter_sales_desc_' + document.filter_sales_groups_data[i].id).html(
                                data.description_sales_groups[document.filter_sales_groups_data[i].id]
                            );
                        }
                    }

                    jQuery('.filter_calendar_loader').hide();
                }
            }
        );
    }
}

document.filter_calendar_preloader = function(name){
    jQuery('#' + name + '_worktime').autosize();
    jQuery('#' + name + '_sales').autosize();

    document[name + '_conf_worktime']   = jQuery('#' + name + '_worktime').val();
    document[name + '_conf_sales']      = jQuery('#' + name + '_sales').val();

    jQuery('#' + name + '_worktime').keyup(function(){
        document.filter_calendar_render(name);
    });

    jQuery('#' + name + '_sales').keyup(function(){
        document.filter_calendar_render(name);
    });
}


document.filter_minmax_visual = function(name){
    var min = jQuery('#' + name + '_min').val();
    if(min == ''){
        min = document[name + '_limit_min'];
    }
    else {
        min = parseFloat(min);
        if(min < document[name + '_limit_min']){
            min = document[name + '_limit_min'];
        }

        if(min > document[name + '_limit_max']){
            min = document[name + '_limit_max'];
        }
    }

    var max = jQuery('#' + name + '_max').val();
    if(max == ''){
        max = document[name + '_limit_max'];
    }
    else {
        max = parseFloat(max);
        if(max > document[name + '_limit_max']){
            max = document[name + '_limit_max'];
        }

        if(max < document[name + '_limit_min']){
            max = document[name + '_limit_min'];
        }
    }

    var start   = 0;
    var end     = 0;
    var block   = 100;

    if(min > max){
        block   = 0;
    }
    else {

        if(min != document[name + '_limit_min']){
            start = Math.ceil((min - document[name + '_limit_min']) * 100 /
            (document[name + '_limit_max'] - document[name + '_limit_min']));
        }

        if(max != document[name + '_limit_max']){
            end = Math.ceil((document[name + '_limit_max'] - max) * 100 /
            (document[name + '_limit_max'] - document[name + '_limit_min']));
        }

        if(start + end > 98){
            if(start > end) start = start - 2;
            else            end = end - 2;
        }

        block = 100 - start - end;
    }

    jQuery('#' + name + '_start')
        .css('width', String(start) + '%');

    jQuery('#' + name + '_block')
        .css('width', String(block) + '%');
}

document.filter_minmax_preloader = function(name, limit_min, limit_max){
    document[name + '_limit_min'] = limit_min;
    document[name + '_limit_max'] = limit_max;

    jQuery('#' + name + '_min').change(function(){
        var val = jQuery('#' + name + '_min').val();

        if(val != ''){
            val = parseFloat(val);
            if(isNaN(val)){
                jQuery('#' + name + '_min').val('');
            }
            else if(val < document[name + '_limit_min']){
                jQuery('#' + name + '_min').val(document[name + '_limit_min']);
            }
            else if(val > document[name + '_limit_max']){
                jQuery('#' + name + '_min').val(document[name + '_limit_max']);
            }
            else {
                jQuery('#' + name + '_min').val(val);
            }
        }

        document.filter_minmax_visual(name);
    });

    jQuery('#' + name + '_max').change(function(){
        var val = jQuery('#' + name + '_max').val();
        if(val != ''){
            val = parseFloat(val);
            if(isNaN(val)){
                jQuery('#' + name + '_max').val('');
            }
            else if(val < document[name + '_limit_min']){
                jQuery('#' + name + '_max').val(document[name + '_limit_min']);
            }
            else if(val > document[name + '_limit_max']){
                jQuery('#' + name + '_max').val(document[name + '_limit_max']);
            }
            else {
                jQuery('#' + name + '_max').val(val);
            }
        }

        document.filter_minmax_visual(name);
    });

    document.filter_minmax_visual(name);
}


document.filter_checkboxes_select_all = function(name){
    jQuery('#ui_' + name + '_list input:not(:checked)').click();
}

document.filter_checkboxes_unselect_all = function(name){
    jQuery('#ui_' + name + '_list input:checked').click();
}


document.filter_states_list_all = ["AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];

document.filter_states_parseList = function(name) {
    document[name] = [];
    jQuery('#ui_' + name + '_list :checked').each(function() {
        document[name].push($(this).val());
    });
}

document.filter_states_parseInput = function(name) {
    var arr = jQuery('#ta_' + name).val().toUpperCase().replace(/[,\\r\\n]/g, ' ').split(' ');

    document[name] = [];
    for(i in arr){
        if(
            jQuery.inArray(arr[i], document.filter_states_list_all) >= 0 &&
                jQuery.inArray(arr[i], document[name]) == -1
            ){
            document[name].push(arr[i]);
        }
    }
}

document.filter_states_renderToInput = function(name){
    var pieces = document[name];
    jQuery('#ta_' + name).val(
        (( pieces instanceof Array ) ? pieces.join ( ', ' ) : pieces )
    );
}

document.filter_states_renderToList = function(name){
    document[name + '_on_click_mode'] = false;

    jQuery('#ui_' + name + '_list :checked').click();

    for(i in document[name]){
        jQuery('#ui_' + name + '_list input[value=' + document[name][i] + ']').click();
    }

    document[name + '_on_click_mode'] = true;
}

document.filter_states_renderMap = function(name){
    var values = {};
    for(i in document.filter_states_list_all){
        var lcode = document.filter_states_list_all[i].toLowerCase();

        if(jQuery.inArray(document.filter_states_list_all[i], document[name]) >= 0){
            values[lcode] = '200';
        }
        else {
            values[lcode] = '0';
        }
    }
    document['map_' + name].vectorMap('set', 'values', values);
}

document.filter_states_preloader = function(name, currentValue){
    jQuery('#ta_' + name).autosize();

    document[name] = currentValue;
    document[name + '_on_click_mode'] = true;

    var data = {
        map: 'usa_en',
        backgroundColor: null,
        borderColor: '#777',
        borderOpacity: 0.5,
        borderWidth: 1,
        color: '#E5E5E5',
        enableZoom: true,
        hoverColor: '#bb2413',
        hoverOpacity: null,
        values: null,
        normalizeFunction: 'linear',
        scaleColors: ['#b6da93', '#427d1a'],
        selectedColor: '#c9dfaf',
        selectedRegion: null,
        showTooltip: true,
        onRegionClick: function (element, code, region) {
            element.preventDefault();
            var codeUpper = code.toUpperCase();

            if(jQuery.inArray(codeUpper, document[name]) >= 0){
                for (var key in document[name]) {
                    if (document[name][key] == codeUpper) {
                        document[name].splice(key, 1);
                    }
                }
            }
            else {
                document[name].push(codeUpper);
            }

            document['map_' + name].vectorMap('select', code);

            document.filter_states_renderToInput(name);
            document.filter_states_renderToList(name);
            document.filter_states_renderMap(name);

        }
    };

    document['map_' + name] = jQuery('#ui_' + name + '_map_cnv');

    if (document['map_' + name]) {
        document['map_' + name].width('500px');
        document['map_' + name].vectorMap(data);
    }

    document.filter_states_renderMap(name);

    jQuery('#ui_' + name + '_list input').click(function(){
        if(document[name + '_on_click_mode']){
            document.filter_states_parseList(name);
            document.filter_states_renderToInput(name);
            document.filter_states_renderMap(name);
        }
    });

    jQuery('#ta_' + name).change(function(){
        document.filter_states_parseInput(name);
        document.filter_states_renderToList(name);
        document.filter_states_renderMap(name);
    });

    jQuery('#ta_' + name).keyup(function(){
        document.filter_states_parseInput(name);
        document.filter_states_renderToList(name);
        document.filter_states_renderMap(name);
    });
}




document.filter_regions_list_all = ["22", "28", "29", "30", "31", "32", "33", "34", "35", "36", "79", "75", "37", "38", "07", "39", "40", "41", "09", "42", "43", "44", "23", "24", "45", "46", "47", "48", "49", "77", "50", "51", "83", "52", "53", "54", "55", "56", "57", "58", "59", "25", "60", "01", "04", "02", "03", "05", "06", "08", "10", "11", "81", "12", "13", "14", "15", "16", "17", "19", "61", "62", "63", "78", "64", "65", "66", "82", "67", "26", "68", "69", "70", "71", "72", "18", "73", "27", "86", "74", "20", "21", "87", "89", "76"];

document.filter_regions_parseList = function(name) {
    document[name] = [];
    jQuery('#ui_' + name + '_list :checked').each(function() {
        document[name].push($(this).val());
    });
}

document.filter_regions_parseInput = function(name) {
    var arr = jQuery('#ta_' + name).val().toUpperCase().replace(/[,\\r\\n]/g, ' ').split(' ');

    document[name] = [];
    for(i in arr){
        if(
            jQuery.inArray(arr[i], document.filter_regions_list_all) >= 0 &&
                jQuery.inArray(arr[i], document[name]) == -1
            ){
            document[name].push(arr[i]);
        }
    }
}

document.filter_regions_renderToInput = function(name){
    var pieces = document[name];
    jQuery('#ta_' + name).val(
        (( pieces instanceof Array ) ? pieces.join ( ', ' ) : pieces )
    );
}

document.filter_regions_renderToList = function(name){
    document[name + '_on_click_mode'] = false;

    jQuery('#ui_' + name + '_list :checked').click();

    for(i in document[name]){
        jQuery('#ui_' + name + '_list input[value=' + document[name][i] + ']').click();
    }

    document[name + '_on_click_mode'] = true;
}

document.filter_regions_renderMap = function(name){
    var values = {};
    var list = [];

    for(i in document[name]){
        list[i] =  document[name][i] + "";
        list[i] =  list[i].toLowerCase();
    }


    for(i in document.filter_regions_list_all){
        var lcode = document.filter_regions_list_all[i] + "";
        lcode = lcode.toLowerCase();

        if(jQuery.inArray(document.filter_regions_list_all[i], list) >= 0){
            values[lcode] = '200';
        }
        else {
            values[lcode] = '0';
        }
    }

    document['map_' + name].vectorMap('set', 'values', values);
}

document.filter_regions_preloader = function(name, currentValue){
    jQuery('#ta_' + name).autosize();

    document[name] = currentValue;
    document[name + '_on_click_mode'] = true;

    var data = {
        map: 'russia',
        backgroundColor: null,
        borderColor: '#777',
        borderOpacity: 0.5,
        borderWidth: 1,
        color: '#E5E5E5',
        enableZoom: true,
        hoverColor: '#bb2413',
        hoverOpacity: null,
        values: null,
        normalizeFunction: 'linear',
        scaleColors: ['#ffffff', '#b6da93'],
        selectedColor: '#c9dfaf',
        selectedRegion: null,
        showTooltip: true,
        onRegionClick: function (element, code, region) {
            element.preventDefault();
            var codeUpper = code.toUpperCase();

            if(jQuery.inArray(codeUpper, document[name]) >= 0){
                for (var key in document[name]) {
                    if (document[name][key] == codeUpper) {
                        document[name].splice(key, 1);
                    }
                }
            }
            else {
                document[name].push(codeUpper);
            }

            document['map_' + name].vectorMap('select', code);

            document.filter_regions_renderToInput(name);
            document.filter_regions_renderToList(name);
            document.filter_regions_renderMap(name);

        }
    };

    document['map_' + name] = jQuery('#ui_' + name + '_map_cnv');

    if (document['map_' + name]) {
        document['map_' + name].width('800px');
        document['map_' + name].height('500px');
        document['map_' + name].vectorMap(data);
    }

    document.filter_regions_renderMap(name);

    jQuery('#ui_' + name + '_list input').click(function(){
        if(document[name + '_on_click_mode']){
            document.filter_regions_parseList(name);
            document.filter_regions_renderToInput(name);
            document.filter_regions_renderMap(name);
        }
    });

    jQuery('#ta_' + name).change(function(){
        document.filter_regions_parseInput(name);
        document.filter_regions_renderToList(name);
        document.filter_regions_renderMap(name);
    });

    jQuery('#ta_' + name).keyup(function(){
        document.filter_regions_parseInput(name);
        document.filter_regions_renderToList(name);
        document.filter_regions_renderMap(name);
    });
}




document.filter_webmasters_preloader = function(name, currentValue){
    jQuery('#list_' + name).autosize();

    jQuery('#list_' + name).keyup(function(){
        document.filter_webmasters_parse(name);
    });

    document.filter_webmasters_parse(name);
}

document.filter_webmasters_parse = function(name){
    var arr = jQuery('#list_' + name).val().split('\n');

    var tempArr = {};
    var wmIDs = [];
    var temp1, temp2;

    for(ind in arr){
        tempArr[ind] = {
            'id': '',
            'str': ''
        };

        temp1 = jQuery.trim(arr[ind]);
        temp2 = temp1.replace(/[^0-9]/g, '');
        if(temp2.length >= 5 && temp1 == temp2){
            tempArr[ind].id = temp2;
            wmIDs.push(tempArr[ind].id);
        }
        else {
            tempArr[ind].str = "<span style='color:#CCC; text-decoration:line-through'>" + arr[ind] + "</span>";
        }
    }

    var getWebmasters = '';
    for(id in wmIDs){
        if(jQuery.Storage.get('webmastersPool' + wmIDs[id]) === undefined){
            if(getWebmasters.length > 0) getWebmasters += ',';
            getWebmasters += wmIDs[id];
        }
    }

    if(getWebmasters.length > 0){
        jQuery.getJSON(
            '/account/buyers/postings/get-webmasters-ids',
            {
                'ids': getWebmasters
            },
            function (data){
                for(id in data){
                    if(data[id] !== null){
                        jQuery.Storage.set('webmastersPool' + id, data[id]);

                        jQuery('span.filter_webmaster_wait_id_' + id).html(data[id]);
                    }
                    else {
                        jQuery('span.filter_webmaster_wait_id_' + id).html(
                            "<span style='color:#CCC; text-decoration:line-through;'>" + id + "</span>"
                        );
                    }
                }
            }
        )
    }

    for(ind in tempArr){
        if(tempArr[ind].id){
            if(jQuery.Storage.get('webmastersPool' + tempArr[ind].id) !== undefined){
                tempArr[ind].str = '<span>' + jQuery.Storage.get('webmastersPool' + tempArr[ind].id) + '</span>'
            }
            else {
                tempArr[ind].str = "<span class='filter_webmaster_wait_id_" + tempArr[ind].id + "'>" +
                    "<span style='color:#CCC'>...</span>"
                + "</span>";
            }
        }
    }

    var visual = '';
    for(ind in arr){
        visual+= tempArr[ind].str + '<br>';
    }
    jQuery('#visual_' + name).html(visual);


}

document.filter_sales_groups_render_empty = function(){
    jQuery('#filter_sales_content').html(
        "<div style='color:#777; text-align: center; padding-top: 5px; font-size: 12px'>" +
            "This buyer is not included in <b>Sales per Day Groups</b>, click to " +
            "<a style='cursor: pointer' onclick='document.group_sales_create_group()'>Create New Group</a>" +
            "</div>"
    );
}

document.filter_sales_groups_render_template = function(){
    jQuery('#filter_sales_content').html(
        "<h4 style='border-bottom: #EEE solid 1px; padding: 0 0 10px 10px; margin-bottom: 0px'>Sales in Day Groups</h4>" +
        "<div id='filter_sales_groups_content'></div>" +
        "<div id='filter_sales_bottom_content' class='filter_sales_bottom_content'>" +
            "<a id='filter_sales_create_btn' style='cursor: pointer; font-size: 11px' onclick='document.group_sales_create_group()'>Add new sales group</a>" +
            "<img id='filter_sales_create_load' src='/static/img/load/28x20.GIF' style='display: none'>" +
        "</div>"
    );
}

document.filter_sales_groups_render_all = function(){
    if(jQuery.isEmptyObject(document.filter_sales_groups_data)){
        document.filter_sales_groups_render_empty();
    }
    else {
        document.filter_sales_groups_render_template();

        for(j in document.filter_sales_groups_data){
            document.filter_sales_groups_render_add_group(document.filter_sales_groups_data[j])
        }
    }
}

document.filter_sales_groups_render_add_group = function(groupObject){
    cnt=  "<div id='filter_sales_group_content_" + groupObject.id + "' style='padding: 5px 0 10px 0; border-bottom: #EEE solid 1px'>";
    cnt+= "<a class='filter_sales_group_delete fa fa-trash-o' onclick='document.group_sales_delete_group(" + groupObject.id + ")'></a>";
    cnt+= "<h5>" +
        "<a " +
            "id='filter_sales_group_gr_set_label_" + groupObject.id + "'" +
            "class='filter_sales_group_label'" +
            "onclick='document.group_sales_edit_group_label(" + groupObject.id + ")'" +
        "><span id='filter_sales_group_gr_label_" + groupObject.id + "'>" +
            groupObject.label +
            "</span> <i class='fa fa-pencil'></i></span>" +
        "</a>" +
        "<span id='filter_sales_group_gr_load_label_" + groupObject.id + "' style='display:none'>" +
            "<img src='/static/img/load/28x20.GIF'>" +
        "</span>" +
        "</h5>";

    cnt+= "<table style='float: left; width: 100%;'>";
    cnt+= "<tr>";
    cnt+= "<td style='width: 270px; padding: 5px 20px 10px 0; border: 0;'>";
        cnt+= "<textarea ";
            cnt+= "id='filter_sales_group_gr" + groupObject.id + "' ";
            cnt+= "name='filter_sales_group_gr" + groupObject.id + "' ";
            cnt+= "onkeyup=\"document.filter_calendar_render('filter_calendar')\" ";
            cnt+= "style=\"width: 94%; resize:none; margin: 0; font-family: 'Courier New'; background: center no-repeat\" ";
        cnt+= ">" + jQuery('<div/>').text(groupObject.data_ui).html() + "</textarea>";
    cnt+= "</td>";
    cnt+= "<td style='width: 300px; padding: 6px 0 10px 10px; border: 0;' id='filter_sales_desc_" + groupObject.id + "'>";
        cnt+= groupObject.data_ui_description;
    cnt+= "</td>";
    cnt+= "<td style='border: 0'>";

    var last_product = 0;
    for(var k in document.filter_sales_groups_postings){
        postingObject = document.filter_sales_groups_postings[k];

        if(last_product != postingObject.product){
            if(last_product != 0) cnt+= "</div>";

            cnt+= "<div style='float: left; margin: 0 50px 30px 0'>";
            cnt+= "<div style='color: #999; font-size: 10px;'>";
            cnt+= "<b>" +
                document.filter_sales_groups_products[postingObject.product] +
                "</b>";
            cnt+= "</div>";
        }

        cnt+= "<div>";
        cnt+= "<a ";
            cnt+= "id='filter_sales_posting_" + groupObject.id + "_" + postingObject.id + "' ";
            cnt+= "style='";
                cnt+= "cursor: pointer;";
                cnt+= "text-decoration: none;";
                cnt+= "padding-left: 20px;";

                if(jQuery.inArray(
                    postingObject.id,
                    groupObject.postings
                ) != -1){
                    cnt+= "color:#062"; // include
                }
                else {
                    cnt+= "color:#AAA"; // no include
                }

            cnt+= "' ";
            cnt+= "onclick='document.group_sales_toggle_posting(" + groupObject.id + ", " + postingObject.id + ")' " +
        ">";

        if(document.filter_sales_groups_cur_posting_id == postingObject.id){
            cnt+= "<span style='text-decoration:underline'>" +
                "<b>" + postingObject.id + "</b> - " + postingObject.name +
                "</span>"
        }
        else {
            cnt+= postingObject.id + " - " + postingObject.name;
        }

        cnt+= "</a></div>";

        last_product = postingObject.product;
    }

    cnt+= "</div>";

    cnt+= "</td>";
    cnt+= "</tr>";
    cnt+= "</table>";
    cnt+= "<br style='clear: both'>";
    cnt+= "</div>";

    jQuery('#filter_sales_groups_content').append(cnt);

    jQuery(
        '#filter_sales_group_gr' + groupObject.id
    ).autosize();
}

document.group_sales_created_group_now = false;

document.group_sales_create_group = function(){
    if(!document.group_sales_created_group_now){
        document.group_sales_created_group_now = true;

        if(jQuery.isEmptyObject(document.filter_sales_groups_data)){
            document.filter_sales_groups_render_template();
        }

        jQuery('#filter_sales_create_btn').hide();
        jQuery('#filter_sales_create_load').show();

        jQuery.getJSON(
            "/account/buyers/group-sales-create-group",
            {
                'buyer': document.filter_sales_groups_cur_buyer_id
            },
            function(data){
                jQuery('#filter_sales_create_btn').show();
                jQuery('#filter_sales_create_load').hide();

                if(data.create_id){
                    var groupObject = {
                        "id"                    :   data.create_id,
                        "label"                 :   "",
                        "data_ui"               :   "",
                        "postings"              :   [],
                        "data_ui_description"   :   ""
                    };

                    document.filter_sales_groups_data[data.create_id] = groupObject;
                    document.filter_sales_groups_render_add_group(groupObject);

                    document.filter_calendar_render('filter_calendar', 1);
                }
                else {
                    alert(data.reason);
                }

                document.group_sales_created_group_now = false;
            }
        )
    }
}

document.group_sales_edit_group_label = function(id){
    var currentLabel = jQuery('#filter_sales_group_gr_label_' + id).text();
    var newLabel = window.prompt("Set new label", currentLabel);

    if(newLabel != currentLabel && newLabel != null){
        jQuery('#filter_sales_group_gr_set_label_' + id).hide();
        jQuery('#filter_sales_group_gr_load_label_' + id).show();

        jQuery.getJSON(
            "/account/buyers/group-sales-set-group-label",
            {
                'id': id,
                'label': newLabel
            },
            function(data){

                jQuery('#filter_sales_group_gr_load_label_' + id).hide();
                jQuery('#filter_sales_group_gr_set_label_' + id).show();

                if(data.result){
                    jQuery('#filter_sales_group_gr_label_' + id).text(newLabel);
                    document.filter_sales_groups_data[id].label = newLabel;
                    document.filter_calendar_render('filter_calendar', 1);
                }
                else {
                    alert(data.error_string);
                }
            }
        );
    }
}

document.group_sales_delete_group = function(id){
    jQuery('#filter_sales_group_content_' + id)
        .css('opacity', '0.4');

    if(window.confirm("Remove group `" + jQuery('#filter_sales_group_gr_label_' + id).text() + "`?")){
        jQuery('#filter_sales_group_content_' + id).hide();

        delete document.filter_sales_groups_data[id];

        document.filter_calendar_render('filter_calendar', 1);

        if(jQuery.isEmptyObject(document.filter_sales_groups_data)){
            document.filter_sales_groups_render_empty();
        }

        jQuery.getJSON(
            "/account/buyers/group-sales-delete-group",
            {
                'id': id
            },
            function(data){
                if(!data.result){
                    alert(data.error_string);
                }
            }
        )
    }
    else {
        jQuery('#filter_sales_group_content_' + id)
            .css('opacity', '1');
    }
}

document.group_sales_toggle_posting = function(group, posting){
    group = group.toString();
    posting = posting.toString();

    jQuery('#filter_sales_posting_' + group + "_" + posting).css('color', '#f89406');

    var pos = jQuery.inArray(posting, document.filter_sales_groups_data[group].postings);

    if(pos == -1){
        document.filter_sales_groups_data[group].postings.push(posting);
        action = 1;
    }
    else {
        document.filter_sales_groups_data[group].postings.splice(pos, 1);
        action = 0;
    }

    document.filter_calendar_render('filter_calendar', 1);

    jQuery.getJSON(
        '/account/buyers/group-sales-toggle-posting',
        {
            'group'   : group,
            'posting' : posting,
            'action'  : action
        },
        function(data){
            if(data.result){
                if(action){
                    jQuery('#filter_sales_posting_' + group + "_" + posting).css('color', '#062');
                }
                else {
                    jQuery('#filter_sales_posting_' + group + "_" + posting).css('color', '#AAA');
                }
            }
            else {
                alert(data.error_string);
            }
        }
    )
}

document.filter_list_refresh = function(name, buyer, type){
    var list = jQuery('#list_' + name).val();

    jQuery('#listDiv_' + name).html('<i class="fa fa-refresh fa-spin"></i>');
    jQuery.getJSON(
        "/account/buyers/postings/get-select-for-filters-list",
        {
            'name':  name,
            'buyer': buyer,
            'type':  type,
            'list':  list
        },
        function(data){
            jQuery('#listDiv_' + name).html(data.select);
        }
    )
}