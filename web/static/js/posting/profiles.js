document.renderProfile_setCompanyName = function(value){
    if(value.length){
        jQuery('#profile_example_cname').show().html(value);
    }
    else {
        jQuery('#profile_example_cname').hide();
    }
}

document.renderProfile_setDescription = function(value){
    if(value.length){
        jQuery('#profile_example_description').show().html(value);
    }
    else {
        jQuery('#profile_example_description').hide();
    }
}

document.renderProfile_setWebsite = function(value){
    if(value.length){
        jQuery('#profile_example_website').show().html(value);
    }
    else {
        jQuery('#profile_example_website').hide();
    }
}

document.renderProfile_setPhone = function (value){
    if(value.length){
        jQuery('#profile_example_phone').show();
        phone = '(' + value.substr(0, 3) + ')';

        if(value.length > 3)  phone+= ' ' + value.substr(3, 3);
        if(value.length > 6)  phone+= ' - ' + value.substr(6, 4);
        if(value.length > 10) phone+= ' ext. ' + value.substr(10, 5);

        jQuery('#profile_example_phone span').html(phone);
    }
    else {
        jQuery('#profile_example_phone').hide();
    }
}

document.renderProfile_setLogo = function (value) {
    if (!value.length) value = '_default.png';
    jQuery('#profile_example_logo').attr('src', '/static/logo/' + value);
};

document.renderProfile = function(){
    jQuery('#profile_example').html(
        "<div style='float: left; padding-right: 20px'> " +
            "<img src='/static/logo/_default.png' id='profile_example_logo' style='width: 150px'> " +
        "</div> " +
        "<div style='float: left; min-width: 300px'> " +
            "<h3 style='margin: 0 0 8px 0; line-height: 100%' id='profile_example_cname'></h3> " +
            "<div> " +
                "<a id='profile_example_website' style='color: #00802a; padding-right: 15px'></a> " +
                "<span style='white-space: nowrap; display: none' id='profile_example_phone'> " +
                    "<i class='icon-phone' style='color: #333'></i> " +
                    "<span style='color:#379; margin-left: 3px; font-weight: bold'></span> " +
                "</span> " +
                "<div id='profile_example_description' style='color: #666; white-space: pre; margin-top: 7px'></div> " +
            "</div> " +
        "</div>"
    );

    document.renderProfile_setCompanyName(jQuery('#company_name').val());
    document.renderProfile_setDescription(jQuery('#description').val());
    document.renderProfile_setWebsite(jQuery('#website').val());
    document.renderProfile_setPhone(jQuery('#phone').val());
    document.renderProfile_setLogo(jQuery('#logo').val());
}

jQuery(function(){
    jQuery('#company_name')
        .keyup(function(){document.renderProfile_setCompanyName(this.value)})
        .change(function(){document.renderProfile_setCompanyName(this.value)});

    jQuery('#description')
        .keyup(function(){document.renderProfile_setDescription(this.value)})
        .change(function(){document.renderProfile_setDescription(this.value)});

    jQuery('#website')
        .keyup(function(){document.renderProfile_setWebsite(this.value)})
        .change(function(){document.renderProfile_setWebsite(this.value)});

    jQuery('#phone')
        .keyup(function(){document.renderProfile_setPhone(this.value)})
        .change(function(){document.renderProfile_setPhone(this.value)});

    jQuery('#logo')
        .keyup(function(){document.renderProfile_setLogo(this.value)})
        .change(function(){document.renderProfile_setLogo(this.value)});

    document.renderProfile();
});