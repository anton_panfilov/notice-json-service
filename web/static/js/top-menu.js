// Bzid version

document.topMenuDropDownBlockShow = false;
document.topMenuDropDownBlockShowIter = 0;

jQuery(function(){
    jQuery('ul.topmenu li').mouseover(function(){
        jQuery('ul.topmenu li div.topmenuDropDownBlock').hide();
        // jQuery(this).find('div.topmenuDropDownBlock').show('blind', 100);
        jQuery(this).find('div.topmenuDropDownBlock').show();
        document.topMenuDropDownBlockShow = true;
        document.topMenuDropDownBlockShowIter++;
    });

    jQuery('ul.topmenu li').mouseout(function(){
        document.topMenuDropDownBlockShow = false;
        var iter = document.topMenuDropDownBlockShowIter;

        setTimeout(function(){
            if(document.topMenuDropDownBlockShow == false){
                if(iter == document.topMenuDropDownBlockShowIter){
                    jQuery('ul.topmenu li div.topmenuDropDownBlock').hide();
                }
            }
        }, 300);
    });
});