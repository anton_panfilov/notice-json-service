 
	function uploadFiles (form, bar, percent, status, b_upload, b_cancel) {        
		
        form.ajaxForm({
            beforeSend: function (jqXHR) {
                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
                b_upload.attr('disabled', 'disabled');
                b_cancel.removeAttr('disabled');
                b_cancel.on ('click', 
                    function() {
                        jqXHR.abort();
                    });
            },
            uploadProgress: function(event, position, total, percentComplete) {
                //console.log ('% compl = ', percentComplete);
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                console.log (xhr);
                b_upload.removeAttr('disabled');
                b_cancel.attr('disabled', 'disabled');
                if ('abort' === xhr.statusText) status.html('Cancelled by user');
                else {
                    var log = xhr.responseText;
                    jQuery('.console-log', log).each(function() {
                        console.log (jQuery(this).html());
                        });
                        
                    var msg = xhr.responseText;
                    msg = jQuery('.file-upload-complete-status', msg).html();
                    if (msg == '') { bar.width('0%');  percent.html('0%'); }
                    status.html(msg);
                }
            }
        });
	}