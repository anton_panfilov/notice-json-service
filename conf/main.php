<?php return [
    // Локализация
    'locales'               => [
        'us' => 'English',
        'ru' => 'Русский',
    ],
    'locale_default'        => 'us',
    'localeLearningMode'    => false,

    // Домены
    'domain'                => 'json-service.com',
    'domain_account'        => 'json-service.com',
    'domain_static'         => 'static.json-service.com',

    // Временная зона
    'timezone'              => 'America/Los_Angeles',

    // Для SMTP
    'hostname_for_smtp'     => 'mail.t3leads.com',
];
